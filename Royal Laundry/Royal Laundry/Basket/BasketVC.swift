//
//  BasketVC.swift
//  Royal Laundry
//
//  Created by Shine on 19/10/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class BasketVC: UIViewController, UITextFieldDelegate {
    
    //MARK:- Outlets
    
    @IBOutlet weak var AddItemHeight: NSLayoutConstraint! //33.5
    @IBOutlet weak var viewVoucherData: UIView!
    @IBOutlet weak var lblVoucherDiscount: UILabel!
    @IBOutlet weak var lblVoucherPrice: UILabel!
    
    @IBOutlet weak var viewTopStatusBG: UIView!
    @IBOutlet weak var viewTop: UIView!
    
    @IBOutlet weak var lblViewTitle: UILabel!
    
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet weak var objScrollview: UIScrollView!
    @IBOutlet weak var viewScrollContent: UIView!
    @IBOutlet weak var viewAddressBG: UIView!
    @IBOutlet weak var viewDeliveryBG: UIView!
    @IBOutlet weak var viewBasketItemBG: UIView!
    @IBOutlet weak var viewCheckoutItemBG: UIView!
    
    @IBOutlet weak var txtAddressLine1: UITextField!
    @IBOutlet weak var imgAddressLine1: UIImageView!
    @IBOutlet weak var sepAddressLine1: UIView!
    
    
    @IBOutlet weak var txtAddressLine2: UITextField!
    @IBOutlet weak var imgAddressLine2: UIImageView!
    @IBOutlet weak var sepAddressLine2: UIView!
    
    
    @IBOutlet weak var txtPostCode: UITextField!
    @IBOutlet weak var imgPostCode: UIImageView!
    @IBOutlet weak var sepPostCode: UIView!
    
    
    @IBOutlet weak var txtDeliveryNotes: UITextField!
    @IBOutlet weak var imgDeliveryNotes: UIImageView!
    @IBOutlet weak var sepDeliveryNotes: UIView!
    
    @IBOutlet weak var lblCollectionTimeTitle: UILabel!
    @IBOutlet weak var lblCollectDate: UILabel!
    @IBOutlet weak var lblCollectTime: UILabel!
    
    @IBOutlet weak var lblDeliTImeTitle: UILabel!
    @IBOutlet weak var lblDeliDate: UILabel!
    @IBOutlet weak var lblDeliTime: UILabel!
    
    
    @IBOutlet weak var txtCleaningInst: UITextField!
    @IBOutlet weak var imgCleaningInst: UIImageView!
    @IBOutlet weak var sepCleaningInst: UIView!
    
    @IBOutlet weak var tblBasketItem: UITableView!
    @IBOutlet weak var viewSkipItemSel: UIView!
    
    @IBOutlet weak var lblBasketPriceTitle: UILabel!
    @IBOutlet weak var lblBasketPrice: UILabel!
    
    @IBOutlet weak var btnAddItem: UIButton!
    
    @IBOutlet weak var btnEnterVoucherCode: UIButton!
    
    @IBOutlet weak var viewFooter: UIView!
    
    @IBOutlet weak var btnPlaceOrder: UIButton!
    
    @IBOutlet weak var constTblHeight: NSLayoutConstraint!

    
    @IBOutlet weak var viewCollection: UIView!
    @IBOutlet weak var viewDelivery: UIView!
    
    // MARK: - constants & variables
    
    var arrBaksetItem = SQLiteDataArray()
    let objDbManager = SQLiteHelper()
    var objCollWorkingTime = WorkingTimeModel()
    var objDeliWorkingTime = WorkingTimeModel()

    
    //MARK:- UIView Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*~~~~~ notification  ~~~~~*/
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBasketPrice), name: NSNotification.Name(rawValue: kUpdateProductDetailViewBasketPrice), object: nil)

        self.navigationController?.navigationBar.isHidden = true
        tblBasketItem.dataSource = self
        tblBasketItem.delegate = self
        tblBasketItem.tableFooterView = UIView() // Removes empty cell separators
        tblBasketItem.estimatedRowHeight = 60
        tblBasketItem.rowHeight = UITableView.automaticDimension
        //tblBasketItem.registerCell(cellIDs: [ProductItemTableViewCell], isDynamicHeight: true, estimatedHeight: 40)
        let nib1 = UINib(nibName: "ProductItemTableViewCell", bundle: nil)
        tblBasketItem.register(nib1, forCellReuseIdentifier: "ProductItemTableViewCell")
        btnClose.addTarget(self, action: #selector(backToHome), for: .touchUpInside)
        
        
        btnEnterVoucherCode.addTarget(self, action: #selector(btnEnterVoucherCodePressed), for: .touchUpInside)
        
        txtPostCode.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        
        self.sepPostCode.backgroundColor = kAppTextSepColor
        
     //   self.setupInitialView()
        let pricedouble = Double(kAppDelegate.basketPrice)
        let str2 = "\(StringFile.strPriceSymbol[kAppDelegate.userCurrentLanguage])\(pricedouble)"
        self.lblBasketPrice.text = str2
        self.viewVoucherData.isHidden = true
        self.btnEnterVoucherCode.isHidden = false
        if arrBaksetItem.count > 0{
            btnAddItem.isHidden = true
            AddItemHeight.constant = 0
            
        }else{
            btnAddItem.isHidden = false
            AddItemHeight.constant = 33.5
            btnAddItem.addTarget(self, action: #selector(btnAddItemPressed), for: .touchUpInside)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.getBasketItems()
        self.updateBasketPrice()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.getBasketItems()
        self.updateBasketPrice()
    }
    override func viewDidLayoutSubviews() {
        constTblHeight.constant = tblBasketItem.contentSize.height
    }
    //MARK:- Class Methods and Functions
    @objc func updateBasketPrice(){
            
        if kAppDelegate.basketPrice != 0{
           
            let pricedouble = Double(kAppDelegate.basketPrice)
            let str2 = "\(StringFile.strPriceSymbol[kAppDelegate.userCurrentLanguage])\(pricedouble) "
            lblBasketPrice.text = str2
        }else{
            let str2 = "\(StringFile.strPriceSymbol[kAppDelegate.userCurrentLanguage])0.00 "
            lblBasketPrice.text = str2
        }
        if arrBaksetItem.count == 1{
            self.constTblHeight.constant =  CGFloat(self.arrBaksetItem.count * 155)
            self.tblBasketItem.updateConstraints()
        }else{
            self.constTblHeight.constant =  tblBasketItem.contentSize.height
            self.tblBasketItem.updateConstraints()
        }
    }
    func setupInitialView(){
        viewTopStatusBG.backgroundColor = kAppThemePrimaryDarkColor
        viewTop.backgroundColor = kAppThemePrimaryLightColor
        self.getBasketItems()
    }
    
    //MARK: - Web Services

    func checkPostalAvailability(postcode:String)
    {
        //let kPostcodeAthorisedkey = "kv0NXS-5pU6z2uywVF2rxw11778"
        let url = "https://api.getaddress.io/find/\(postcode)?api-key=\(kPostcodeAthorisedkey)"
        print(url)
        Alamofire.request(url).responseJSON { response in
            switch response.result {
            case .success:
                print(response.result.value)
                let value = "\(response.result.value)"
                if value.contains(find: "Invalid postcode"){
                    print("Invalid postal code")
                }else{
                    let objVC = self.storyboard?.instantiateViewController(withIdentifier: "PostalCodeAlertVC") as! PostalCodeAlertVC
                    
                    self.addChild(objVC)
                    objVC.postalcode = self.txtPostCode.text
                    objVC.view.frame = self.view.frame
                    self.view.addSubview(objVC.view)
                    objVC.didMove(toParent: self)
                }

            case .failure: break
               // print(error)
            }
        }
    }
    func getWorkingTimeApi(withpostcode postcode: String){
        
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        WebServiceCall.callMethodWithURL(route: APIRouter.GetWorkingTime(withpostcode: postcode)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            if (responseValue != nil) {
                
                let responseModel = WorkingTimeModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200 {
                    
                    
                    print(responseModel.data)
                    
                    self.objCollWorkingTime = responseModel
                    
                    self.lblCollectDate.text = self.objCollWorkingTime.data.first!.date
                    self.lblCollectTime.text = self.objCollWorkingTime.data.first!.working_time.first!.working_hr
                    GlobalMethods.setUserDefaultObject(object: self.lblCollectDate.text as AnyObject, key: MyUserDefaultsKeys.colldate)
                    GlobalMethods.setUserDefaultObject(object: self.lblCollectTime.text as AnyObject, key: MyUserDefaultsKeys.collTime)
                    
                    self.getDeliveryTimeApi(withpostcode: postcode, collection_date: self.objCollWorkingTime.data.first!.date, collection_time: self.objCollWorkingTime.data.first!.working_time.first!.working_hr)
                    
                }
                else {
                    GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.dismissLoaderView()
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }

    func getDeliveryTimeApi(withpostcode postcode: String, collection_date: String, collection_time: String){
        
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        WebServiceCall.callMethodWithURL(route: APIRouter.GetDeliveryTime(withpostcode: postcode, collection_date: collection_date, collection_time: collection_time)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            if (responseValue != nil) {
                
                let responseModel = WorkingTimeModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200 {
                    
                    
                    print(responseModel.data)
                    self.objDeliWorkingTime = responseModel
                    
                    self.lblDeliDate.text = self.objDeliWorkingTime.data.first!.date
                    self.lblDeliTime.text = self.objDeliWorkingTime.data.first!.working_time.first!.working_hr
                    GlobalMethods.setUserDefaultObject(object: self.lblDeliDate.text as AnyObject, key: MyUserDefaultsKeys.deldate)
                    GlobalMethods.setUserDefaultObject(object: self.lblDeliTime.text as AnyObject, key: MyUserDefaultsKeys.delTime)

                    
                }
                else {
                    GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.dismissLoaderView()
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func getPlaceOrderApi(){
        
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        let reqParam = [ParameterKeys.Id : 1, ParameterKeys.Customer_id : 1, ParameterKeys.User_Id : 1] as [String : Any]
        
        WebServiceCall.callMethodWithURL(route: APIRouter.PlaceOrder(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            if (responseValue != nil) {
                
                print(responseValue)
                                
            }
            else {
                GlobalMethods.dismissLoaderView()
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func getBasketItems(){
        
        let strCountQuery = "SELECT * FROM cart_item;"
        print(strCountQuery)
        
        let database =  try! SQLitePool.manager().initialize(database: "royalLaundry", withExtension: "sqlite")
        
        let resultSelectAll = try! database.query(strCountQuery)
        print(resultSelectAll)
        
        
        if resultSelectAll.results != nil {
            viewSkipItemSel.isHidden = true
            arrBaksetItem = resultSelectAll.results!            
            tblBasketItem.reloadData()
            btnAddItem.isHidden = true
            viewSkipItemSel.isHidden = true

        }else{
            viewSkipItemSel.isHidden = false
            btnAddItem.isHidden = false
            tblBasketItem.isHidden = true
            
        }
    }
    
    @objc func backToHome() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func btnAddItemPressed() {
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchItemViewController") as! SearchItemViewController
        self.addChild(objVC)
        objVC.isbasket = true
        objVC.didSelectItem = { [weak self](item) in
            if let objVC = self {
                GlobalMethods.getBasketUpdate()
                self!.getBasketItems()
                self!.tblBasketItem.reloadData()
            }
        }
        objVC.view.frame = self.view.frame
        self.view.addSubview(objVC.view)
        objVC.didMove(toParent: self)
    }
    func showToast(message : String) {

        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 250, height: 35))
        toastLabel.center.x = self.view.center.x
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    @objc func btnPlaceOrderPressed() {
        
        if txtAddressLine1.text!.isEmpty || txtAddressLine2.text!.isEmpty{
            showToast(message: "Please enter address")
            txtAddressLine1.becomeFirstResponder()
            return
        }
        if GlobalMethods.getUserDefaultObjectForKey(key: MyUserDefaultsKeys.isloginsucess) != nil{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TotalPaymentVC") as! TotalPaymentVC
            vc.line1 = self.txtAddressLine1.text
            vc.line2 = self.txtAddressLine2.text
            vc.postalcode = self.txtPostCode.text
            vc.collTime = self.lblCollectTime.text
            vc.collDate = self.lblCollectDate.text//"collection,\(self.lblColDate.text!)"
            vc.delDate =  self.lblDeliDate.text//"Delivery,\(self.lblDeliveryDate.text!)"
            vc.delTime = self.lblDeliTime.text
            let str1 = "Your Order(\(kAppDelegate.basketItemQuantity))    "
            let str2 = "\(StringFile.strPriceSymbol[kAppDelegate.userCurrentLanguage])\(kAppDelegate.basketPrice)"
            vc.basket = str1 + str2
            vc.price = Int(kAppDelegate.basketPrice)
            vc.deliverynote = self.txtDeliveryNotes.text
            vc.arrItems = arrBaksetItem
          //  navigationController?.pushViewController(vc, animated: true)
            //self.present(vc, animated: true, completion: nil)
          self.presentInFullScreen(vc, animated: true)
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.presentInFullScreen(vc, animated: true)
        }
    }
    @objc func btnEnterVoucherCodePressed() {
        if arrBaksetItem.count == 0 {
            showToast(message: "Please add item in cart")
            return
        }
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "VoucherCodeAlertViewController") as! VoucherCodeAlertViewController
        objVC.didSelectItem = { [weak self](voucherDisccount) in
            if let objVC = self {
                self?.btnEnterVoucherCode.isHidden = true
                self?.viewVoucherData.isHidden = false
                self?.lblVoucherDiscount.text = voucherDisccount
                self?.lblVoucherPrice.text = self?.lblBasketPrice.text
                let totalDouble = Double(kAppDelegate.basketPrice)
                let voucherdis = Double(voucherDisccount)
                let price = totalDouble - voucherdis!
                let pricedouble = Double(price)
                let str2 = "\(StringFile.strPriceSymbol[kAppDelegate.userCurrentLanguage])\(pricedouble)"
                self?.lblBasketPrice.text = str2
            }
        }
        self.addChild(objVC)
        objVC.totalprice = Int(kAppDelegate.basketPrice)
        objVC.view.frame = self.view.frame
        self.view.addSubview(objVC.view)
        objVC.didMove(toParent: self)
        
    }
    @objc func DeliveryPressed() {
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "CollAndDelViewController") as! CollAndDelViewController
        self.addChild(objVC)
        objVC.isCollection = false
        let code = self.txtPostCode.text
        objVC.postcode = code
        objVC.workTime = self.lblCollectTime.text
        objVC.workDate = self.lblCollectDate.text
        objVC.didSelectItem = { [weak self](time,date,day,ischarge) in
            if let objVC = self {
                print("Delivery Time:\(time)")
                self?.lblDeliTime.text = time
                //self?.lblDeliveryDay.text = day
                self?.lblDeliDate.text = date
            }
        }
        objVC.view.frame = self.view.frame
        self.view.addSubview(objVC.view)
        objVC.didMove(toParent: self)
    }
    @objc func CollectionPressed(){

        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "CollAndDelViewController") as! CollAndDelViewController
        self.addChild(objVC)
        objVC.isCollection = true
        let code = self.txtPostCode.text
        objVC.postcode = code
        objVC.parentvc = self
        objVC.didSelectItem = { [weak self](time,date,day,ischarge) in
            if let objVC = self {
                print("CollectionTime:\(time)")
                self?.lblCollectTime.text = time
                self?.lblCollectDate.text = date
               // self?.lblColDay.text = day
//                if ischarge == true{
//                    let total = Double(kAppDelegate.basketPrice) + 1.99
//                    self?.lblBasketPrice.text = "\(total)"
//                }
            }
        }
        objVC.view.frame = self.view.frame
        self.view.addSubview(objVC.view)
        objVC.didMove(toParent: self)
    }
    
    // MARK: - UITextField Delegate Methods
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == txtPostCode {
            let strPostCode = txtPostCode.text!
            if strPostCode.count > 0 {
                if strPostCode.isValidPostalCode() {
                    checkPostalAvailability(postcode: strPostCode)
                    self.sepPostCode.backgroundColor = kAppTextSepColor
                    self.getWorkingTimeApi(withpostcode: strPostCode)
                    self.viewCollection.borderWidth = 5
                    self.viewCollection.borderColor = kAppButtonBGActiveColor
                    self.viewDelivery.borderWidth = 5
                    self.viewDelivery.borderColor = kAppButtonBGActiveColor
                    btnPlaceOrder.isUserInteractionEnabled = true
                    btnPlaceOrder.addTarget(self, action: #selector(btnPlaceOrderPressed), for: .touchUpInside)
                    btnPlaceOrder.backgroundColor = kAppButtonBGActiveColor
                    btnPlaceOrder.setTitleColor(kAppButtonTextActiveColor, for: .normal)
                    viewCollection.addTapGesture(tapNumber: 1, target: self, action: #selector(CollectionPressed))
                    viewDelivery.addTapGesture(tapNumber: 1, target: self, action: #selector(DeliveryPressed))
                }
                else{
                    self.sepPostCode.backgroundColor = UIColor.red
                }
            }else{
                btnPlaceOrder.isUserInteractionEnabled = false
                self.sepPostCode.backgroundColor = kAppTextSepColor
                self.viewCollection.borderWidth = 5
                self.viewCollection.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                self.viewDelivery.borderWidth = 5
                self.viewDelivery.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                btnPlaceOrder.backgroundColor = kAppButtonBGDeactivateColor
                btnPlaceOrder.setTitleColor(kAppButtonTextDeactivateColor, for: .normal)
            }
        }
    }
}

// MARK: - UITableView DataSource, Delegate Methods

extension BasketVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let unique = Array(Set(arrBaksetItem))
        return unique.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductItemTableViewCell", for: indexPath) as! ProductItemTableViewCell

        let resultItem = arrBaksetItem[indexPath.row]
        let quantity = Int(resultItem["quantity"] as! String)
        
//        if arrBaksetItem.count > 2{
//            constTblHeight.constant = constTblHeight.constant + constTblHeight.constant
//        }
        
        if quantity! > 0 {
            cell.imgIncrement.image = #imageLiteral(resourceName: "add_item")
            cell.btnDecHeight.constant = 40
            cell.lblQuaHeight.constant = 40
            
            cell.imgDecrement.isHidden = false
            cell.lblQuantity.isHidden = false
            cell.btnDecrement.isHidden = false
            
        } else {
            cell.imgIncrement.image = #imageLiteral(resourceName: "Plus")
            cell.btnDecHeight.constant = 0
            cell.lblQuaHeight.constant = 0
            
            cell.imgDecrement.isHidden = true
            cell.lblQuantity.isHidden = true
            cell.btnDecrement.isHidden = true
            
        }
        
        cell.lblQuantity.text = "\(quantity!)"

        cell.lblTitle.text = (resultItem["product_name"] as! String)
        cell.lblDesc.text = (resultItem["product_description"] as! String)
        //cell.lblItemPrice.text = "\(StringFile.strPriceSymbol[kAppDelegate.userCurrentLanguage])\((resultItem["price"] as! String))"
        let doubleprice = Double(resultItem["price"] as! String)
        cell.lblPrice.text = "\(StringFile.strPriceSymbol[kAppDelegate.userCurrentLanguage])\(doubleprice ?? 0.0)"

        let database =  try! SQLitePool.manager().initialize(database: "royalLaundry", withExtension: "sqlite")
        
        let strCountQuery = "SELECT * FROM cart_item WHERE service_id=\((resultItem["service_id"] as! String)) and category_id=\((resultItem["category_id"] as! String)) and subcategory_id=\((resultItem["subcategory_id"] as! String));"
        print(strCountQuery)

        let resultSelectAll = try! database.query(strCountQuery)

        
        cell.actionDescrementItemClosure = { [weak self] in
            print("item removed")
            
            let database =  try! SQLitePool.manager().initialize(database: "royalLaundry", withExtension: "sqlite")
            
            if resultSelectAll.affectedRowCount > 0 {
                
                let resultPrice = resultSelectAll.results![0]
                
                var quantity = Int(resultPrice["quantity"] as! String)
                
                quantity = quantity! - 1
                
                //                if itemsAdd[indexPath.row].userSelectedQuantity > 0{
                
                if quantity! > 0 {
                    
                    let strUpdateQuery = "UPDATE cart_item SET quantity = \(quantity!) WHERE service_id=\((resultItem["service_id"] as! String)) and category_id=\((resultItem["category_id"] as! String)) and subcategory_id=\((resultItem["subcategory_id"] as! String));"
                    //                    print(strUpdateQuery)
                    
                    database.query(strUpdateQuery, successClosure: { (result) in
                        
                        //                        print(result)
                        GlobalMethods.getBasketUpdate()
                        self!.getBasketItems()

                        
                    }) { (e) in
                        print(e)
                    }
                    
                } else {
                    
                    let strDeleteQuery = "DELETE from cart_item WHERE service_id=\((resultItem["service_id"] as! String)) and category_id=\((resultItem["category_id"] as! String)) and subcategory_id=\((resultItem["subcategory_id"] as! String));"
                    //                    print(strDeleteQuery)
                    
                    database.query(strDeleteQuery, successClosure: { (result) in
                        
                        //                        print(result)
                        
                        GlobalMethods.getBasketUpdate()
                        self!.getBasketItems()

                        
                    }) { (e) in
                        print(e)
                    }
                }
            }
            else{
                
                let strDeleteQuery = "DELETE from cart_item WHERE service_id=\((resultItem["service_id"] as! String)) and category_id=\((resultItem["category_id"] as! String)) and subcategory_id=\((resultItem["subcategory_id"] as! String));"
                //                    print(strDeleteQuery)
                
                database.query(strDeleteQuery, successClosure: { (result) in
                    
                    //                        print(result)
                    GlobalMethods.getBasketUpdate()
                    self!.getBasketItems()

                    
                }) { (e) in
                    print(e)
                }
            }
        }
        
        
        cell.actionIncrementItemClosure = { [weak self] in
            print("item added")
                            
            let database =  try! SQLitePool.manager().initialize(database: "royalLaundry", withExtension: "sqlite")
            
            let strQuery = "SELECT * FROM cart_item WHERE service_id=\((resultItem["service_id"] as! String)) and category_id=\((resultItem["category_id"] as! String)) and subcategory_id=\((resultItem["subcategory_id"] as! String));"
            //                print(strQuery)
            
            let result = try! database.query(strQuery)
            //                print(result)
            
            if result.affectedRowCount > 0 {
                
                let resultPrice = resultSelectAll.results![0]
                
                var quantity = Int(resultPrice["quantity"] as! String)
                
                quantity = quantity! + 1
                
                let strUpdateQuery = "UPDATE cart_item SET quantity = \(quantity!) WHERE service_id=\((resultItem["service_id"] as! String)) and category_id=\((resultItem["category_id"] as! String)) and subcategory_id=\((resultItem["subcategory_id"] as! String));"
                //                    print(strUpdateQuery)
                
                database.query(strUpdateQuery, successClosure: { (result) in
                    
                    //                        print(result)
                    GlobalMethods.getBasketUpdate()
                    self!.getBasketItems()
                }) { (e) in
                    print(e)
                }
            } else {
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

