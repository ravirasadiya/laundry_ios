//
//  OrderSummeryVC.swift
//  Royal Laundry
//
//  Created by Shine on 19/03/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import UIKit

class OrderSummeryVC: UIViewController {

    //MARK:Variables
    var arrItems = SQLiteDataArray()
    var line1:String!
    var line2:String!
    var postalcode:String!
    var collDate:String!
    var collTime:String!
    var delDate:String!
    var delTime:String!
    var basket:String!
    var price:Int!
    var deliverynote:String?
    
    var window: UIWindow?
    //MARK:Outlets
    
    @IBOutlet var viewAlert: UIView!
    @IBOutlet var imgAlertIcon: UIImageView!
    @IBOutlet var lblAlertTitle: UILabel!
    
    
    @IBOutlet var btnPlaceOrder: UIButton!
    @IBOutlet var lblBasket: UILabel!
    @IBOutlet var btnAddtoCalender: UIButton!
    @IBOutlet var lblDeliveryTime: UILabel!
    @IBOutlet var lblDeliveryDate: UILabel!
    
    @IBOutlet var lblCollectionTime: UILabel!
    @IBOutlet var lblCollectionDate: UILabel!
    @IBOutlet var lblPostalCode: UILabel!
    @IBOutlet var lblLine1: UILabel!
    @IBOutlet var lblLine2: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblLine1.text = self.line1
        self.lblLine2.text = self.line2
        self.lblPostalCode.text = self.postalcode
        self.lblCollectionDate.text = "collection,\(self.collDate ?? "")"
        self.lblCollectionTime.text = "deliivery,\(self.collTime ?? "")"
        self.lblDeliveryDate.text = self.delDate
        self.lblDeliveryTime.text = self.delTime
        self.lblBasket.text = self.basket
        
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPlaceOrderPressed(_ sender: UIButton) {
        let settingVC = SettingsViewController()
        let product = ""
        let price = kAppDelegate.basketPrice
        let checkoutViewController = CheckoutViewController(product: product,
                                                            price: Int(price),
                                                            settings: settingVC.settings)

        self.window = UIWindow(frame: UIScreen.main.bounds)
        var nav1 = UINavigationController()
        var mainView = checkoutViewController
        
        mainView.postcode = self.postalcode
        mainView.productID = self.arrItems[0]["product_id"] as! String
        mainView.serviceID = self.arrItems[0]["service_id"] as! String
        mainView.categoryID = self.arrItems[0]["category_id"] as! String
        mainView.subcategoryID = self.arrItems[0]["subcategory_id"] as! String
        mainView.count = self.arrItems[0]["quantity"] as! String
        mainView.productprice = self.arrItems[0]["price"] as! String
       mainView.coltime = self.collTime
        mainView.coldate = self.collDate
        mainView.delNote = self.deliverynote
        mainView.deltime = self.delTime
        mainView.deldate = self.delDate
        mainView.address = self.line1
        nav1.viewControllers = [mainView]
        self.window?.rootViewController = nav1
        self.window?.makeKeyAndVisible()
        // self.navigationController?.pushViewController(checkoutViewController, animated: true)
    }
    
    @IBAction func btnClosePressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
