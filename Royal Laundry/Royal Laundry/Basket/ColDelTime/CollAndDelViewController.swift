//
//  CollAndDelViewController.swift
//  DemoAPI
//
//  Created by Latitude Technolabs on 01/12/20.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

class CollAndDelViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource {

    //MARK:Variables
    var workDate:String?
    var workTime:String?
    var selectedday = String()
    var selectedIndex = Int ()
    var daysArray = ["S","M","T","W","T","F","S"]
    var daysNumberArray = ["1","2","3","4","5","6","7"]
    var oneHorursAraay = ["07.00 - 08.00","08.00 - 09.00","09.00 - 10.00","10.00 - 11.00","11.00 - 12.00","12.00 - 13.00","13.00 - 14.00","14.00 - 15.00","15.00 - 16.00","16.00 - 17.00","17.00 - 18.00","18.00 - 19.00","19.00 - 20.00","20.00 - 21.00","21.00 - 22.00","22.00 - 23.00"]
    var ecofriendlyhours = ["07.00 - 09.00","08.00 - 10.00","09.00 - 11.00","10.00 - 12.00","11.00 - 13.00","12.00 - 14.00","13.00 - 15.00","14.00 - 16.00","15.00 - 17.00","16.00 - 18.00","17.00 - 19.00","18.00 - 20.00","19.00 - 21.00","20.00 - 22.00","21.00 - 23.00","22.00 - 00.00"]
    var btnPressBool:Bool?
    var weeekchar:String?
    var postcode:String?
    
    var objCollectionWorkingTime = [WorkingTimeDataModel]()
    var objDeliveryWorkingTime = [WorkingTimeDataModel]()
    
    var arrayAvailableday = [Int]()
    var arraydates = [String]()
    var arrayworkinghours = [TimeDataModel]()
    
    var tbl:Bool?
    var isCollection:Bool?
    var time:String?
    var selectedDate:String?
    var didSelectItem: ((_ time: String,_ date:String,_ day:String,_ ischarge:Bool) -> Void)?
    
    var arrTime = [String]()
    var arrDate = [String]()
    
    var parentvc:BasketVC?
    var someValueToSend : String?
    //MARK:Outlets
    
    @IBOutlet var collDay: UICollectionView!
    @IBOutlet var tblHours: UITableView!
    @IBOutlet var lblTitileTop: UILabel!
    @IBOutlet var viewTop: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.001130390563, green: 0.6324652433, blue: 0.6175534129, alpha: 1)
        self.navigationController?.isNavigationBarHidden = true
        if isCollection == true{
            self.lblTitileTop.text = "Collection"
        }else{
            self.lblTitileTop.text = "Delivery"
        }
        //register tableviewview xib
        let nib = UINib(nibName: "HoursTableViewCell", bundle: nil)
        tblHours.register(nib, forCellReuseIdentifier: "HoursTableViewCell")
        //register collectionview xib
        let nibcoll = UINib(nibName: "WeeksCollectionViewCell", bundle: nil)
        collDay.register(nibcoll,forCellWithReuseIdentifier: "WeeksCollectionViewCell")
        
        collDay.delegate = self
        collDay.dataSource = self
        tblHours.delegate = self
        tblHours.dataSource = self
       // getWorkingTimeApi(withpostcode: "wc2n5du")


        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if isCollection == true{
            let day = Calendar.current.component(.weekday, from: Date()) - 1
            selectedIndex = day
        }else{
            let str = workDate?.prefix(2)
            let str1 = Int(String(str!))! + 1
            let s = workDate?.dropFirst(1)
            let mydate = "\(str1)" + (s?.suffix(max(s!.count - 1, 0)))!
            print(mydate)
            let day = Calendar.current.component(.weekday, from: Date()) - 1
            
            let index = day + 1
            if index == 6{
                selectedIndex = 0
            }else if index == 7{
                selectedIndex = 1
            }else if index == 8{
                selectedIndex = 2
            }
        }

        getWorkingTimeApi(withpostcode: "wc2n5du")
        if isCollection == false{
            self.getDeliveryTimeApi(withpostcode: "wc2n5du", collection_date: self.workDate!, collection_time:self.workTime!)
        }
        collDay.reloadData()
        tblHours.reloadData()
    }
    //MARK:Function
    func getDayOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }
    //MARK:Actions

    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        //self.removeFromParentViewController()
        //self.dismiss(animated: true, completion: nil)
        self.view.removeFromSuperview()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //Collectionview Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if objCollectionWorkingTime.count != 0 {
            return daysArray.count
        }else{
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeeksCollectionViewCell", for: indexPath as IndexPath) as! WeeksCollectionViewCell
        cell.lblWeekDay.text = daysArray[indexPath.row]
        cell.lblWeekNumber.text = daysNumberArray[indexPath.row]
        //cell.backgroundColor = selectedIndex == indexPath.row ? #colorLiteral(red: 0.8832678795, green: 0.4046355486, blue: 0.549364984, alpha: 1) : UIColor.white
        cell.lblWeekDay.textColor = selectedIndex == indexPath.row ? #colorLiteral(red: 0, green: 0.6012048721, blue: 0.5929524302, alpha: 1) : UIColor.black
        cell.lblWeekNumber.textColor = selectedIndex == indexPath.row ? #colorLiteral(red: 0, green: 0.6012048721, blue: 0.5929524302, alpha: 1) : UIColor.black
        let day = Calendar.current.component(.weekday, from: Date()) - 1
        /*if indexPath.row == day{
            cell.isSelected = true
            cell.backgroundColor = #colorLiteral(red: 0.8832678795, green: 0.4046355486, blue: 0.549364984, alpha: 1)
            cell.layer.cornerRadius = 5.0
            cell.lblWeekDay.textColor = UIColor.white
            cell.lblWeekNumber.textColor = UIColor.white
        }*/
        if isCollection == true{
            if objCollectionWorkingTime.count != 0{
                let obj = objCollectionWorkingTime[indexPath.row]
                let weekday = getDayOfWeek(obj.date)! - 1
                arrayAvailableday.append(weekday)
                arraydates.append(obj.date)
            }
            let element = arrayAvailableday.firstIndex(of: selectedIndex)
             let date = arraydates[element!]
             for data in objCollectionWorkingTime{
                 if date == data.date{
                     selectedDate = date
                     self.arrayworkinghours.removeAll()
                     self.arrayworkinghours = data.working_time
                     tblHours.reloadData()
                 }
             }
        }else{
//            if objDeliveryWorkingTime.count != 0{
//                let obj = objDeliveryWorkingTime[indexPath.row]
//                let weekday = getDayOfWeek(obj.date)! - 1
//                arrayAvailableday.append(weekday)
//                arraydates.append(obj.date)
//            }
//            let element = arrayAvailableday.firstIndex(of: selectedIndex)
//             let date = arraydates[element!]
//             for data in objDeliveryWorkingTime{
//                 if date == data.date{
//                     selectedDate = date
//                     self.arrayworkinghours.removeAll()
//                     self.arrayworkinghours = data.working_time
//                     tblHours.reloadData()
//                 }
//             }

            }
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isCollection == true{
            if let element =  arrayAvailableday.firstIndex(of: indexPath.row){
                selectedIndex = indexPath.row
                tbl = true
                let date = arraydates[element]
                for data in objCollectionWorkingTime{
                    if date == data.date{
                        selectedDate = date
                        self.arrayworkinghours.removeAll()
                        self.arrayworkinghours = data.working_time
                        tblHours.reloadData()
                    }
                }
            }
        }else{
            if let element =  arrayAvailableday.firstIndex(of: indexPath.row){
                selectedIndex = indexPath.row
                tbl = true
                let date = arraydates[element]
                for data in objDeliveryWorkingTime{
                    if date == data.date{
                        selectedDate = date
                        self.arrayworkinghours.removeAll()
                        self.arrayworkinghours = data.working_time
                        tblHours.reloadData()
                    }
                }
            }
        }
        self.collDay.reloadData()
    }
 
    //Tableview Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

//        if tbl == true{
//            return arrayworkinghours.count
//        }else{
//            return ecofriendlyhours.count
//        }
        if arrayworkinghours.count != 0{
            return arrayworkinghours.count
        }else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HoursTableViewCell") as! HoursTableViewCell
            cell.lblCharge.layer.cornerRadius = 5.0
        //let unique = Array(Set(arrTime))
        cell.lblCharge.text = "Free"
     //   if isCollection == true{
        
            cell.lblHoursSlot.text = arrayworkinghours[indexPath.row].working_hr
            time = cell.lblHoursSlot.text
            arrTime.append(cell.lblHoursSlot.text!)
        //}

        
//                cell.lblHoursSlot.text = ecofriendlyhours[indexPath.row]
//                time = cell.lblHoursSlot.text
//                cell.lblCharge.text = "Free"
//                arrTime.append(cell.lblHoursSlot.text!)
                return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // if isCollection == true{
            let selectdtime = arrTime[indexPath.row]
            if selectedDate == nil{
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/yyyy"
                let result = formatter.string(from: date)
               selectedDate = result
            }
            let day = getDayOfWeek(selectedDate!)! - 1
             //daysArray[day]
            if day == 1{
                selectedday = "Monday"
            }else if day == 2{
                selectedday = "Tuesday"
            }else if day == 3{
                selectedday = "Wednesday"
            }else if day == 4{
                selectedday = "Thursday"
            }else if day == 5{
                selectedday = "Friday"
            }else if day == 6{
                selectedday = "Saturday"
            }else if day == 7{
                selectedday = "Sunday"
            }
            let boolValue:Bool!
            if btnPressBool == true || isCollection == true{
                boolValue = true
            }
            else{
                boolValue = false
            }
        self.workDate = selectedDate
        self.workTime = selectdtime
            didSelectItem!(selectdtime,selectedDate!,selectedday,boolValue)
            self.view.removeFromSuperview()

      //  }else{
            
       //     didSelectItem!("10.00-11.0","07/11/2020","Friday",false)
        //}
    }

    //MARK: - Web Services
    
    func getWorkingTimeApi(withpostcode postcode: String){
        
        WebServiceCall.callMethodWithURL(route: APIRouter.GetWorkingTime(withpostcode: postcode)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            if (responseValue != nil) {
                
                let responseModel = WorkingTimeModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200 {
                    
                    print(responseModel.data)
                    
                    self.objCollectionWorkingTime = responseModel.data
                    self.collDay.reloadData()
                    self.tblHours.reloadData()
                    
                  //  self.lblDay.text = self.objCollectionWorkingTime.data.first!.date
                   // self.lblTime.text = self.objCollectionWorkingTime.data.first!.working_time.first!.working_hr
                    

                    
//                    self.getDeliveryTimeApi(withpostcode: postcode, collection_date: self.objCollectionWorkingTime.first!.date, collection_time:self.objCollectionWorkingTime.first!.working_time.first!.working_hr)
                    
                }
                else {
                    //  GlobalMethods.showAlert(alertTitle: stringfile.strError[kAppDelegate.userCurrentLanguage], alertMessage: stringfile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.dismissLoaderView()
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    func getDeliveryTimeApi(withpostcode postcode: String, collection_date: String, collection_time: String){
        
        WebServiceCall.callMethodWithURL(route: APIRouter.GetDeliveryTime(withpostcode: postcode, collection_date: collection_date, collection_time: collection_time)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            if (responseValue != nil) {
                
                let responseModel = WorkingTimeModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200 {
                    print(responseModel.data)
                    self.objDeliveryWorkingTime = responseModel.data
                    for obj in self.objDeliveryWorkingTime{
                        if self.objDeliveryWorkingTime.count != 0{
                            
                            let weekday = self.getDayOfWeek(obj.date)! - 1
                            self.arrayAvailableday.append(weekday)
                            self.arraydates.append(obj.date)
                        }
                    }
                   // let obj = objDeliveryWorkingTime[indexPath.row]
                
                    let element = self.arrayAvailableday.firstIndex(of: self.selectedIndex)
                    let date = self.arraydates[element ?? 0]
                    for data in self.objDeliveryWorkingTime{
                         if date == data.date{
                            self.selectedDate = date
                             self.arrayworkinghours.removeAll()
                             self.arrayworkinghours = data.working_time
                            self.tblHours.reloadData()
                         }
                     }
                    self.collDay.reloadData()
                    self.tblHours.reloadData()
                    
                  //  self.lblDeliveryDate.text = self.objDeliveryWorkingTime.data.first!.date
                   // self.lblDeliveryTimePeriod.text = self.objDeliveryWorkingTime.data.first!.working_time.first!.working_hr
                }
                else {
                      // self.ShowAlertDisplay(titleObj: stringfile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: stringfile.msgInvalidPostCode[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
                }
            }
            else {
                GlobalMethods.dismissLoaderView()
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
}
