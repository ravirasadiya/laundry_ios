//
//  StringFile.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/18/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class StringFile: NSObject {
    
    static let strAppName = ["Royal Laundry","",""]
    
    static let strError = ["Error","",""]
    static let strSorry = ["Sorry","",""]
    static let strThankYou = ["Thank You","",""]
    static let strSuccess = ["Success","",""]

    static let strAlert = ["Alert", "", ""]
    static let strUnknown_error = ["Unknown error found", "", ""]
    static let MsgNoInternet = ["Internet not available", "", ""]
    
    static let msgNeedProperDetails = ["Something missing can you please try again with proper details", "", ""]
    static let msgSomthingWrong = ["Something went wrong", "", ""]
    static let strOK = ["OK", "", ""]

    
    static let msgAlreadyAccount = ["Already have an account? Sign in","",""]
    static let msgContinueToTheApp = ["Continue to the app","",""]
    static let strApply = ["APPLY","",""]
    static let strIDontHaveCode = ["I DON'T HAVE A CODE","",""]
    static let strRetry = ["RETRY","",""]

    static let strPriceSymbol = ["£","£","£"]

    static let strSecondsRemaining = ["Time Remaining","",""]

    static let msgEnterEnterCode = ["Please enter code","कृपया कोड दर्ज करें","કૃપા કરી કોડ દાખલ કરો"]
    
    static let customernotfound = "Customer not found"

}
