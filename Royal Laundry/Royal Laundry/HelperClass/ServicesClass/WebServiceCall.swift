//
//  WebServiceCall.swift

import UIKit
import Alamofire
import Keychain


var LogAuthHeader = false
var LogRequest = true
var LogResponse = true
var LogEnable = false

func LoggedInAuthToken() -> String {
    
    guard let authToken = Keychain.load(ParameterKeys.AuthToken) else { return "" }
    return authToken
}

class WebServiceCall: NSObject {
    
    class func callMethodWithURL(route : APIRouter, completion: @escaping (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) -> ()) {
        
        var headers: HTTPHeaders? = [ParameterKeys.HeaderFields.DeviceType : "\(DeviceType.iOS.rawValue)", ParameterKeys.HeaderFields.AppVersion : appVersion ?? "", ParameterKeys.HeaderFields.OSVersion : osVersion]
        
        let authToken = LoggedInAuthToken()
        if !authToken.isEmpty {
            headers?[ParameterKeys.HeaderFields.Authorization] = "Bearer " + authToken
        }
        
        if LogAuthHeader {
            print("\(String(describing: headers))")
        }
        
        var apiURL = route.baseURL + route.path + route.queryString
        apiURL = apiURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        if LogRequest {
            print("URL :- \(apiURL)")
            print("\n/*~~~~~  ~~~~~*/\n")
            print("Method Type :- \(String(describing: route.method))")
            print("\n/*~~~~~  ~~~~~*/\n")
            print("Parameters :- \(String(describing: route.parameters))")
        }
        
        Alamofire.request(apiURL, method: route.method, parameters: route.parameters, encoding: JSONEncoding.default, headers: headers).validate(contentType: ["application/json", "text/json", "text/javascript", "application/x-www-form-urlencoded", "text/html", "appliletion/javascript", "text/plain", "text/plain; charset=utf-8"]).responseJSON { response in
            
            switch(response.result) {
            case .success(let JSON):
                
                if LogResponse {
                    print("Response Code :- \(String(describing: (response.response?.statusCode)!))")
                    print("\n/*~~~~~  ~~~~~*/\n")
                    print("Response Value :- \(String(describing: JSON))")
                }
                
                if let dict = JSON as? [String : AnyObject] {
                    
                    if  let status = dict["status"] as? Int, status == 111 {
                        GlobalMethods.dismissLoaderView()
                        cancelAllAPIRequest()
                        NotificationCenter.default.post(name: NSNotification.Name(kLogoutMoveLoginPage), object: nil)
                        return
                    }
                }
                
                completion((response.response?.statusCode)!, JSON as? [String : AnyObject], nil)
                break
            case .failure(let error):
                
                if LogResponse {
                    print("Error Value :- \(String(describing: error))")
                }
                
                if response.response?.statusCode == 200 {
                    completion((response.response?.statusCode)!, nil, nil)
                }
                else{
                    completion(0, nil, error)
                }
                break
            }
        }
    }
    
    class func uploadImageWithURL(route : APIRouter, imageArray : [ImageUploadModel?]?, completion: @escaping (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) -> ()) {
        
        var headers: HTTPHeaders? = [ParameterKeys.HeaderFields.DeviceType : "\(DeviceType.iOS.rawValue)", ParameterKeys.HeaderFields.AppVersion : appVersion ?? "", ParameterKeys.HeaderFields.OSVersion : osVersion]
        
        let authToken = LoggedInAuthToken()
        if !authToken.isEmpty {
            headers?[ParameterKeys.HeaderFields.Authorization] = "Bearer " + authToken
        }
        
        if LogAuthHeader {
            print("\(String(describing: headers))")
        }
        
        var apiURL = route.baseURL + route.path + route.queryString
        apiURL = apiURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        if LogRequest {
            print("URL :- \(apiURL)")
            print("\n/*~~~~~  ~~~~~*/\n")
            print("Method Type :- \(String(describing: route.method))")
            print("\n/*~~~~~  ~~~~~*/\n")
            print("Parameters :- \(String(describing: route.parameters))")
        }

        Alamofire.upload(multipartFormData: { (multipartFormData) in
                    
            if imageArray != nil {
                
                for imgUploadModel in imageArray! {
                    let imgMultipleData = imgUploadModel!.Image.jpegData(compressionQuality: 0.5)!
                    multipartFormData.append(imgMultipleData, withName: imgUploadModel!.ImageKey, fileName: "file.jpeg", mimeType: "image/jpeg")
                }
            }
        
            if route.parameters != nil {
                
                for (key, value) in route.parameters!
                {
                   multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }
            
            }, usingThreshold: UInt64.init(), to: apiURL, method: route.method, headers: headers) { encodingResult in

            switch encodingResult {

            case .success(let upload, _, _):

                upload.responseJSON { response in

                    completion((response.response?.statusCode)!, response.result.value as? [String : AnyObject], nil)

                } case .failure(let error):
                    completion(0, nil, error)
            }
        }
    }
    
    //MARK: - Cancel API request
    class func cancelAPIRequest(arryLastPathComponents : Array<String>) {
        
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (dataTasks, uploadTasks, downloadTasks) in
            
            for dataTask in dataTasks {
                
                if let lastPathComponent = dataTask.currentRequest?.url?.lastPathComponent {
                    if arryLastPathComponents.contains(lastPathComponent) {
                        dataTask.cancel()
                        
                        if LogEnable {
                            print("Request canceled :- \(lastPathComponent)")
                        }
                    }
                }
            }
        }
    }
    
    class func cancelAllAPIRequest() {
        
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (dataTasks, uploadTasks, downloadTasks) in
            
            for dataTask in dataTasks {
                dataTask.cancel()
            }
            
            for uploadTask in uploadTasks {
                uploadTask.cancel()
            }
            
            for downloadTask in downloadTasks {
                downloadTask.cancel()
            }
        }
    }

}
