//
//  CheckBoxButton.swift
//  Royal Laundry
//
//  Created by Shine on 21/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import Foundation
import UIKit

class CheckBoxButton: UIButton {
    let checkedImage = #imageLiteral(resourceName: "check") //UIImage(named: system)! as UIImage
    let uncheckedImage = #imageLiteral(resourceName: "uncheck") //UIImage(named: "uncheck")! as UIImage
    
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(checkedImage, for: UIControl.State.normal)
            } else {
                self.setImage(uncheckedImage, for: UIControl.State.normal)
            }
        }
    }
    override func awakeFromNib() {
        self.addTarget(self, action:#selector(buttonClicked(sender:)), for: UIControl.Event.touchUpInside)
        self.isChecked = false
    }
    
    @objc func buttonClicked(sender: UIButton) {
        if sender == self {
            isChecked = !isChecked
        }
    }
}
