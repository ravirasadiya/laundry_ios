//
//  LaundryItemCell.swift
//  Royal Laundry
//
//  Created by Shine on 12/10/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import QuartzCore

class LaundryItemCell: UICollectionViewCell {

    
    @IBOutlet weak var viewCellBG: UIView!
    
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var viewPrice: UIView!
    @IBOutlet weak var lblPrice: UILabel!
    
    @IBOutlet weak var viewItemTitle: UIView!
    @IBOutlet weak var lblItemTitle: UILabel!
    
    @IBOutlet weak var viewOffer: UIView!
    @IBOutlet weak var lblOffer: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
