//
//  WeeksCollectionViewCell.swift
//  DemoAPI
//
//  Created by Latitude Technolabs on 02/12/20.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

class WeeksCollectionViewCell: UICollectionViewCell {

    //MARK:Outlets
    
    @IBOutlet var lblWeekNumber: UILabel!
    @IBOutlet var lblWeekDay: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
