//
//  ProfileVC.swift
//  Royal Laundry
//
//  Created by Shine on 02/01/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import UIKit
import iOSDropDown

class ProfileVC: UIViewController,UITextFieldDelegate {

    //MARK:Outlets
    
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var txtEditMobile: UITextField!
    @IBOutlet weak var txtEditCountryCodde: DropDown!
    @IBOutlet weak var viewEditPhone: UIView!
    @IBOutlet weak var txtEditGender: DropDown!
    @IBOutlet weak var viewEditGender: UIView!
    @IBOutlet weak var txtEditEmail: UITextField!
    @IBOutlet weak var viewEditEmail: UIView!
    @IBOutlet weak var txtEditName: UITextField!
    @IBOutlet weak var viewEditName: UIView!
    @IBOutlet weak var viewEdit: UIView!
    @IBOutlet var imgPhoneHeight: NSLayoutConstraint!
    @IBOutlet var viewLocationHeight: NSLayoutConstraint!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnSave: UIButton!
    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var txtGender: UITextField!
    @IBOutlet var imgGender: UIImageView!
    @IBOutlet var txtPhone: UITextField!
    @IBOutlet var imgPhone: UIImageView!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var imgEmail: UIImageView!
    @IBOutlet var txtLocation: UITextField!
    @IBOutlet var imgLocation: UIImageView!
    @IBOutlet var txtFullName: UITextField!
    @IBOutlet var imgFname: UIImageView!
    @IBOutlet var viewButton: UIView!
    @IBOutlet var viewGender: UIView!
    @IBOutlet var viewPhone: UIView!
    @IBOutlet var viewEmail: UIView!
    @IBOutlet var viewLocation: UIView!
    @IBOutlet var viewName: UIView!
    @IBOutlet var lblTitleTop: UILabel!
    @IBOutlet var viewTop: UIView!
    @IBOutlet weak var btnLogout: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        SetUpInitialView()
    }
    //MARK:Functions
    func SetUpInitialView(){
        viewStatus.backgroundColor = kAppThemePrimaryDarkColor
        viewTop.backgroundColor = kAppThemePrimaryLightColor
        
        viewEdit.isHidden = true
        
        btnEdit.backgroundColor = kAppButtonBGActiveColor
        btnEdit.setTitleColor(kAppButtonTextActiveColor, for: .normal)
        btnLogout.backgroundColor = kAppButtonBGActiveColor
        btnLogout.setTitleColor(kAppButtonTextActiveColor, for: .normal)
        btnSave.backgroundColor = kAppButtonBGActiveColor
        btnSave.setTitleColor(kAppButtonTextActiveColor, for: .normal)
        btnCancel.backgroundColor = kAppButtonBGActiveColor
        btnCancel.setTitleColor(kAppButtonTextActiveColor, for: .normal)
        
        btnEdit.layer.cornerRadius = 5.0
        btnSave.layer.cornerRadius = 5.0
        btnCancel.layer.cornerRadius = 5.0
        btnLogout.layer.cornerRadius = 5.0
        
        txtFullName.isUserInteractionEnabled = false
        txtLocation.isUserInteractionEnabled = false
        txtEmail.isUserInteractionEnabled = false
        txtPhone.isUserInteractionEnabled = false
        txtGender.isUserInteractionEnabled = false
        dropdown()
        
        let name: String = GlobalMethods.getUserDefaultObjectForKey(key: MyUserDefaultsKeys.username) as! String
        let location: String = GlobalMethods.getUserDefaultObjectForKey(key: MyUserDefaultsKeys.useraddress) as! String
        let email: String = GlobalMethods.getUserDefaultObjectForKey(key: MyUserDefaultsKeys.useremail) as! String
        let phone: String = GlobalMethods.getUserDefaultObjectForKey(key: MyUserDefaultsKeys.usermobile) as! String
        let gender: String = GlobalMethods.getUserDefaultObjectForKey(key: MyUserDefaultsKeys.usergender) as! String
        txtFullName.text = name
        txtLocation.text = location
        txtEmail.text = email
        txtPhone.text = phone
        txtGender.text = gender
        
        txtEditName.text = name
        txtEditEmail.text = email
        txtEditMobile.text = phone
        txtEditGender.text = gender
        
        txtEditCountryCodde.delegate = self
        txtEditMobile.delegate = self
        txtEditGender.delegate = self
        txtEditMobile.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
    }
    @objc func textFieldDidChange(_ textField: UITextField) {

    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtEditGender.resignFirstResponder()
        txtEditCountryCodde.resignFirstResponder()
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtEditGender{
            txtEditGender.resignFirstResponder()
            return false
        }
        if textField == txtEditCountryCodde{
            txtEditCountryCodde.resignFirstResponder()
            return false
        }
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        if (textField.text?.count)! > 0 && textField != txtEditMobile{
            return false
        }
        if textField == txtEditMobile{
            return range.location < 15
        }
        return string.isNumber
    }


    func dropdown(){
        txtEditMobile.keyboardType = .phonePad
        txtEditCountryCodde.optionArray = ["+91","+41","+44","+1","+61","+33","+49","+971"]
        txtEditCountryCodde.didSelect{(selectedText , index ,id) in
            self.txtEditCountryCodde.text = selectedText
        }
        txtEditGender.optionArray = ["Male","Female"]
        txtEditGender.didSelect{(selectedText , index ,id) in
            self.txtEditGender.text = selectedText
        }
    }
    func showToast(message : String) {

        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 250, height: 35))
        toastLabel.center.x = self.view.center.x
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    //MARK:Actions
    
    @IBAction func btnLogOutPressed(_ sender: Any) {
        GlobalMethods.setUserDefaultObject(object: false as AnyObject, key: MyUserDefaultsKeys.isloginsucess)
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.presentInFullScreen(objVC, animated: true)
    }
    
    @IBAction func btnEditPressed(_ sender: UIButton) {
        viewEdit.isHidden = false
    }
    @IBAction func btnCancelPressed(_ sender: UIButton) {
        viewEdit.isHidden = true
    }
    
    @IBAction func btnSavePressed(_ sender: UIButton) {
        if txtEditMobile.text!.count < 6{
            showToast(message: "Please Enter Valid Number")
        }
        if txtEditName.text!.isEmpty || txtEditEmail.text!.isEmpty || txtEditMobile.text!.isEmpty || txtEditGender.text!.isEmpty || txtEditCountryCodde.text!.isEmpty{
            showToast(message: "Please Fill Empty Field")
            return
        }else{
            UpdateProfile()
        }
        
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
//        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "MainTabVC") as! acco
//        self.presentInFullScreen(objVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:Web API
    func UpdateProfile(){

//        let reqParam = [ParameterKeys.Id:64,ParameterKeys.Name :txtEditName.text,ParameterKeys.Email:txtEditEmail.text,ParameterKeys.Mobile:txtEditMobile.text,ParameterKeys.Status: 0,ParameterKeys.Postcode:"wc2n5du",ParameterKeys.Address:txtLocation.text,ParameterKeys.Gender:txtEditGender.text,ParameterKeys.CountryCode:txtEditCountryCodde.text] as [String : Any]
        let reqParam = [ParameterKeys.Id:64,ParameterKeys.Name :txtEditName.text,ParameterKeys.Email:txtEditEmail.text,ParameterKeys.Mobile:txtEditMobile.text,ParameterKeys.Status: 0,ParameterKeys.Postcode:"wc2n5du",ParameterKeys.Address:txtLocation.text,ParameterKeys.Gender:txtEditGender.text,ParameterKeys.CountryCode:txtEditCountryCodde.text] as [String : Any]
        
        WebServiceCall.callMethodWithURL(route: APIRouter.UpdateProfile(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            if (responseValue != nil) {
                let responseModel = UpdateProfileModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200  {
     
                    print("Updated sucessfully")
                    self.viewEdit.isHidden = true
                    self.txtFullName.text = responseModel.data[0].name
                    self.txtLocation.text = responseModel.data[0].address
                    self.txtEmail.text = responseModel.data[0].email
                    self.txtPhone.text = responseModel.data[0].mobile
                    self.txtGender.text = responseModel.data[0].gender
                    
                    self.txtEditName.text = responseModel.data[0].name
                    self.txtEditEmail.text = responseModel.data[0].email
                    self.txtEditMobile.text = responseModel.data[0].mobile
                    self.txtEditGender.text = responseModel.data[0].gender
                    

                }
            }
            else {
                
                GlobalMethods.dismissLoaderView()
                 GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    
}
