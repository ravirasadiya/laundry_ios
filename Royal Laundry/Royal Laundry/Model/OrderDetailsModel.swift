//
//  OrderDetailsModel.swift
//  Royal Laundry
//
//  Created by Shine on 19/03/21.
//  Copyright © 2021 Shine. All rights reserved.
//


import UIKit
import EVReflection

class OrderDetailsModel: EVObject {
    var status : Int = 0
    var message = ""
    var data = [OrderDetailsDataModel]()
    
}

class OrderDetailsDataModel: EVObject {
    var id: Int = 0
    var collection_time = ""
    var total = ""
    var status = ""
    var collection_date = ""
    var delivery_time = ""
    var delivery_date = ""
    var deliveryaddress = ""
    var invoice = ""
    var payment_type = ""
    var payment_status = ""
    var voucher_code = ""
    var voucher = ""
    var voucher_total = ""
    var voucher_type = ""
    var referral_total = ""
    var referral = ""
    var cart = [CartDataModel]()
    
    override public func initValidation(_ dict: NSDictionary) {
        if let sValue = dict["cart"] as? NSMutableArray {
            self.cart = sValue as! [CartDataModel]
        }
    }
    
}

class CartDataModel: EVObject {
    
    var id:Int = 0
    var product_name = ""
    var product_price = ""
    var count = ""
}
