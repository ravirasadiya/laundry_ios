//
//  ImageUploadModel.swift
//  ClipDart
//
//  Created by Chirag Patel on 05/12/19.
//  Copyright © 2019 ClipDart Inc. All rights reserved.
//

import UIKit
import EVReflection

class ImageUploadModel: EVObject {
    
    var Image : UIImage!
    var ImageKey : String = ""

}
