//
//  VoucherCodeModel.swift
//  Royal Laundry
//
//  Created by Shine on 24/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import EVReflection

class VoucherCodeModel: EVObject {
    var status : Int = 0
    var message = ""
    var data = [VoucherCodeDataModel]()
    
}

class VoucherCodeDataModel: EVObject {
    var id:Int = 0
    var type = ""
    var voucher = ""
    var voucher_code = ""
    var total = ""
    var voucher_total = ""
    var voucher_discount = ""
}
