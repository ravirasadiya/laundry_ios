//
//  FaqModel.swift
//  Royal Laundry
//
//  Created by Shine on 13/10/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import EVReflection

class FaqModel: EVObject {
    var status : Int = 0
    var message = ""
    var data = [FaqDataModel]()

}

class FaqDataModel: EVObject {
    var Que = ""
    var Ans = ""
}
