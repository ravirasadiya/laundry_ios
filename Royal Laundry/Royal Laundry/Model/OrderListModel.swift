//
//  OrderListModel.swift
//  Royal Laundry
//
//  Created by Shine on 19/03/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import UIKit
import EVReflection

class OrderListModel: EVObject {
    var status : Int = 0
    var message = ""
    var data = [OrderListDataModel]()
    
}

class OrderListDataModel: EVObject {
    
    var id : Int = 0
    var status = ""
    var date = ""
    var total = ""
    
}
