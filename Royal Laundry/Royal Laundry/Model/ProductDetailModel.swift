//
//  ProductDetailModel.swift
//  Royal Laundry
//
//  Created by Shine on 13/10/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import EVReflection

class ProductDetailModel: EVObject {
    var status : Int = 0
    var message = ""
    var data = [ProductDetailDataModel]()
}

class ProductDetailDataModel: EVObject {

    var name = ""
    var category_name = ""
    var description1 = ""
    var price = ""
    var service = ""
    var offers = ""
    var status = ""
    var created_at = ""
    var updated_at = ""
    var offerprice = ""
    var id = ""
    var subcat_id = ""
    var service_name = ""
    var category_id = ""
    
    var userSelectedQuantity : Int = 0
    var totalAmount : Double = 0.0

    override public func initValidation(_ dict: NSDictionary) {
        if let sValue = dict["description"] as? String {
            self.description1 = sValue
        }
        
    }

}

class ProductSubCatModel: EVObject {
    var service_name = ""
    var catData = [ProductDetailDataModel]()
}

