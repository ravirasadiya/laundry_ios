//
//  VerifyOTPModel.swift
//  Royal Laundry
//
//  Created by Shine on 07/11/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import EVReflection

class VeriftOTPModel: EVObject {
    var status : Int = 0
    var message = ""
    var data = [VerifyOTPModelDataModel]()
    
}

class VerifyOTPModelDataModel: EVObject {
    var id:Int = 0
    var name = ""
    var email = ""
    var mobile = ""
    var country_code = ""
    var status = ""
    var address = ""
    var otp = ""
    var profile = ""
    var postcode = ""
    var gender = ""
    
}

