//
//  SignUpModel.swift
//  Royal Laundry
//
//  Created by Shine on 07/11/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import EVReflection

class SignUpModel: EVObject {
    var status : Int = 0
    var message = ""
    var data = [SignUpModelDataModel]()

}

class SignUpModelDataModel: EVObject {

}
