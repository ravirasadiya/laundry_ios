//
//  ProductModel.swift
//  Royal Laundry
//
//  Created by Shine on 12/10/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import EVReflection

class ProductModel: EVObject {
    var status : Int = 0
    var message = ""
    var data = [ProductDataModel]()

}

class ProductDataModel: EVObject {
    var id = ""
    var name = ""
    var description1 = ""
    var cover = ""
    var status = ""
    var categories = [ProductCatModel]()
    
    var imgUrl              : URL?
    
    override public func initValidation(_ dict: NSDictionary) {
        if let sValue = dict["description"] as? String {
            self.description1 = sValue
        }
        
        if let sValue = dict["image"] as? String {
            let fullUrl = "\(StaticURL.ImagePath)\(sValue)"
            self.imgUrl = URL(string: fullUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!)
            
        }

    }
    

    
}

class ProductCatModel: EVObject {
    var id = ""
    var name = ""
    var description1 = ""
    var status = ""
    var created_at = ""
    var updated_at = ""
    var product_id = ""
    var image = ""
    
    var imgUrl              : URL?
    
    override public func initValidation(_ dict: NSDictionary) {
        if let sValue = dict["description"] as? String {
            self.description1 = sValue
        }
        
        if let sValue = dict["image"] as? String {
            let fullUrl = "\(StaticURL.ImagePath)\(sValue)"
            self.imgUrl = URL(string: fullUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!)
            
        }

    }

}
