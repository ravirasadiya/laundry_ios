//
//  MakePaymentModel.swift
//  Royal Laundry
//
//  Created by Shine on 19/03/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import UIKit
import EVReflection

class MakePaymentModel: EVObject {
    var status : Int = 0
    var message = ""
    var data = [MakePaymentDataModel]()
    
}

class MakePaymentDataModel: EVObject {
    var order_id = ""
}
