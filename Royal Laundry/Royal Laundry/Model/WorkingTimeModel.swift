//
//  WorkingTimeModel.swift
//  DemoAPI
//
//  Created by Latitude Technolabs on 18/11/20.
//  Copyright © 2020 test. All rights reserved.
//


import UIKit
import EVReflection

class WorkingTimeModel: EVObject {
    var status : Int = 0
    var message = ""
    var data = [WorkingTimeDataModel]()
    
}

class WorkingTimeDataModel: EVObject {
    var date = ""
    var working_time = [TimeDataModel]()
    
    override public func initValidation(_ dict: NSDictionary) {
        if let sValue = dict["working time"] as? NSMutableArray {
            self.working_time = sValue as! [TimeDataModel]
        }
    }
    
}

class TimeDataModel: EVObject {
    
    var working_hr = ""
    var status = ""
    var working_hr_id = ""
    
}
