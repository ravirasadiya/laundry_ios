//
//  UpdateProfileModel.swift
//  Royal Laundry
//
//  Created by Shine on 19/03/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import UIKit
import EVReflection

class UpdateProfileModel: EVObject {
    var status : Int = 0
    var message = ""
    var data = [UpdateProfileDataModel]()
    
}

class UpdateProfileDataModel: EVObject {
    var id:Int = 0
    var name = ""
    var country_code = ""
    var mobile = ""
    var email = ""
    var status = ""
    var postcode = ""
    var address = ""
    var gender = ""
    var profile = ""
}
