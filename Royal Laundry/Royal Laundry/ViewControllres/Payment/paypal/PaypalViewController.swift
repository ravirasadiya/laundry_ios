//
//  PaypalViewController.swift
//  DemoAPI
//
//  Created by Latitude Technolabs on 25/11/20.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

class PaypalViewController: UIViewController,PayPalPaymentDelegate {
   

    var payPalConfig = PayPalConfiguration()
    var environment:String = PayPalEnvironmentNoNetwork{
        willSet(newEnvironment){
            if (newEnvironment != environment){
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    var acceptCreditCards:Bool = true{
        didSet{
            payPalConfig.acceptCreditCards = acceptCreditCards
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        payPalConfig.acceptCreditCards = acceptCreditCards
        payPalConfig.merchantName = "shineinfosoft"
        payPalConfig.languageOrLocale = NSLocale.preferredLanguages[0] as! String
        payPalConfig.payPalShippingAddressOption = .payPal
        PayPalMobile.preconnect(withEnvironment: environment)
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnPay(_ sender: UIButton) {
        
        var item1 = PayPalItem(name: "my items", withQuantity: 1, withPrice: NSDecimalNumber(string: "9.99"), withCurrency: "USD", withSku: "shineinfosoft-004")
        let items = [item1]
        let subtotal = PayPalItem.totalPrice(forItems: items)
        let shipping = NSDecimalNumber(string: "0.00")
        let tax = NSDecimalNumber(string: "0.00")
        let paymentdetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        let total = subtotal?.adding(shipping).adding(tax)
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "its shineinfosoft", intent: .sale)
        payment?.items = items
        payment?.paymentDetails = paymentdetails
        
        let paymentvc = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
        self.present(paymentvc!, animated: true, completion: nil)
        if (payment?.processable)!{
            let paymentvc = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            self.present(paymentvc!, animated: true, completion: nil)
        }else{
            print("payment not procebla\(payment)")
        }
    }
    
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController!) {
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController!, didComplete completedPayment: PayPalPayment!) {
        paymentViewController.dismiss(animated: true , completion: { () -> Void in
            print("payment \(completedPayment.confirmation)")
        })
    }
    

}
