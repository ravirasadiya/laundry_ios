//
//  CheckoutViewController.swift


import UIKit
import Stripe

class CheckoutViewController: UIViewController, STPPaymentContextDelegate {

    

    // 1) To get started with this demo, first head to https://dashboard.stripe.com/account/apikeys
    // and copy your "Test Publishable Key" (it looks like pk_test_abcdef) into the line below.
    let stripePublishableKey = "pk_test_gz44KxgHfxwAqXMKP0u13hX8"

    // 2) Next, optionally, to have this demo save your user's payment details, head to
    // https://github.com/stripe/example-ios-backend/tree/v13.0.3, click "Deploy to Heroku", and follow
    // the instructions (don't worry, it's free). Replace nil on the line below with your
    // Heroku URL (it looks like https://blazing-sunrise-1234.herokuapp.com ).
    let backendBaseURL: String? = "https://shrouded-cove-05224.herokuapp.com"
    //"https://shrouded-cove-05224.herokuapp.com"
    //"https://office-hours-one-time-payment.herokuapp.com/"
        //"https://office-hours-one-time-payment.herokuapp.com/"
    //"https://laundryroyal.herokuapp.com"
    //"https://morning-retreat-54013.herokuapp.com"
    // 3) Optionally, to enable Apple Pay, follow the instructions at https://stripe.com/docs/mobile/apple-pay
    // to create an Apple Merchant ID. Replace nil on the line below with it (it looks like merchant.com.yourappname).
    let appleMerchantID: String? = nil

    // These values will be shown to the user when they purchase with Apple Pay.
    let companyName = "Sylvia Grey"
    let paymentCurrency = "usd"

    let paymentContext: STPPaymentContext

    var postcode:String?
    var address:String?
    var coltime:String?
    var deltime:String?
    var coldate:String?
    var deldate:String?
    var delNote:String?
    
    var productID:String?
    var serviceID:String?
    var categoryID:String?
    var subcategoryID:String?
    var count:String?
    var productprice:String?

    var orderID:String?
    var totalprice:Int?
    var PaymentDic:[String:AnyObject] = [:]

    let theme: STPTheme
    let paymentRow: CheckoutRowView
    let shippingRow: CheckoutRowView
    let totalRow: CheckoutRowView
    let buyButton: BuyButton
    let rowHeight: CGFloat = 44
    let productImage = UILabel()
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    let numberFormatter: NumberFormatter
    let shippingString: String
    var product = ""
    var customerID:Int = GlobalMethods.getUserDefaultObjectForKey(key: MyUserDefaultsKeys.customerid) as! Int
    var paymentInProgress: Bool = false {
        didSet {

            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
                if self.paymentInProgress {
                    self.activityIndicator.startAnimating()
                    self.activityIndicator.alpha = 1
                    self.buyButton.alpha = 0
                }
                else {
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.alpha = 0
                    self.buyButton.alpha = 1
                }
                }, completion: nil)
        }
    }

    init(product: String, price: Int, settings: Settings) {

        self.totalprice = price
        let stripePublishableKey = self.stripePublishableKey
        let backendBaseURL = self.backendBaseURL

        assert(stripePublishableKey.hasPrefix("pk_"), "You must set your Stripe publishable key at the top of CheckoutViewController.swift to run this app.")
        assert(backendBaseURL != nil, "You must set your backend base url at the top of CheckoutViewController.swift to run this app.")

        self.product = product
        self.productImage.text = product
        self.theme = settings.theme
        MyAPIClient.sharedClient.baseURLString = self.backendBaseURL

        // This code is included here for the sake of readability, but in your application you should set up your configuration and theme earlier, preferably in your App Delegate.
        let config = STPPaymentConfiguration.shared()
        config.publishableKey = self.stripePublishableKey
        config.appleMerchantIdentifier = self.appleMerchantID
        config.companyName = self.companyName
        config.requiredBillingAddressFields = settings.requiredBillingAddressFields
        config.requiredShippingAddressFields = settings.requiredShippingAddressFields
        config.shippingType = settings.shippingType
        config.additionalPaymentOptions = settings.additionalPaymentOptions

        // Create card sources instead of card tokens
        //config.createCardSources = true;
        
        let customerContext = STPCustomerContext(keyProvider: MyAPIClient.sharedClient)
        let paymentContext = STPPaymentContext(customerContext: customerContext,
                                               configuration: config,
                                               theme: settings.theme)
        let userInformation = STPUserInformation()
        paymentContext.prefilledInformation = userInformation
        paymentContext.paymentAmount = price
        paymentContext.paymentCurrency = self.paymentCurrency

        let paymentSelectionFooter = PaymentContextFooterView(text: "")
        paymentSelectionFooter.theme = settings.theme
        paymentContext.paymentOptionsViewControllerFooterView = paymentSelectionFooter

        let addCardFooter = PaymentContextFooterView(text: "")
        addCardFooter.theme = settings.theme
        paymentContext.addCardViewControllerFooterView = addCardFooter

        self.paymentContext = paymentContext

        self.paymentRow = CheckoutRowView(title: "Payment", detail: "Select Payment",
                                          theme: settings.theme)
        var shippingString = "Contact"
        if config.requiredShippingAddressFields?.contains(.postalAddress) ?? false {
            shippingString = config.shippingType == .shipping ? "Shipping" : "Delivery"
        }
        self.shippingString = shippingString
        self.shippingRow = CheckoutRowView(title: self.shippingString,
                                           detail: "Enter \(self.shippingString) Info",
                                           theme: settings.theme)
        self.totalRow = CheckoutRowView(title: "Total", detail: "", tappable: false,
                                        theme: settings.theme)
        self.buyButton = BuyButton(enabled: true, theme: settings.theme)
        var localeComponents: [String: String] = [
            NSLocale.Key.currencyCode.rawValue: self.paymentCurrency,
        ]
        localeComponents[NSLocale.Key.languageCode.rawValue] = NSLocale.preferredLanguages.first
        let localeID = NSLocale.localeIdentifier(fromComponents: localeComponents)
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = Locale(identifier: localeID)
        numberFormatter.numberStyle = .currency
        numberFormatter.usesGroupingSeparator = true
        self.numberFormatter = numberFormatter
        super.init(nibName: nil, bundle: nil)
        self.paymentContext.delegate = self
        paymentContext.hostViewController = self
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let logoutBarButtonItem = UIBarButtonItem(title: "Back", style: .done, target: self, action: #selector(BackButton))
        self.navigationItem.leftBarButtonItem  = logoutBarButtonItem
        self.view.backgroundColor = self.theme.primaryBackgroundColor
        var red: CGFloat = 0
        self.theme.primaryBackgroundColor.getRed(&red, green: nil, blue: nil, alpha: nil)
        self.activityIndicator.style = red < 0.5 ? .white : .gray
        self.navigationItem.title = "Sylvia Grey"

        self.productImage.font = UIFont.systemFont(ofSize: 70)
        self.view.addSubview(self.totalRow)
        self.view.addSubview(self.paymentRow)
        self.view.addSubview(self.shippingRow)
        self.view.addSubview(self.productImage)
        self.view.addSubview(self.buyButton)
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.alpha = 0
        self.buyButton.addTarget(self, action: #selector(didTapBuy), for: .touchUpInside)
        self.totalRow.detail = self.numberFormatter.string(from: NSNumber(value: Float(self.paymentContext.paymentAmount)))!
        self.paymentRow.onTap = { [weak self] in
            self?.paymentContext.pushPaymentOptionsViewController()
        }
        self.shippingRow.onTap = { [weak self]  in
            self?.paymentContext.pushShippingViewController()
        }
    }

    @objc func BackButton(){
         print("clicked")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = storyboard.instantiateViewController(withIdentifier: "MainTabVC")
        self.navigationController?.pushViewController(VC, animated: true)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var insets = UIEdgeInsets.zero
        if #available(iOS 11.0, *) {
            insets = view.safeAreaInsets
        }
        let width = self.view.bounds.width - (insets.left + insets.right)
        self.productImage.sizeToFit()
        self.productImage.center = CGPoint(x: width/2.0,
                                           y: self.productImage.bounds.height/2.0 + rowHeight)
        self.paymentRow.frame = CGRect(x: insets.left, y: self.productImage.frame.maxY + rowHeight,
                                       width: width, height: rowHeight)
        self.shippingRow.frame = CGRect(x: insets.left, y: self.paymentRow.frame.maxY,
                                        width: width, height: rowHeight)
        self.totalRow.frame = CGRect(x: insets.left, y: self.shippingRow.frame.maxY,
                                     width: width, height: rowHeight)
        self.buyButton.frame = CGRect(x: insets.left, y: 0, width: 88, height: 44)
        self.buyButton.center = CGPoint(x: width/2.0, y: self.totalRow.frame.maxY + rowHeight*1.5)
        self.activityIndicator.center = self.buyButton.center
    }

    @objc func didTapBuy() {
        self.paymentInProgress = true
        self.paymentContext.requestPayment()
    }

    // MARK: STPPaymentContextDelegate

    func paymentContext(_ paymentContext: STPPaymentContext, didCreatePaymentResult paymentResult: STPPaymentResult, completion: @escaping STPPaymentStatusBlock) {
        MyAPIClient.sharedClient.completeCharge(paymentResult,
                                                amount: self.paymentContext.paymentAmount,
                                                shippingAddress: self.paymentContext.shippingAddress,
                                                shippingMethod: self.paymentContext.selectedShippingMethod){ result in
                                                    switch result {
                                                    case .success:
                                                        print("sucess")
                                                        completion(.success,nil)

                                                    case .failure(let error):
                                                        // A real app should retry this request if it was a network error.
                                                        print("Failed to create a Payment Intent: \(error)")
                                                        completion(.error, error)
                                                        break
                                                    }
                                                }
                                                
    }
    
    func paymentContext(_ paymentContext: STPPaymentContext, didCreatePaymentResult paymentResult: STPPaymentResult, completion: @escaping STPErrorBlock) {
//        MyAPIClient.sharedClient.completeCharge(paymentResult,
//                                                amount: self.paymentContext.paymentAmount,
//                                                shippingAddress: self.paymentContext.shippingAddress,
//                                                shippingMethod: self.paymentContext.selectedShippingMethod,
//                                                completion: completion)
    }

    func paymentContext(_ paymentContext: STPPaymentContext, didFinishWith status: STPPaymentStatus, error: Error?) {
        self.paymentInProgress = false
        let title: String
        let message: String
        switch status {
        case .error:
            title = "Error"
            message = error?.localizedDescription ?? ""
        case .success:
            title = "Success"
            message = "Payment sucessful"
            //"You bought a \(self.product)!"
        case .userCancellation:
            return
        }
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
            if message == "Payment sucessful"{
                self.make_payment()
            }
        
        })
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
    func paymentContextDidChange(_ paymentContext: STPPaymentContext) {
        self.paymentRow.loading = paymentContext.loading
        if let paymentMethod = paymentContext.selectedPaymentOption {
            self.paymentRow.detail = paymentMethod.label
        }
        else {
            self.paymentRow.detail = "Select Payment"
        }
        if let shippingMethod = paymentContext.selectedShippingMethod {
            self.shippingRow.detail = shippingMethod.label
        }
        else {
            self.shippingRow.detail = "Enter \(self.shippingString) Info"
            
        }
        self.totalRow.detail = self.numberFormatter.string(from: NSNumber(value: Float(self.paymentContext.paymentAmount)))!
    }

    func paymentContext(_ paymentContext: STPPaymentContext, didFailToLoadWithError error: Error) {
        
        let alertController = UIAlertController(
            title: "Error",
            message: error.localizedDescription,
            preferredStyle: .alert
        )
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            // Need to assign to _ because optional binding loses @discardableResult value
            // https://bugs.swift.org/browse/SR-1681
            _ = self.navigationController?.popViewController(animated: true)
        })
        let retry = UIAlertAction(title: "Retry", style: .default, handler: { action in
            self.paymentContext.retryLoading()
        })
        alertController.addAction(cancel)
        alertController.addAction(retry)
        self.present(alertController, animated: true, completion: nil)
 
    }

    // Note: this delegate method is optional. If you do not need to collect a
    // shipping method from your user, you should not implement this method.
    func paymentContext(_ paymentContext: STPPaymentContext, didUpdateShippingAddress address: STPAddress, completion: @escaping STPShippingMethodsCompletionBlock) {
        let upsGround = PKShippingMethod()
        upsGround.amount = 0
        upsGround.label = "UPS Ground"
        upsGround.detail = "Arrives in 3-5 days"
        upsGround.identifier = "ups_ground"
        let upsWorldwide = PKShippingMethod()
        upsWorldwide.amount = 0.0
        upsWorldwide.label = "UPS Worldwide Express"
        upsWorldwide.detail = "Arrives in 1-3 days"
        upsWorldwide.identifier = "ups_worldwide"
        let fedEx = PKShippingMethod()
        fedEx.amount = NSDecimalNumber(value: self.paymentContext.paymentAmount)//NSDecimalNumber(value: self.paymentContext.paymentAmount)
        fedEx.label = "FedEx"
        fedEx.detail = "Arrives tomorrow"
        fedEx.identifier = "fedex"

        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
        
            if address.country == nil || address.country == "US" {
                completion(.valid, nil, [upsGround, fedEx], fedEx)
            }
            else if address.country == "AQ" {
                let error = NSError(domain: "ShippingError", code: 123, userInfo: [NSLocalizedDescriptionKey: "Invalid Shipping Address",
                                                                                   NSLocalizedFailureReasonErrorKey: "We can't ship to this country."])
                completion(.invalid, error, nil, nil)
            }
            else {
                fedEx.amount = NSDecimalNumber(value: self.paymentContext.paymentAmount)
                completion(.valid, nil, [upsWorldwide, fedEx], fedEx)
            }
            
        }
    }

    func make_payment(){
        self.PaymentDic.removeAll()
        self.PaymentDic["payment_mode"] = "Stripe" as AnyObject
        self.PaymentDic["total"] = self.totalprice as AnyObject
        self.PaymentDic["transaction_id"] = "ch_1HtYnXApMOzu9OeQLLfSY1rW" as AnyObject
            //MyAPIClient.sharedClient.jsondata["id"] as AnyObject
        self.PaymentDic["payment_response"] = "" as AnyObject
        
        let datainfo = try! JSONSerialization.data(withJSONObject: PaymentDic)
        let base64Representation = datainfo.base64EncodedString()
        
        let x = base64Representation
        var newText = String()
        for (index, character) in x.enumerated() {
            if index != 0 && index % 76 == 0 {
                newText.append("\n")
            }
            newText.append(String(character))
        }

    let reqParam = [ParameterKeys.Customer_id :customerID, ParameterKeys.Para : newText] as [String : Any]
        
        WebServiceCall.callMethodWithURL(route: APIRouter.make_payment(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            if (responseValue != nil) {
                let responseModel = MakePaymentModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200  {
                    self.orderID = responseModel.data[0].order_id
                    
                     print("sucess to make_payment")
                    self.getPlaceOrderApi()
                }
            }
            else {
                
                GlobalMethods.dismissLoaderView()
                 //GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    func getPlaceOrderApi(){
        let reqParam = [ParameterKeys.Customer_id : customerID, ParameterKeys.Postcode : postcode ?? "",ParameterKeys.Address : address ?? "","collection_time":coltime ?? "",
                        "delivery_time":deltime ?? "",
                        "collection_date":coldate ?? "",
                        "delivery_date":deldate ?? "",
                        "delivery_note":"",
                        "cart":"[{\"productid\":\"63\",\"serviceid\":\"\(serviceID ?? "")\",\"categoryid\":\"\(categoryID ?? "")\",\"subcategoryid\":\"\(subcategoryID ?? "")\",\"count\":\"\(count ?? "")\",\"price\":\(productprice ?? "")}]",
                        "latitude":"",
                        "longitude":"",
                        "voucher_code":"",
                        "voucher_type":"",
                        "voucher":"",
                        "order_total":"",
                        "voucher_total":"",
                        "order_id":orderID ?? 0] as [String : Any]
        
        WebServiceCall.callMethodWithURL(route: APIRouter.PlaceOrder(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            if (responseValue != nil) {
                let responseModel = PlaceOrder(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200  {
                    print("sucess")
                    kAppDelegate.isPaymentSucess = true
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let VC = storyboard.instantiateViewController(withIdentifier: "MainTabVC")
                    self.navigationController?.pushViewController(VC, animated: true)
                }else{
                    print("errored status code")
                }
            }
            else {
                GlobalMethods.dismissLoaderView()
               // GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
}
