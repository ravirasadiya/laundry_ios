//
//  BackendAPIAdapter.swift


import Foundation
import Stripe
import Alamofire

class MyAPIClient: NSObject, STPCustomerEphemeralKeyProvider {

    
    var jsondata:[String:AnyObject] = [:]
    static let sharedClient = MyAPIClient()
    var baseURLString: String? = nil
    var baseURL: URL {
        if let urlString = self.baseURLString, let url = URL(string: urlString) {
            return url
        } else {
            fatalError()
        }
    }

//    func completeCharge(_ result: STPPaymentResult,
//                        amount: Int,
//                        shippingAddress: STPAddress?,
//                        shippingMethod: PKShippingMethod?,
//                        completion: @escaping STPErrorBlock) {
//        let url = self.baseURL.appendingPathComponent("charge")
//        var params: [String: Any] = [
//            "customer": "cus_EzwppKdSzgkWan",
//            "amount": amount,
//            "currency":"USD"
//        ]
//        params["shipping"] = STPAddress.shippingInfoForCharge(with: shippingAddress, shippingMethod: shippingMethod)
//        Alamofire.request(url, method: .post, parameters: params)
//            .validate(statusCode: 200..<300)
//            .responseString { response in
//                switch response.result {
//                case .success:
//                    completion(nil)
//                case .failure(let error):
//                    completion(error)
//                }
//        }
//    }
    func completeCharge(_ result: STPPaymentResult,
                        amount: Int,
                        shippingAddress: STPAddress?,
                        shippingMethod: PKShippingMethod?,
                        completion: @escaping ((Result<String>) -> Void)) {
        let url = self.baseURL.appendingPathComponent("charge")
        var params: [String: Any] = [
            "customer": "cus_EzwppKdSzgkWan",
            "amount": amount,
            "currency":"USD"
        ]
        params["shipping"] = STPAddress.shippingInfoForCharge(with: shippingAddress, shippingMethod: shippingMethod)
        Alamofire.request(url, method: .post, parameters: params)
            .validate(statusCode: 200..<300)
            .responseString { response in
                switch response.result {
                case .success(let sucess):
                    completion(.success(sucess))
                case .failure(let error):
                    completion(.failure(error))
                }
        }
    }
    func createCustomerKey(withAPIVersion apiVersion: String, completion: @escaping STPJSONResponseCompletionBlock) {
        let url = self.baseURL.appendingPathComponent("ephemeral_keys")
        Alamofire.request(url, method: .post, parameters: [
            "api_version": apiVersion,
            "customer_id":"cus_EzwppKdSzgkWan"
            ])
            .validate(statusCode: 200..<300)
            .responseJSON { responseJSON in
                switch responseJSON.result {
                case .success(let json):
                    completion(json as? [String: AnyObject], nil)
                    self.jsondata = (json as? [String:AnyObject])!
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }

}
