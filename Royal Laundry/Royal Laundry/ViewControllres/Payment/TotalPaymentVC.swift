//
//  TotalPaymentVC.swift
//  Royal Laundry
//
//  Created by Shine on 24/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit

class TotalPaymentVC: UIViewController {

    //MARK:Variables
    var arrItems = SQLiteDataArray()
    var line1:String!
    var line2:String!
    var postalcode:String!
    var collDate:String!
    var collTime:String!
    var delDate:String!
    var delTime:String!
    var basket:String!
    var price:Int!
    var deliverynote:String?
    
     var window: UIWindow?
    
    //MARK:Outlet
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var viewTop: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTop.backgroundColor = kAppThemePrimaryLightColor
        let pricedouble = Double(kAppDelegate.basketPrice)
        let str = "\(StringFile.strPriceSymbol[kAppDelegate.userCurrentLanguage])\(pricedouble)"
        self.lblTotalAmount.text = str
        
    }
    
    //MARK:Actions
    @IBAction func btnBackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func btnPaymentPressed(_ sender: Any) {
         let settingVC = SettingsViewController()
         let product = ""
         let price = kAppDelegate.basketPrice
         let checkoutViewController = CheckoutViewController(product: product,
                                                             price: Int(price),
                                                             settings: settingVC.settings)

         self.window = UIWindow(frame: UIScreen.main.bounds)
         var nav1 = UINavigationController()
         var mainView = checkoutViewController
         
         mainView.postcode = self.postalcode
         mainView.productID = self.arrItems[0]["product_id"] as! String
         mainView.serviceID = self.arrItems[0]["service_id"] as! String
         mainView.categoryID = self.arrItems[0]["category_id"] as! String
         mainView.subcategoryID = self.arrItems[0]["subcategory_id"] as! String
         mainView.count = self.arrItems[0]["quantity"] as! String
         mainView.productprice = self.arrItems[0]["price"] as! String
        mainView.coltime = self.collTime
         mainView.coldate = self.collDate
         mainView.delNote = self.deliverynote
         mainView.deltime = self.delTime
         mainView.deldate = self.delDate
         mainView.address = self.line1
         nav1.viewControllers = [mainView]
         self.window?.rootViewController = nav1
         self.window?.makeKeyAndVisible()
    }
    
}
