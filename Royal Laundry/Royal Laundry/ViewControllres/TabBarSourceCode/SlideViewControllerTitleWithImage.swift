//
//  SlideViewControllerTitleWithImage.swift
//  TabBar
//
//  Created by mohamed shaat on 6/23/19.
//  Copyright © 2019 shaat. All rights reserved.
//

import UIKit

class SlideViewControllerTitleWithImage: UIViewController, TabBarDataSourse {
    //MARK:- Outlets

    
    // MARK: - constants & variables
    let viewControllerTabTitle = ["TOPS", "LAUNDRY", "BEDDING", "DRYCLEAN", "TRAINERS", "DRESSES", "SPECIALIST", "LEATHER", "HOUSEHOLD", "MOSTPOPULAR"]
//    let viewControllerTabTitle = ["TOPS", "LAUNDRY", "BEDDING", "SUITS", "TROUSERS", "DRESSES", "OUTDOOR", "ACCESSORIES", "HOME", "BUSINESS"]
    let viewControllerTabImage = ["tops", "laundry", "bedding", "suits", "trousers", "dresses", "outdoor", "accessories", "home", "business"]

    
    
    //MARK:- UIView Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func cellTabName() -> String {
        return TabBarCollectionViewCellTitleWithImage.resusableName
    }
    
    func configureTabCell(collectionView: UICollectionView, indexpath:IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellTabName(), for: indexpath) as! TabBarCollectionViewCellTitleWithImage
        cell.imageView.image = UIImage(named: viewControllerTabImage[indexpath.row])
        cell.titleLabel.text = "\(viewControllerTabTitle[indexpath.row])"
        
//        let item = viewControllerTabTitle[indexpath.row]
//        let itemSize = item.size(withAttributes: [
//            NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14)
//        ])

        return cell
    }

}

