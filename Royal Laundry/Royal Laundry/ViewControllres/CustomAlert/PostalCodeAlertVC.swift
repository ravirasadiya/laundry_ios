//
//  PostalCodeAlertVC.swift
//  Royal Laundry
//
//  Created by Shine on 21/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class PostalCodeAlertVC: UIViewController {

    // MARK: - constants & variables
    var arrCodeData = [String]()
    var postalcode:String?
    //MARK:Outlets
    @IBOutlet weak var tblPostalCode: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        self.showAnimate()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        callGetPostcodeAPi()
    }
     func showAnimate()
     {
         self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
         self.view.alpha = 0.0;
         UIView.animate(withDuration: 0.25, animations: {
             self.view.alpha = 1.0
             self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
             
         });
     }
     
     func removeAnimate()
     {
         UIView.animate(withDuration: 0.25, animations: {
             self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
             self.view.alpha = 0.0;
             
         }, completion:{(finished : Bool)  in
             if (finished)
             {
                 self.view.removeFromSuperview()
             }
         });
    }
    
        func callGetPostcodeAPi()
        {
            let url = "https://api.getaddress.io/find/\(postalcode ?? "")?api-key=\(kPostcodeAthorisedkey)"
            
            Alamofire.request(url).validate().responseJSON { response in
                switch response.result {
                case .success:
                    
                    
                    let responseJSON = JSON(response.result.value!)
                    
                    print("Postcode response \(responseJSON)")
                    
                    if ((responseJSON.dictionaryObject) != nil)
                    {
                        let parsedData = JSON(responseJSON)
                        print("parsedData response \(parsedData)")

                        let addresses = parsedData["addresses"]
                        let longitude = parsedData["longitude"]
                        let latitude = parsedData["latitude"]
                        print("longitude latitude response \(longitude)\(latitude)")

                        
                        for add in addresses {
                                      
                            print("add response \(add)")
                            

                        }
                        if parsedData["addresses"].count != 0{
    //                        let str = parsedData["addresses"][0].string
    //                        let parsed = str?.replacingOccurrences(of: ", ,", with: ",")
                            self.arrCodeData.append(parsedData["addresses"][0].string ?? "")
                            self.arrCodeData.append(parsedData["addresses"][1].string ?? "")
                            self.arrCodeData.append(parsedData["addresses"][2].string ?? "")
                            self.arrCodeData.append(parsedData["addresses"][3].string ?? "")
                            self.arrCodeData.append(parsedData["addresses"][4].string ?? "")
                            self.arrCodeData.append(parsedData["addresses"][5].string ?? "")
                            self.arrCodeData.append(parsedData["addresses"][6].string ?? "")
                            //self.arrCodeData.removeAll(where: { $0 == ", ," })
                            self.tblPostalCode.reloadData()
                        }
                        
    //                    let objVC = self.storyboard?.instantiateViewController(withIdentifier: "ReferralCodeVC") as! ReferralCodeVC
    //                    self.navigationController?.pushViewController(objVC, animated: false)

                    }
                    else
                    {
                        
                    }
                case .failure(let error):
                    print(error)
                    
                    
                }
            }
            
        }
}
extension PostalCodeAlertVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCodeData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let str = arrCodeData[indexPath.row]
        let str1 = str.replacingOccurrences(of: ", ,", with: ",")
       // cell.textLabel?.text = str1.replacingOccurrences(of: ", ,", with: ",")
        cell.textLabel?.numberOfLines = 3
        cell.textLabel?.textColor = UIColor.darkGray
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.textLabel?.textAlignment = .center
        let labelFont = UIFont(name: "HelveticaNeue-Bold", size: 16)
        let attributes :Dictionary = [NSAttributedString.Key.font : labelFont]
        let attrString = NSAttributedString(string: str1.replacingOccurrences(of: ", ,", with: ","), attributes:attributes as [NSAttributedString.Key : Any])
        cell.textLabel?.attributedText = attrString
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(arrCodeData[indexPath.row])
        GlobalMethods.setUserDefaultObject(object: arrCodeData[indexPath.row] as AnyObject, key: MyUserDefaultsKeys.PostalCodeData)
        self.view.removeFromSuperview()
        
    }
}

