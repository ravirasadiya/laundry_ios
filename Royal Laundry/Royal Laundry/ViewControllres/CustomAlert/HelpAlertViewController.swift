//
//  HelpAlertViewController.swift
//  DemoAPI
//
//  Created by Latitude Technolabs on 24/11/20.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

class HelpAlertViewController: UIViewController {

    //MARK:Outlets

    @IBOutlet var imgHelp: UIImageView!
    @IBOutlet var lblViewTitle: UILabel!
    @IBOutlet var viewHelp: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.addTapGesture(tapNumber: 1, target: self, action: #selector(close))
        self.showAnimate()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:Actions
    @IBAction func btnClosePressed(_ sender: UIButton) {
        self.view.removeFromSuperview()
       // self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnSENDEMAILPressed(_ sender: UIButton) {
    }
    @IBAction func btnCHATPressed(_ sender: UIButton) {
    }
    
    @IBAction func btnFAQPressed(_ sender: UIButton) {
        
//        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
//        objVC.docurl =  "https://support.laundrapp.com/hc/en-us?mobile_site=true"
//        self.presentInFullScreen(objVC, animated: true)
        
    }
    
    //MARK:Functions
    @objc func close(){
        self.view.removeFromSuperview()
    }
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }

}
