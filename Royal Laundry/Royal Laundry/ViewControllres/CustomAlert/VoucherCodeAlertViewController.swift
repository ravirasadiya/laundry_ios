//
//  VoucherCodeAlertViewController.swift
//  DemoAPI
//
//  Created by Latitude Technolabs on 05/12/20.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

class VoucherCodeAlertViewController: UIViewController {

    //MARK:Variable
    var didSelectItem: ((_ voucherDisccount: String) -> Void)?
    var arrVoucherCode = [VoucherCodeDataModel]()
    var totalprice:Int?
    //MARK:Outlets
    
    @IBOutlet var imgVouchercode: UIImageView!
    @IBOutlet var txtVoucherCode: UITextField!
    @IBOutlet var btnApply: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //round image
       // imgVouchercode.layer.cornerRadius = imgVouchercode.frame.size.width / 2
        //imgVouchercode.clipsToBounds = true
         self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        txtVoucherCode.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        btnApply.isUserInteractionEnabled = false
        self.view.addTapGesture(tapNumber: 1, target: self, action: #selector(close))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func close(){
        self.view.removeFromSuperview()
    }
    //MARK:Actions
    @IBAction func btnClosePressed(_ sender: UIButton) {
        self.view.removeFromSuperview()
        //self.removeFromParentViewController()
    }
    @IBAction func btnApplyPressed(_ sender: UIButton) {
        CheckVoucherCode(withVoucherCode: txtVoucherCode.text ?? "", withTotalPrice: totalprice ?? 0)
    }
    
    //MARK:Functions
    @objc func textFieldDidChange(_ textField: UITextField) {
        btnApply.tintColor = .white
        if (textField.text!.count == 0){
            btnApply.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            btnApply.backgroundColor = kAppButtonBGDeactivateColor
            btnApply.setTitleColor(kAppButtonTextDeactivateColor, for: .normal)
            btnApply.isUserInteractionEnabled = false
        } else {
            btnApply.tintColor = .white
            btnApply.backgroundColor = kAppButtonBGActiveColor
            btnApply.setTitleColor(kAppButtonTextActiveColor, for: .normal)
            btnApply.isUserInteractionEnabled = true
        }
    }
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    func showToast(message : String) {

        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 250, height: 35))
        toastLabel.center.x = self.view.center.x
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    //MARK: web service API call
    func CheckVoucherCode(withVoucherCode code:String,withTotalPrice total:Int){
        
          GlobalMethods.presentLoaderViewCtr(withViewController: self)
          
        WebServiceCall.callMethodWithURL(route: APIRouter.VoucherCode(withVoucherCode: code, withTotalPrice: total)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
              
              if (responseValue != nil) {
                  
                  let responseModel = VoucherCodeModel(dictionary: responseValue! as NSDictionary)
                  
                  if responseModel.status == 200 {
                    self.arrVoucherCode = responseModel.data
                      //print(responseModel.data)
                    let discount = self.arrVoucherCode[0].voucher_discount
                    self.didSelectItem!(discount)
                    self.view.removeFromSuperview()
                      
                  }
                  else {
                    self.showToast(message: "Voucher code not found")
                      //GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
                  }
              }
              else {
                  GlobalMethods.dismissLoaderView()
                  //GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
              }
          }
    }
}
