//
//  ComingSoonVC.swift
//  Royal Laundry
//
//  Created by Shine on 30/09/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit

class ComingSoonVC: UIViewController {
    //MARK:- Outlets
    @IBOutlet weak var viewMainBG: UIView!
    @IBOutlet weak var viewTopStatusBG: UIView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var txtviewHeader: UITextView!
    @IBOutlet weak var txtviewDetail: UITextView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSubmitEmail: UIButton!
    @IBOutlet weak var lblContinueToApp: UILabel!
    @IBOutlet weak var btnContinueToApp: UIButton!
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var viewEmailEnterBG: UIView!
    @IBOutlet weak var viewSubmitBtnBG: UIView!

    
    // MARK: - constants & variables
    
    
    //MARK:- UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupInitialView()
        txtEmail.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
    }
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        viewTopStatusBG.backgroundColor = kAppThemePrimaryDarkColor
        
        viewMainBG.backgroundColor = kAppThemePrimaryLightColor
        viewTop.backgroundColor = kAppThemePrimaryLightColor
        
        viewEmailEnterBG.backgroundColor = kAppTextSepColor
        viewSubmitBtnBG.backgroundColor = kAppTextSepColor
        
        txtEmail.setLeftPaddingPoints(kTextFieldPading)

        
        let wholeStr = StringFile.msgContinueToTheApp[kAppDelegate.userCurrentLanguage]
        //                let rangeToUnderLine = (wholeStr as NSString).range(of: "Sign in")
        let rangeToUnderLine = NSRange(location: 0, length: wholeStr.count)
        let font = kCustom_Font(fontName: kNunito_Regular, fontSize: 17.0)
        let underLineTxt = NSMutableAttributedString(string: wholeStr, attributes: [NSAttributedString.Key.font:font,NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.8)])
        underLineTxt.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: rangeToUnderLine)
        
        lblContinueToApp.attributedText = underLineTxt
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {

        if (textField.text!.count == 0){
            viewEmailEnterBG.backgroundColor = kAppTextSepColor
        } else {
            let strInput = textField.text!
            if strInput.contains(find: "@") {
                //viewEmailEnterBG.backgroundColor = UIColor.red
            }
            else{
                //viewEmailEnterBG.backgroundColor = UIColor.clear
            }
        }
        
        if textField.text!.isValidEmail() {
            viewSubmitBtnBG.backgroundColor = kAppButtonBGActiveColor
            btnSubmitEmail.isUserInteractionEnabled = true
        } else {
            viewSubmitBtnBG.backgroundColor = kAppTextSepColor
            btnSubmitEmail.isUserInteractionEnabled = false
        }

    }


    //MARK:- IBActions
    @IBAction func btnBackPressed(_ sender: Any) {
       // self.navigationController?.popViewController(animated: true)
        let objVc = self.storyboard?.instantiateViewController(withIdentifier: "AvailabilityCheckVC") as! AvailabilityCheckVC
        //self.navigationController?.pushViewController(objVc, animated: false)
        self.presentInFullScreen(objVc, animated: true)
    }
    
    @IBAction func btnSubmitEmailPressed(_ sender: Any) {
        let objVc = self.storyboard?.instantiateViewController(withIdentifier: "MainTabVC") as! MainTabVC
        self.presentInFullScreen(objVc, animated: true)
    }
    
    @IBAction func btnContinueToAppPressed(_ sender: Any) {
        let objVc = self.storyboard?.instantiateViewController(withIdentifier: "MainTabVC") as! MainTabVC
        self.presentInFullScreen(objVc, animated: true)
       // self.navigationController?.pushViewController(objVc, animated: false)

    }
    
}

//MARK:- UITextFieldDelegate

extension ComingSoonVC:UITextFieldDelegate{
    
}

