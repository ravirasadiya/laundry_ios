//
//  LoginVC.swift
//  Royal Laundry
//
//  Created by Shine on 05/11/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import iOSDropDown
class LoginVC: UIViewController,UITextFieldDelegate{

    //MARK:- Outlets
    @IBOutlet weak var txtCountry: DropDown!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewTopStatusBG: UIView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var txtSignInTitle: UITextView!
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    
    
    //MARK:- Variables
    var myPickerView : UIPickerView!
    var selectedIndex = 0
    var currentIndex = 0

    var arrayCountry = [countryList]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupInitialView()
        txtMobileNumber.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        
        txtMobileNumber.keyboardType = .phonePad
        DropDown()
        txtMobileNumber.delegate = self
        btnContinue.isUserInteractionEnabled = false
    }
    
    //MARK:- Class Methods and Functions
    func DropDown(){
        txtCountry.optionArray = ["+91","+41","+44","+1","+61","+33","+49","+971"]
        txtCountry.didSelect{(selectedText , index ,id) in
            self.txtCountry.text = selectedText
        }
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        btnContinue.tintColor = .white
        if (textField.text!.count == 0){
            btnContinue.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            btnContinue.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            btnContinue.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            btnContinue.isUserInteractionEnabled = false
        }
        if textField == txtMobileNumber{
            let phone = txtMobileNumber.text
            if phone!.count > 6{
                btnContinue.tintColor = .white
                btnContinue.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
                btnContinue.backgroundColor = #colorLiteral(red: 0.8832678795, green: 0.4046355486, blue: 0.549364984, alpha: 1)
                btnContinue.isUserInteractionEnabled = true
            }else{
                btnContinue.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                btnContinue.backgroundColor = kAppButtonBGDeactivateColor
                btnContinue.setTitleColor(kAppButtonTextDeactivateColor, for: .normal)
                btnContinue.isUserInteractionEnabled = false
            }
        }
    }
    func setupInitialView(){
        
        viewTopStatusBG.backgroundColor = kAppThemePrimaryDarkColor
        self.view.backgroundColor = kAppThemePrimaryLightColor
        viewTop.backgroundColor = kAppThemePrimaryLightColor
        btnContinue.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        btnContinue.backgroundColor = kAppButtonBGDeactivateColor
        btnContinue.setTitleColor(kAppButtonTextDeactivateColor, for: .normal)
        btnContinue.isUserInteractionEnabled = false
//        txtMobileNumber.setLeftPaddingPoints(kTextFieldPading)
//        let codeIndia = countryList.init()
//        codeIndia.name = "India"
//        codeIndia.phone_code = "+91"
//
//        let codeSwitzerland = countryList.init()
//        codeSwitzerland.name = "Switzerland"
//        codeSwitzerland.phone_code = "+41"
//
//        let codeUk = countryList.init()
//        codeUk.name = "Uk"
//        codeUk.phone_code = "+44"
//
//        let codeUs = countryList.init()
//        codeUs.name = "Us"
//        codeUs.phone_code = "+1"
//
//        let codeAus = countryList.init()
//        codeAus.name = "Aus"
//        codeAus.phone_code = "+61"
//
//        let codeFrance = countryList.init()
//        codeFrance.name = "France"
//        codeFrance.phone_code = "+33"
//
//        let codeGermany = countryList.init()
//        codeGermany.name = "Germany"
//        codeGermany.phone_code = "+49"
//
//        let codeDubai = countryList.init()
//        codeDubai.name = "Dubai"
//        codeDubai.phone_code = "+971"
//
//
//        arrayCountry.append(codeIndia)
//        arrayCountry.append(codeSwitzerland)
//        arrayCountry.append(codeUk)
//        arrayCountry.append(codeUs)
//        arrayCountry.append(codeAus)
//        arrayCountry.append(codeFrance)
//        arrayCountry.append(codeGermany)
//        arrayCountry.append(codeDubai)
//
//        //txtContryCode.text = arrayCountry[0].phone_code
//        //txtContryCode.delegate = self
    }
    func sendOtpApi(){

        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        WebServiceCall.callMethodWithURL(route: APIRouter.Signin(withmobile: txtMobileNumber.text!, country_code: txtCountry.text!)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in

            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = LoginModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200 {
                    
                    let objVC = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerifyVC") as! OTPVerifyVC
                    objVC.strCountry_Code = self.txtCountry.text!
                    objVC.strMobileNumber = self.txtMobileNumber.text!
                    self.presentInFullScreen(objVC, animated: false)
                }else{
                    self.showToast(message: "please enter valid number")
                   // GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.customernotfound, viewCtr: self, completion: nil)
                }
            }
            else {
                //GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    func showToast(message : String) {

        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 250, height: 35))
        toastLabel.center.x = self.view.center.x
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }

  /*  func pickUp(_ textField : UITextField){
        
        // UIPickerView
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        //self.myPickerView.selectedRow(inComponent: selectedIndex)
        textField.inputView = self.myPickerView
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    @objc func doneClick() {
        txtContryCode.resignFirstResponder()
        selectedIndex = currentIndex
    }
    @objc func cancelClick() {
        txtContryCode.resignFirstResponder()
        if selectedIndex != currentIndex{
            txtContryCode.text = arrayCountry[selectedIndex].phone_code
            currentIndex = selectedIndex
        }
    }
*/
    //MARK:Actions
    @IBAction func btnContinuePressed(_ sender: Any) {
        if txtMobileNumber.text!.isEmpty{
            showToast(message: "Please enter MobileNumber")
            return
        }else {
            self.sendOtpApi()
        }
    }
    
    @IBAction func btnSignUpPressed(_ sender: Any) {
        
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.presentInFullScreen(objVC, animated: false)
  
    }
    

    
 /*   //MARK:- PickerView Delegate & DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayCountry.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayCountry[row].phone_code
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtContryCode.text = arrayCountry[row].phone_code
        currentIndex = row
    }
    */
    //MARK:- TextFiled Delegate
    

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true
        }
        
        if (textField.text?.count)! > 0 && textField != txtMobileNumber{
            return false
        }
        if textField == txtMobileNumber{
            return range.location < 15
        }
        return string.isNumber
    }

}
