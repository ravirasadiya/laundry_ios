//
//  ReferralCodeVC.swift
//  Royal Laundry
//
//  Created by Shine on 01/10/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit

class ReferralCodeVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var viewMainBG: UIView!
    @IBOutlet weak var viewTopStatusBG: UIView!
    @IBOutlet weak var viewTop: UIView!

    
    @IBOutlet weak var txtviewHeader: UITextView!
    @IBOutlet weak var txtviewDetail: UITextView!
    @IBOutlet weak var txtVoucherRefcode: UITextField!
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet weak var viewVoucherRefcodeSep: UIView!

    
    // MARK: - constants & variables

    
    //MARK:- UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        btnApply.backgroundColor = kAppButtonBGActiveColor
        btnApply.setTitle(StringFile.strRetry[kAppDelegate.userCurrentLanguage], for: .normal)
        txtVoucherRefcode.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        btnApply.setTitle("I DON'T HAVE CODE", for: .normal)
        btnApply.backgroundColor = UIColor.lightGray
        btnApply.setTitleColor(UIColor.darkGray, for: .normal)
        btnApply.layer.cornerRadius = 5.0
        setupInitialView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:Functions
    @objc func textFieldDidChange(_ textField: UITextField) {
        btnApply.tintColor = .white
        if (textField.text!.count == 0){
            btnApply.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            btnApply.tintColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1);
            btnApply.setTitleColor(#colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1), for: .normal)
            btnApply.setTitle("I DON'T HAVE CODE", for: .normal)
        } else {
            btnApply.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
            btnApply.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1);
            btnApply.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            btnApply.setTitle(StringFile.strApply[kAppDelegate.userCurrentLanguage], for: .normal)
            
        }
    }
    func setupInitialView(){

        viewTopStatusBG.backgroundColor = kAppThemePrimaryDarkColor
        
        viewMainBG.backgroundColor = kAppThemePrimaryLightColor
        viewTop.backgroundColor = kAppThemePrimaryLightColor

        viewVoucherRefcodeSep.backgroundColor = kAppTextSepColor
        
        txtVoucherRefcode.setLeftPaddingPoints(kTextFieldPading)
        
        //btnApply.backgroundColor = kAppButtonBGActiveColor
        //btnApply.setTitle(StringFile.strRetry[kAppDelegate.userCurrentLanguage], for: .normal)

    }
/*    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupInitialView()
        txtVoucherRefcode.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)

    }

    //MARK:- Class Methods and Functions

    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if (textField.text!.count == 0){
            viewVoucherRefcodeSep.backgroundColor = kAppTextSepColor
            btnApply.setTitle(StringFile.strIDontHaveCode[kAppDelegate.userCurrentLanguage], for: .normal)
        } else {
            
            viewVoucherRefcodeSep.backgroundColor = UIColor.clear
            btnApply.setTitle(StringFile.strApply[kAppDelegate.userCurrentLanguage], for: .normal)
            
        }
    }
*/
    //MARK:- IBActions

    @IBAction func btnBackPressed(_ sender: Any) {
        //self.navigationController?.popViewController(animated: true)
        let objVc = self.storyboard?.instantiateViewController(withIdentifier: "AvailabilityCheckVC") as! AvailabilityCheckVC
        //self.navigationController?.pushViewController(objVc, animated: false)
        self.presentInFullScreen(objVc, animated: true)
    }
    
    @IBAction func btnApplyPressed(_ sender: Any) {
        
        if txtVoucherRefcode.text!.count > 0 {
            
            txtVoucherRefcode.resignFirstResponder()
            
            let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
            self.addChild(popvc)
            popvc.view.frame = self.view.frame
            self.view.addSubview(popvc.view)
            popvc.didMove(toParent: self)

        } else {
            let objVc = self.storyboard?.instantiateViewController(withIdentifier: "MainTabVC") as! MainTabVC
            //self.navigationController?.pushViewController(objVc, animated: false)
            self.presentInFullScreen(objVc, animated: true)

        }
        
    }
    
}

//MARK:- UITextFieldDelegate

extension ReferralCodeVC:UITextFieldDelegate{
    
}

