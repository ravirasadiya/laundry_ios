//
//  RegisterViewController.swift
//  DemoAPI
//
//  Created by Latitude Technolabs on 12/11/20.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit
import iOSDropDown
import YYCalendar
class RegisterViewController: UIViewController,UITextFieldDelegate {

    //MARK:Variables
    var strProfileImg = ""
    var strName = ""
    var strEmail = ""
    var strMobile = ""
    var strStatus = ""
    var strPostcode = ""
    var strAddress = ""
    var strGender = ""
    var strCountryCode = ""
    var strInviteCode = ""
     let datePicker = UIDatePicker()
    
    var istap:Bool!
    var count = 0
    
    var isNewsChecked:Bool = false
    var isTermsChecked:Bool = false
    //MARK:Outlet
    
  //MARK:Outlets
    
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewInner: UIView!
    @IBOutlet weak var imgGender: UIImageView!
    @IBOutlet weak var imgDOB: UIImageView!
    @IBOutlet weak var imgEmail: UIImageView!
    @IBOutlet weak var imgLastName: UIImageView!
    @IBOutlet weak var imgFisrtName: UIImageView!
    @IBOutlet weak var btnRefferalcode: CheckBoxButton!
    @IBOutlet weak var btnTermsAndCondition: CheckBoxButton!
    @IBOutlet weak var btnNewsAndOffers: CheckBoxButton!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var HeightTxrRefferalcode: NSLayoutConstraint!
    @IBOutlet var txtFirstName: UITextField!
    @IBOutlet var txtLastName: UITextField!
    @IBOutlet var txtEmailAddress: UITextField!
    @IBOutlet var txtMobileNumber: UITextField!
    @IBOutlet var txtDOB: UITextField!
    @IBOutlet var txtGender: DropDown!
    @IBOutlet var txtCountrycode: DropDown!
    @IBOutlet weak var viewEmailUnderline: UIView!
    @IBOutlet weak var viewMobileUnderline: UIView!
    @IBOutlet weak var txtRefferalcode: UITextField!
    @IBOutlet weak var viewStatusBG: UIView!
    //ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        SetUpInitialView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
      //  scrollview.contentSize = CGSize(width: self.viewInner.frame.size.width, height: self.viewInner.frame.size.height + 1000)
    }
    //MARK:Functions
    func SetUpInitialView(){
        viewStatusBG.backgroundColor = kAppThemePrimaryDarkColor
        viewTop.backgroundColor = kAppThemePrimaryLightColor
        txtDOB.delegate = self
        txtEmailAddress.delegate = self
        txtMobileNumber.delegate = self
        txtGender.delegate = self
        txtCountrycode.delegate = self
         dropdown()
        txtFirstName.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        txtLastName.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        txtEmailAddress.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        txtMobileNumber.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        txtMobileNumber.keyboardType = .phonePad
        self.HeightTxrRefferalcode.constant = 0
        self.txtRefferalcode.isHidden = true
    }
    func dropdown(){
        self.imgGender.image = #imageLiteral(resourceName: "gender_gray")
        txtGender.resignFirstResponder()
        txtGender.optionArray = ["Male","Female"]
        self.inputAccessoryViewDidFinish(textfield: self.txtGender)
        txtGender.didSelect{(selectedText , index ,id) in
            self.txtGender.text = selectedText
            self.imgGender.image = #imageLiteral(resourceName: "gender_colored")
        }
        txtCountrycode.resignFirstResponder()
        txtCountrycode.optionArray = ["+91","+41","+44","+1","+61","+33","+49","+971"]
        self.inputAccessoryViewDidFinish(textfield: self.txtCountrycode)
        txtCountrycode.didSelect{(selectedText , index ,id) in
            self.txtCountrycode.text = selectedText
        }
//        txtInfo.optionArray = ["Where did you hear about us?","Newspaper/publiation","Search engine","App store","Social media","Radio/podcast","out of home e.g.billboard,ube ad","Word of mouth","Other"]
//        txtInfo.didSelect{(selectedText , index ,id) in
//            self.txtInfo.text = selectedText
//        }
        
    }
    func registerApi(){
        strProfileImg = ""
        strName = "\(txtFirstName.text!) \(txtLastName.text!)"
        strEmail = "\(txtEmailAddress.text!)"
        strMobile = "\(txtMobileNumber.text!)"
        strStatus = "0"
        strPostcode = "wc2n5du"
        strAddress = "london"
        strGender = "\(txtGender.text!)"
        strCountryCode = "\(txtCountrycode.text ?? "")"
        //"\(txtCountryCode.text!)"
        strInviteCode = ""
    /*
        strName = "Test10"
        strEmail = "jadavshital97@gmail.com"
        strMobile = "9924241274"
        strStatus = "0"
        strPostcode = "WC2N5DU"
        strAddress = "London"
        strGender = "Female"
        strCountryCode = "+91"
        strInviteCode = ""
      */
        let params = [ ParameterKeys.Name : strName, ParameterKeys.Email : strEmail, ParameterKeys.Mobile : strMobile, ParameterKeys.Status : strStatus, ParameterKeys.Postcode : strPostcode, ParameterKeys.Address : strAddress, ParameterKeys.CountryCode : strCountryCode, ParameterKeys.InvitedCode : strInviteCode, ParameterKeys.Gender : strGender] as [String : Any]
        print(params)

        WebServiceCall.callMethodWithURL(route: APIRouter.RegisterCustomer(params)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                print("sucessfully register")
                let alertController = UIAlertController(title: "Register Status", message: "sucessfully register", preferredStyle: .alert)
                let action = UIAlertAction(title: "ok", style: UIAlertAction.Style.cancel, handler:  {(action:UIAlertAction!) in
              let objVC = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerifyVC") as! OTPVerifyVC
              objVC.strCountry_Code = self.txtCountrycode.text!
              objVC.strMobileNumber = self.txtMobileNumber.text!
              self.presentInFullScreen(objVC, animated: false)
                    
                })
                
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)

            }
            else {
                let alertController = UIAlertController(title: "Register canceled", message: "Please try again", preferredStyle: .alert)
                let action = UIAlertAction(title: "RETRY", style: UIAlertAction.Style.cancel, handler: nil)

                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }


    @objc func textFieldDidChange(_ textField: UITextField) {
        txtDOB.resignFirstResponder()
        txtDOB.endEditing(true)
        txtGender.resignFirstResponder()
        txtGender.endEditing(true)
        txtCountrycode.resignFirstResponder()
        txtCountrycode.endEditing(true)
        if (textField == txtGender ){
            if textField.text == "Male" || textField.text == "Female"{
            }else{
                textField.text = ""
            }
        }
        if (textField.text!.count == 0){
          //  txtEmailAddress.backgroundColor = kAppTextSepColor
        } else {
            let strInput = txtEmailAddress.text!
            if strInput.contains(find: "@") {
            }
            else{
            }
        }
        if textField == txtEmailAddress{
            if txtEmailAddress.text!.isValidEmail() {
                viewEmailUnderline.backgroundColor = UIColor.darkGray
                imgEmail.image = #imageLiteral(resourceName: "email_colored")
            } else {
                viewEmailUnderline.backgroundColor = UIColor.red
                imgEmail.image = #imageLiteral(resourceName: "email_gray")
            }
        }
        if textField == txtMobileNumber{
            let phone = txtMobileNumber.text
            if phone!.count > 6{
                viewMobileUnderline.backgroundColor = UIColor.darkGray
                
            }else{
                viewMobileUnderline.backgroundColor = UIColor.red
            }
        }
        if textField == txtFirstName{
            if txtFirstName.text!.count > 0{
                imgFisrtName.image = #imageLiteral(resourceName: "edit_Colored")
            }else{
                imgFisrtName.image = #imageLiteral(resourceName: "edit_grey")
            }
        }
        if textField == txtLastName{
            if txtLastName.text!.count > 0{
                imgLastName.image = #imageLiteral(resourceName: "edit_Colored")
            }else{
                imgLastName.image = #imageLiteral(resourceName: "edit_grey")
            }
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if txtDOB.text?.count == 0{
            self.imgDOB.image = #imageLiteral(resourceName: "DOB")
        }
        txtGender.resignFirstResponder()
        txtCountrycode.resignFirstResponder()
        txtDOB.resignFirstResponder()
        if textField == txtDOB{
            let calendar = YYCalendar(normalCalendarLangType: .ENG, date: "07/01/2019", format: "dd/MM/yyyy") { date in
                print(date)
                self.txtDOB.text = date
                self.imgDOB.image = #imageLiteral(resourceName: "DOB_Colored")
                self.txtDOB.resignFirstResponder()
            }
            calendar.show()
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtDOB{
            txtDOB.resignFirstResponder()
            return false
        }
        if textField == txtGender{
            txtGender.resignFirstResponder()
            return false
        }
        if textField == txtCountrycode{
            txtCountrycode.resignFirstResponder()
            return false
        }
        if textField == txtMobileNumber{
            return range.location < 15
        }
        return true
    }
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.bordered, target: self, action: "donedatePicker")
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.bordered, target: self, action: "cancelDatePicker")
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        // add toolbar to textField
        txtDOB.inputAccessoryView = toolbar
        // add datepicker to textField
        txtDOB.inputView = datePicker
        
    }
    
    func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        txtDOB.text = formatter.string(from: datePicker.date)
        //dismiss date picker dialog
       // datePicker.endEditing(true)
      //  txtDOB.inputAccessoryView?.endEditing(true)
        self.view.endEditing(true)
    }
    
    func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        //txtDOB.inputAccessoryView?.endEditing(true)
        datePicker.endEditing(true)
    }
    func showToast(message : String,width:CGFloat) {

        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: width, height: 35))
        toastLabel.center.x = self.view.center.x
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    func inputAccessoryViewDidFinish(textfield:UITextField) {
        textfield.resignFirstResponder()
}
    //MARK:Actions
    @IBAction func btnContinue(_ sender: UIButton) {

        if txtFirstName.text!.isEmpty || txtLastName.text!.isEmpty || txtEmailAddress.text!.isEmpty || txtMobileNumber.text!.isEmpty || txtCountrycode.text!.isEmpty || txtGender.text!.isEmpty || txtDOB.text!.isEmpty {
           // self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
           // return
        }else{
            if isNewsChecked == false{
                showToast(message: "Please agree News & Offers",width: 250)
                return
            }
            if isTermsChecked == false{
                showToast(message: "Please agree Terms And Conditions",width: 250)
                return
            }
            if istap == true{
                showToast(message: "Please add refferal code",width: 250)
                return
            }
            registerApi()
        }
        
        if txtFirstName.text!.isEmpty{
            showToast(message: "Please enter firstname",width: 200)
            return
        }else if txtLastName.text!.isEmpty{
            showToast(message: "Please enter lastname",width: 200)
            return
        }else if txtEmailAddress.text!.isEmpty{
            showToast(message: "Please enter email",width: 200)
            return
        }else if txtMobileNumber.text!.isEmpty{
            showToast(message: "Please enter MobileNumber",width: 200)
            return
        }else if txtDOB.text!.isEmpty{
            showToast(message: "Please enter DOB",width: 200)
            return
        }else if txtGender.text!.isEmpty{
            showToast(message: "Please enter Gender",width: 200)
            return
        }
    }
    @IBAction func btnRefferalcodePressed(_ sender: Any) {
        count += 1
        print(count)
        if count % 2 != 0 {
            self.HeightTxrRefferalcode.constant = 42
            self.txtRefferalcode.isHidden = false
            istap = true
        }else{
            self.HeightTxrRefferalcode.constant = 0
            self.txtRefferalcode.isHidden = true
            istap = false
        }
    }

    @IBAction func btnDismiss(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnNewsPressed(_ sender: UIButton) {
        isNewsChecked = !isNewsChecked
    }
    
    @IBAction func btnTermsPressed(_ sender: UIButton) {
        isTermsChecked = !isTermsChecked
    }
    
}

