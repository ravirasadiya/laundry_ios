//
//  SignUpVC.swift
//  Royal Laundry
//
//  Created by Shine on 07/11/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import YYCalendar
class SignUpVC: UIViewController,UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    
    //MARK:- Outlets
    @IBOutlet weak var viewMainBG: UIView!
    @IBOutlet weak var viewTopStatusBG: UIView!
    @IBOutlet weak var viewTop: UIView!

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblViewTitle: UILabel!
    
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmailAddress: UITextField!
    @IBOutlet weak var txtCountryCode: UITextField!
    @IBOutlet weak var txtMobileNUmber: UITextField!
    @IBOutlet weak var txtDOBirth: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    
    
    
    @IBOutlet weak var imgCheckRefCode: UIImageView!
    @IBOutlet weak var btnRefCode: UIButton!
    @IBOutlet weak var lblRefCode: UILabel!

    @IBOutlet weak var imgNewsLetter: UIImageView!
    @IBOutlet weak var btnNewsLetter: UIButton!
    @IBOutlet weak var lblNewsLetter: UILabel!

    @IBOutlet weak var imgTermsAndCondition: UIImageView!
    @IBOutlet weak var btnTermsAndCondition: UIButton!
    @IBOutlet weak var lblTermsAndCondition: UILabel!

    @IBOutlet weak var btnContinue: UIButton!
    
    
    @IBOutlet weak var viewEmailUnderline: UIView!
    
    
    @IBOutlet weak var viewMobileUnderline: UIView!
    //MARK:- Variables

    var myPickerView : UIPickerView!
    var selectedIndex = 0
    var currentIndex = 0

    var arrayCountry = [countryList]()
    
    var strProfileImg = ""
    var strName = ""
    var strEmail = ""
    var strMobile = ""
    var strStatus = ""
    var strPostcode = ""
    var strAddress = ""
    var strGender = ""
    var strCountryCode = ""
    var strInviteCode = ""


    override func viewDidLoad() {
        super.viewDidLoad()
        txtDOBirth.delegate = self
        txtEmailAddress.delegate = self
        txtMobileNUmber.delegate = self
        
        // Do any additional setup after loading the view.
        self.setupInitialView()
        txtEmailAddress.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
    }
    
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
                
        let codeIndia = countryList.init()
        codeIndia.name = "India"
        codeIndia.phone_code = "+91"
        
        let codeSwitzerland = countryList.init()
        codeSwitzerland.name = "Switzerland"
        codeSwitzerland.phone_code = "+41"

        let codeUk = countryList.init()
        codeUk.name = "Uk"
        codeUk.phone_code = "+44"

        let codeUs = countryList.init()
        codeUs.name = "Us"
        codeUs.phone_code = "+1"

        let codeAus = countryList.init()
        codeAus.name = "Aus"
        codeAus.phone_code = "+61"

        let codeFrance = countryList.init()
        codeFrance.name = "France"
        codeFrance.phone_code = "+33"

        let codeGermany = countryList.init()
        codeGermany.name = "Germany"
        codeGermany.phone_code = "+49"

        let codeDubai = countryList.init()
        codeDubai.name = "Dubai"
        codeDubai.phone_code = "+971"

        
        arrayCountry.append(codeIndia)
        arrayCountry.append(codeSwitzerland)
        arrayCountry.append(codeUk)
        arrayCountry.append(codeUs)
        arrayCountry.append(codeAus)
        arrayCountry.append(codeFrance)
        arrayCountry.append(codeGermany)
        arrayCountry.append(codeDubai)

        txtCountryCode.text = arrayCountry[0].phone_code
        txtCountryCode.delegate = self

        
    }
    
    func pickUp(_ textField : UITextField){
        
        // UIPickerView
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        //self.myPickerView.selectedRow(inComponent: selectedIndex)
        textField.inputView = self.myPickerView
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    @objc func doneClick() {
        txtCountryCode.resignFirstResponder()
        selectedIndex = currentIndex
    }
    @objc func cancelClick() {
        txtCountryCode.resignFirstResponder()
        if selectedIndex != currentIndex{
            txtCountryCode.text = arrayCountry[selectedIndex].phone_code
            currentIndex = selectedIndex
        }
    }

    
    func registerCustomer(){
        
        //profile,name,email,mobile,status=0, postcode=wc2n5du,address=london, gender,country_code, invited_code


        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        //mobile,otp,notificationToken,country_code,type
        
        strProfileImg = "img.png"
        strName = "\(txtFirstName.text!) \(txtLastName.text!)"
        strEmail = "\(txtEmailAddress.text!)"
        strMobile = "\(txtMobileNUmber.text!)"
        strStatus = "0"
        strPostcode = "wc2n5du"
        strAddress = "UK"
        strGender = "\(txtGender.text!)"
        strCountryCode = "\(txtCountryCode.text!)"
        strInviteCode = "0"

        let reqParam = [ParameterKeys.Profile : strProfileImg, ParameterKeys.Name : strName, ParameterKeys.Email : strEmail, ParameterKeys.Mobile : strMobile, ParameterKeys.Status : strStatus, ParameterKeys.Postcode : strPostcode, ParameterKeys.Address : strAddress, ParameterKeys.CountryCode : strCountryCode, ParameterKeys.InvitedCode : strInviteCode, ParameterKeys.Gender : strGender] as [String : Any]
        
        WebServiceCall.callMethodWithURL(route: APIRouter.RegisterCustomer(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                                let responseModel = SignUpModel(dictionary: responseValue! as NSDictionary)

                                if responseModel.status == 200  {

                                    let objVC = self.storyboard?.instantiateViewController(withIdentifier: "MainTabVC") as! MainTabVC
                                        self.presentInFullScreen(objVC, animated: true)

                                }
                                else {
                                    GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
                                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)

    }
    
    @IBAction func btnRefCodePressed(_ sender: Any) {
        
    }
    @IBAction func btnNewsLetterPressed(_ sender: Any) {
    }
    @IBAction func btnTermsAndConditionPressed(_ sender: Any) {
    }

    
    @IBAction func btnContinuePressed(_ sender: Any) {
        self.registerCustomer()
        
    }
    
    
    
    //MARK:- PickerView Delegate & DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayCountry.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayCountry[row].phone_code
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtCountryCode.text = arrayCountry[row].phone_code
        currentIndex = row
    }
    
    //MARK:- TextFiled Delegate
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == txtEmailAddress{
            if textField.text?.count != nil{
                if (textField.text?.isValidEmail())!{
                    viewEmailUnderline.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                }else{
                    viewEmailUnderline.backgroundColor = #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
                }
            }
        }
        if textField == txtMobileNUmber{
            if textField.text?.count != nil{
                
                
            }
        }

    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtCountryCode{
            self.pickUp(txtCountryCode)
        }
        if textField == txtDOBirth{
            let calendar = YYCalendar(normalCalendarLangType: .ENG, date: "07/01/2019", format: "dd/MM/yyyy") { date in
                print(date)
                self.txtDOBirth.text = date
            }
            calendar.show()
        }
    }

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true
        }
        
        if (textField.text?.count)! > 0 && textField != txtMobileNUmber{
            return false
        }
        return string.isNumber
       
    }


}
