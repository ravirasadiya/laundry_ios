//
//  AvailabilityCheckVC.swift
//  Royal Laundry
//
//  Created by Shine on 30/09/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AvailabilityCheckVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var viewMainBG: UIView!
    @IBOutlet weak var viewTopStatusBG: UIView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var txtviewHeader: UITextView!
    @IBOutlet weak var txtviewDetail: UITextView!
    @IBOutlet weak var txtPostcode: UITextField!
    @IBOutlet weak var btnCheckAvailability: UIButton!
    @IBOutlet weak var lblAlreadyAC: UILabel!
    @IBOutlet weak var btnSignin: UIButton!
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet weak var viewPostcodeSep: UIView!
    @IBOutlet weak var tblPostalCodeData: UITableView!

    
    // MARK: - constants & variables
    var arrCodeData = [String]()
   
    //MARK:- UIView Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationController?.navigationBar.isHidden = true
        setupInitialView()
        tblPostalCodeData.delegate = self
        tblPostalCodeData.dataSource = self
        self.tblPostalCodeData.isHidden = true
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        viewTopStatusBG.backgroundColor = kAppThemePrimaryDarkColor
        viewMainBG.backgroundColor = kAppThemePrimaryLightColor
        viewTop.backgroundColor = kAppThemePrimaryLightColor
        
        viewPostcodeSep.backgroundColor = kAppTextSepColor
        
        txtPostcode.setLeftPaddingPoints(kTextFieldPading)

        btnCheckAvailability.backgroundColor = kAppButtonBGDeactivateColor
        btnCheckAvailability.setTitleColor(kAppButtonTextDeactivateColor, for: .normal)
        
        let wholeStr = StringFile.msgAlreadyAccount[kAppDelegate.userCurrentLanguage]
        let rangeToUnderLine = (wholeStr as NSString).range(of: "Sign in")
//        let rangeToUnderLine = NSRange(location: 0, length: 7)
        let font = kCustom_Font(fontName: kNunito_Regular, fontSize: 17.0)
        let underLineTxt = NSMutableAttributedString(string: wholeStr, attributes: [NSAttributedString.Key.font:font,NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.8)])
        underLineTxt.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: rangeToUnderLine)
        
        lblAlreadyAC.attributedText = underLineTxt
        
    }
    //wc2n5du
    func callGetPostcodeAPi()
    {
        let url = "https://api.getaddress.io/find/\(txtPostcode.text!)?api-key=\(kPostcodeAthorisedkey)"
        
        Alamofire.request(url).validate().responseJSON { response in
            switch response.result {
            case .success:
                
                
                let responseJSON = JSON(response.result.value!)
                
                print("Postcode response \(responseJSON)")
                
                if ((responseJSON.dictionaryObject) != nil)
                {
                    let parsedData = JSON(responseJSON)
                    print("parsedData response \(parsedData)")

                    let addresses = parsedData["addresses"]
                    let longitude = parsedData["longitude"]
                    let latitude = parsedData["latitude"]
                    print("longitude latitude response \(longitude)\(latitude)")

                    
                    for add in addresses {
                                  
                        print("add response \(add)")
                        

                    }
                    if parsedData["addresses"].count != 0{
//                        let str = parsedData["addresses"][0].string
//                        let parsed = str?.replacingOccurrences(of: ", ,", with: ",")
                        self.tblPostalCodeData.isHidden = false
                        self.arrCodeData.append(parsedData["addresses"][0].string ?? "")
                        self.arrCodeData.append(parsedData["addresses"][1].string ?? "")
                        self.arrCodeData.append(parsedData["addresses"][2].string ?? "")
                        self.arrCodeData.append(parsedData["addresses"][3].string ?? "")
                        self.arrCodeData.append(parsedData["addresses"][4].string ?? "")
                        self.arrCodeData.append(parsedData["addresses"][5].string ?? "")
                        self.arrCodeData.append(parsedData["addresses"][6].string ?? "")
                        //self.arrCodeData.removeAll(where: { $0 == ", ," })
                        self.tblPostalCodeData.reloadData()
                    }
                    
//                    let objVC = self.storyboard?.instantiateViewController(withIdentifier: "ReferralCodeVC") as! ReferralCodeVC
//                    self.navigationController?.pushViewController(objVC, animated: false)

                }
                else
                {
                    self.afterGetPostcodeResponse()
                }
            case .failure(let error):
                print(error)
                self.afterGetPostcodeResponse()
                
            }
        }
        
    }
    
    func afterGetPostcodeResponse(){
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "ComingSoonVC") as! ComingSoonVC
        self.presentInFullScreen(objVC, animated: true)
       // self.navigationController?.pushViewController(objVC, animated: false)

    }
    
    
    //MARK:- IBActions
    @IBAction func btnCheckAvailabilityPressed(_ sender: Any) {
        self.callGetPostcodeAPi()
    }
    
    @IBAction func btnSigninPressed(_ sender: Any) {
        
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.presentInFullScreen(objVC, animated: false)

    }
    
}

//MARK:- UITextFieldDelegate

extension AvailabilityCheckVC:UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        
//        let  char = string.cString(using: String.Encoding.utf8)!
//        let isBackSpace = strcmp(char, "\\b")
//
//        if (isBackSpace == -92) {
//            return true
//        }
//
//        if textField == tfPhone{
//            return string.isNumber
//        }
        if (range.location == 0 && string.count == 0){
            btnCheckAvailability.backgroundColor = kAppButtonBGDeactivateColor
            btnCheckAvailability.setTitleColor(kAppButtonTextDeactivateColor, for: .normal)
            viewPostcodeSep.backgroundColor = kAppTextSepColor
        } else {
            btnCheckAvailability.backgroundColor = kAppButtonBGActiveColor
            btnCheckAvailability.setTitleColor(kAppButtonTextActiveColor, for: .normal)
            viewPostcodeSep.backgroundColor = UIColor.clear

        }

        
        return true
    }
}

extension AvailabilityCheckVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCodeData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let str = arrCodeData[indexPath.row]
        let str1 = str.replacingOccurrences(of: ", ,", with: ",")
        cell.textLabel?.text = str1.replacingOccurrences(of: ", ,", with: ",")
        cell.textLabel?.numberOfLines = 3
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(arrCodeData[indexPath.row])
//        enum UserDefaultsKeys : String {
//            case PostalCodeData
//        }
//         UserDefaults.standard.set(arrCodeData[indexPath.row], forKey: UserDefaultsKeys.PostalCodeData.rawValue)
        GlobalMethods.setUserDefaultObject(object: arrCodeData[indexPath.row] as AnyObject, key: MyUserDefaultsKeys.PostalCodeData)

                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ReferralCodeVC") as! ReferralCodeVC
        self.presentInFullScreen(viewController, animated: true)
        //self.navigationController?.pushViewController(viewController, animated: false)
    }
}

