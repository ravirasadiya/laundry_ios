//
//  OTPVerifyVC.swift
//  Royal Laundry
//
//  Created by Shine on 05/11/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit

class OTPVerifyVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var viewMainBG: UIView!
    @IBOutlet weak var viewTopStatusBG: UIView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var txtOtpTitle: UITextView!
    @IBOutlet weak var txtFirst: UITextField!
    @IBOutlet weak var txtSecond: UITextField!
    @IBOutlet weak var txtThird: UITextField!
    @IBOutlet weak var txtFour: UITextField!
    @IBOutlet weak var txtFive: UITextField!
    @IBOutlet weak var txtSix: UITextField!
    
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var lblTimer: UILabel!
    
    // MARK: - constants & variables
    
    var remainnigSecond = 0
    var objTimer: Timer?
    var strMobileNumber = ""
    var strOtp = ""
    var strNotificationToken = ""
    var strCountry_Code = ""
    var strType = ""
    var isResendBool:Bool?
    // MARK:- UIView Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupInitialView()
        
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        txtFirst.becomeFirstResponder()
        
        viewTopStatusBG.backgroundColor = kAppThemePrimaryDarkColor
        viewMainBG.backgroundColor = kAppThemePrimaryLightColor
        viewTop.backgroundColor = kAppThemePrimaryLightColor
        
        txtFirst.layer.cornerRadius = 5.0
        txtFirst.layer.masksToBounds = true
        txtFirst.layer.borderWidth = 1
        txtFirst.layer.borderColor = kAppThemeborderColor.cgColor
        
        
        txtSecond.layer.cornerRadius = 5.0
        txtSecond.layer.masksToBounds = true
        txtSecond.layer.borderWidth = 1
        txtSecond.layer.borderColor = kAppThemeborderColor.cgColor
        
        txtThird.layer.cornerRadius = 5.0
        txtThird.layer.masksToBounds = true
        txtThird.layer.borderWidth = 1
        txtThird.layer.borderColor = kAppThemeborderColor.cgColor
        
        txtFour.layer.cornerRadius = 5.0
        txtFour.layer.masksToBounds = true
        txtFour.layer.borderWidth = 1
        txtFour.layer.borderColor = kAppThemeborderColor.cgColor
        
        txtFive.layer.cornerRadius = 5.0
        txtFive.layer.masksToBounds = true
        txtFive.layer.borderWidth = 1
        txtFive.layer.borderColor = kAppThemeborderColor.cgColor
        
        txtSix.layer.cornerRadius = 5.0
        txtSix.layer.masksToBounds = true
        txtSix.layer.borderWidth = 1
        txtSix.layer.borderColor = kAppThemeborderColor.cgColor
        
        
        lblTimer.textColor = kAppThemeborderColor
        
        //        btnVerify.backgroundColor = kAppThemeOrangeLightColor
        //
        //        btnVerify.backgroundColor = kAppThemeOrangeLightColor
        btnVerify.layer.cornerRadius = 5.0
        btnVerify.layer.masksToBounds = true

        txtFirst.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControl.Event.editingChanged)
        txtSecond.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControl.Event.editingChanged)
        txtThird.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControl.Event.editingChanged)
        txtFour.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControl.Event.editingChanged)
        txtFive.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControl.Event.editingChanged)
        txtSix.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControl.Event.editingChanged)
        
        self.setUpViewString()
        self.setUpTimer()
    }
    
    func setUpTimer(){
        remainnigSecond = 60
        lblTimer.text = "\(StringFile.strSecondsRemaining[kAppDelegate.userCurrentLanguage]): 00:\(remainnigSecond)"
        objTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
        self.setViewHideShowWithAnimarion(view: btnVerify, hidden: false)
    }
    @objc func runTimedCode(){
        if remainnigSecond == 0{
            isResendBool = true
            self.objTimer?.invalidate()
            //self.setViewHideShowWithAnimarion(view: btnVerify, hidden: true)
            self.btnVerify.setTitle("RESEND OTP", for: .normal)
            btnVerify.tintColor = .white
            btnVerify.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            btnVerify.backgroundColor = #colorLiteral(red: 0.8832678795, green: 0.4046355486, blue: 0.549364984, alpha: 1)
            btnVerify.isUserInteractionEnabled = true
            txtFirst.text = ""
            txtSecond.text = ""
            txtThird.text = ""
            txtFour.text = ""
            txtFive.text = ""
            txtSix.text = ""
            txtFirst.becomeFirstResponder()
            let strRemain = String(format: "%02d", remainnigSecond)
            lblTimer.text = "\(StringFile.strSecondsRemaining[kAppDelegate.userCurrentLanguage]): 00:\(strRemain)"
        }else{
            remainnigSecond = remainnigSecond - 1
            let strRemain = String(format: "%02d", remainnigSecond)
            
            lblTimer.text = "\(StringFile.strSecondsRemaining[kAppDelegate.userCurrentLanguage]): 00:\(strRemain)"
        }
    }
    
    func setUpViewString(){
        
    }
    
    func setTokenMoveToDashboard(){
        
    }
    
    func verifyOtpApi(){
        
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        //mobile,otp,notificationToken,country_code,type
        
        strNotificationToken = "fgj76jjCSaqTF4YWPu9fOG:APA91bF-jPHtJxJ2Uq1QUwcWpK-SwKY8I0ZIVUVtB8pa-yD2jsJzK4QAnicaYHy8cwyd7VsRR0NhCSBK0YJ8lqwZIYjrcSVadUQ8xtYPNoW_4W2zq6C_fTYe_W61mF0HWyzUoIST6Gdd"
        strType = "signin"
//        strNotificationToken = "123456478"
//        strType = "iOS"

        let reqParam = [ParameterKeys.Mobile : strMobileNumber, ParameterKeys.Otp : strOtp, ParameterKeys.NotificationToken : strNotificationToken, ParameterKeys.CountryCode : strCountry_Code, ParameterKeys.TypeOTP : strType] as [String : Any]
        
        WebServiceCall.callMethodWithURL(route: APIRouter.OtpVerify(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                                let responseModel = VeriftOTPModel(dictionary: responseValue! as NSDictionary)
                
                                if responseModel.status == 200  {
                                    if responseModel.data.count == 0{
                                        self.strNotificationToken = "123456478"
                                        self.strType = "iOS"
                                        self.verifyOtpApi()
                                    }else{
                                          var obj = [VerifyOTPModelDataModel]()
                                              obj = responseModel.data
                                          /*enum UserDefaultsKeys : String {
                                              case customerid
                                              case  username
                                              case useremail
                                              case  usermobile
                                              case usercountrycode
                                              case userstatus
                                              case  useraddress
                                              case userpostcode
                                              case usergender

                                          }
                                          UserDefaults.standard.set(obj[0].id, forKey: UserDefaultsKeys.customerid.rawValue)
                                          UserDefaults.standard.set(obj[0].name, forKey: UserDefaultsKeys.username.rawValue)
                                          UserDefaults.standard.set(obj[0].email, forKey: UserDefaultsKeys.useremail.rawValue)
                                          UserDefaults.standard.set(obj[0].mobile, forKey: UserDefaultsKeys.usermobile.rawValue)
                                          UserDefaults.standard.set(obj[0].country_code, forKey: UserDefaultsKeys.usercountrycode.rawValue)
                                          UserDefaults.standard.set(obj[0].postcode, forKey: UserDefaultsKeys.userpostcode.rawValue)
                                          UserDefaults.standard.set(obj[0].address, forKey: UserDefaultsKeys.useraddress.rawValue)
                                          UserDefaults.standard.set(obj[0].gender, forKey: UserDefaultsKeys.usergender.rawValue)
                                          */
                                        GlobalMethods.setUserDefaultObject(object: obj[0].id as AnyObject, key: MyUserDefaultsKeys.customerid)
                                        GlobalMethods.setUserDefaultObject(object: obj[0].name as AnyObject, key: MyUserDefaultsKeys.username)
                                        GlobalMethods.setUserDefaultObject(object: obj[0].email as AnyObject, key: MyUserDefaultsKeys.useremail)
                                        GlobalMethods.setUserDefaultObject(object: obj[0].mobile as AnyObject, key: MyUserDefaultsKeys.usermobile)
                                        GlobalMethods.setUserDefaultObject(object: obj[0].country_code as AnyObject, key: MyUserDefaultsKeys.usercountrycode)
                                        GlobalMethods.setUserDefaultObject(object: obj[0].postcode as AnyObject, key: MyUserDefaultsKeys.userpostcode)
                                        GlobalMethods.setUserDefaultObject(object: obj[0].address as AnyObject, key: MyUserDefaultsKeys.useraddress)
                                        GlobalMethods.setUserDefaultObject(object: obj[0].gender as AnyObject, key: MyUserDefaultsKeys.usergender)
                                        
                                          GlobalMethods.setUserDefaultObject(object: true as AnyObject, key: MyUserDefaultsKeys.isloginsucess)
                                          print("Sucess")
                                          let objVC = self.storyboard?.instantiateViewController(withIdentifier: "MainTabVC") as! MainTabVC
                                          self.presentInFullScreen(objVC, animated: true)
                                    }        
                                }
//                                else {
//                                    GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
//                                }
            }
            else {
               // GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    func sendOtpApi(){

        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        WebServiceCall.callMethodWithURL(route: APIRouter.Signin(withmobile: strMobileNumber, country_code: strCountry_Code)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in

            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = LoginModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200 {
                   // self.verifyOtpApi()
      
                }else{
                    //self.showToast(message: "please enter valid number")
                   // GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.customernotfound, viewCtr: self, completion: nil)
                }
            }
            else {
                //GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    //MARK:- IBActions
    
    
    @IBAction func btnVerifyPressed(_ sender: Any) {
        if isResendBool == true{
            isResendBool = false
            btnVerify.tintColor = .darkGray
            btnVerify.backgroundColor = kAppButtonBGDeactivateColor
            btnVerify.setTitleColor(kAppButtonTextDeactivateColor, for: .normal)
            btnVerify.setTitle("VERIFY", for: .normal)
            btnVerify.isUserInteractionEnabled = false
            setUpTimer()
            self.sendOtpApi()
            return
        }else{
            self.view.endEditing(true)
            
            strOtp = "\(txtFirst.text!)\(txtSecond.text!)\(txtThird.text!)\(txtFour.text!)\(txtFive.text!)\(txtSix.text!)"
            
            if txtFirst.text!.isEmpty || txtSecond.text!.isEmpty || txtThird.text!.isEmpty || txtFour.text!.isEmpty || txtFive.text!.isEmpty || txtSix.text!.isEmpty{
                //self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgEnterEnterCode[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
                
                return
            }

            self.verifyOtpApi()
        }

    }
    
    
    //MARK:- UITextField Delegate Method
    
    @objc func textFieldEditingDidChange(textField: UITextField) {
        
        let text = textField.text
        
        if (text?.utf16.count)! >= 1{
            switch textField{
            case txtFirst:
                txtSecond.becomeFirstResponder()
            case txtSecond:
                txtThird.becomeFirstResponder()
            case txtThird:
                txtFour.becomeFirstResponder()
            case txtFour:
                txtFive.becomeFirstResponder()
            case txtFive:
                txtSix.becomeFirstResponder()
            case txtSix:
                txtSix.resignFirstResponder()
            default:
                break
            }
        }
        
        
        if  text?.count == 0 {
            switch textField{
            case txtFirst:
                txtFirst.becomeFirstResponder()
            case txtSecond:
                txtFirst.becomeFirstResponder()
            case txtThird:
                txtSecond.becomeFirstResponder()
            case txtFour:
                txtThird.becomeFirstResponder()
            case txtFive:
                txtFour.becomeFirstResponder()
            case txtSix:
                txtFive.becomeFirstResponder()
            default:
                break
            }
        }else{
            btnVerify.tintColor = .white
            btnVerify.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            btnVerify.backgroundColor = #colorLiteral(red: 0.8832678795, green: 0.4046355486, blue: 0.549364984, alpha: 1)
            btnVerify.isUserInteractionEnabled = true
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true
        }
        
        if textField.text!.count == 1{
            return false
        }
        
        return string.isNumber
        
    }
    
}
