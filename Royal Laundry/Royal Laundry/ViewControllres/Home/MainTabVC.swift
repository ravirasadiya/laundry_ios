//
//  MainTabVC.swift
//  Royal Laundry
//
//  Created by Shine on 02/10/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit

class MainTabVC: TabBarContainerViewController {
    //MARK:- Outlets

    @IBOutlet weak var barView: UICollectionView!
    @IBOutlet weak var container: UIScrollView!
    
    @IBOutlet weak var viewTopStatusBG: UIView!
    @IBOutlet weak var viewTop: UIView!
    
    @IBOutlet weak var viewBasket: UIView!
    
    @IBOutlet weak var imgBasket: UIImageView!
    
    @IBOutlet weak var viewSepBasket: UIView!
    @IBOutlet weak var lblItemYourBasket: UILabel!
    @IBOutlet weak var lblPriceBasket: UILabel!
    
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var btnBasket: UIButton!

    @IBOutlet weak var btnOrder: UIButton!
    @IBOutlet weak var btnAccount: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    
    // MARK: - constants & variables
    var objProduct = ProductModel()

    var tabShouldFillWidth = true
    var cellName:String = TabBarCollectionViewCellTitle.resusableName
    
    //MARK:- UIView Life Cycle
    override func viewDidLoad() {
        self.navigationController?.navigationBar.isHidden = true
        cellName = TabBarCollectionViewCellTitleWithImage.resusableName
        tabShouldFillWidth = false

        self.getProductApi()
        self.setupTabBar()

       super.viewDidLoad()
        
        GlobalMethods.createDatabaseTbl()
        
        /*~~~~~ notification  ~~~~~*/
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBasketPrice), name: NSNotification.Name(rawValue: kUpdateHomeBasketPrice), object: nil)

        GlobalMethods.getBasketUpdate()
        SetUpInitialView()
    }
    func SetUpInitialView(){
        viewTopStatusBG.backgroundColor = kAppThemePrimaryDarkColor
        //self.view.backgroundColor = kAppThemePrimaryLightColor
        viewTop.backgroundColor = kAppThemePrimaryLightColor
        barView.backgroundColor = kAppThemePrimaryLightColor
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if kAppDelegate.isPaymentSucess == true{
            kAppDelegate.isPaymentSucess = false
            kAppDelegate.basketPrice = 0.0
            kAppDelegate.basketItemQuantity = 0
            let database =  try! SQLitePool.manager().initialize(database: "royalLaundry", withExtension: "sqlite")
            let strUpdateQuery = "DELETE FROM cart_item"
            print(strUpdateQuery)
            database.query(strUpdateQuery, successClosure: { (result) in
                GlobalMethods.getBasketUpdate()
            }) { (e) in
                print(e)
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
                
        self.updateBasketPrice()
    }
    
    //MARK:- Class Methods and Functions
    func setupTabBar() {
        self.topBarView = barView
        self.barcontainer = container
        self.tabBarShouldFillWidth = tabShouldFillWidth
        self.tabBarWidth = 70
        self.indicatorHeight = 3
        self.selectedIndicatorColor = kAppButtonTextActiveColor
        self.setupTabBar(viewControllers: getViewControllers(type: SlideViewControllerTitleWithImage.self))
        self.moveTo(page: 0)
    }
    
    @objc func updateBasketPrice(){
        let str1 = "Your Basket                 "
        if kAppDelegate.basketPrice != 0{
            
            let pricedouble = Double(kAppDelegate.basketPrice)
            let str2 = "\(StringFile.strPriceSymbol[kAppDelegate.userCurrentLanguage])\(pricedouble) >"
            lblItemYourBasket.text = str1
            lblPriceBasket.text =  str2
            
        }else{
           // let str1 = "Your Basket"
            let str2 = "\(StringFile.strPriceSymbol[kAppDelegate.userCurrentLanguage])0.00 >"
            lblItemYourBasket.text = str1
            lblPriceBasket.text = str2
            
        }
     }

    
    func getViewControllers<T>(type: T.Type)->[T] {
//        var count = 10
//        if tabShouldFillWidth == false {
//            count = 10
//        }
        var viewControllers = [T]()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TopsVC") as? TopsVC
        viewControllers.append(vc! as! T)
        let vc1 = self.storyboard?.instantiateViewController(withIdentifier: "LaundryVC") as? LaundryVC
        viewControllers.append(vc1! as! T)
        let vc2 = self.storyboard?.instantiateViewController(withIdentifier: "BeddingVC") as? BeddingVC
        viewControllers.append(vc2! as! T)
        let vc3 = self.storyboard?.instantiateViewController(withIdentifier: "SuitsVC") as? SuitsVC
        viewControllers.append(vc3! as! T)
        let vc4 = self.storyboard?.instantiateViewController(withIdentifier: "TrousersVC") as? TrousersVC
        viewControllers.append(vc4! as! T)
        let vc5 = self.storyboard?.instantiateViewController(withIdentifier: "DressesVC") as? DressesVC
        viewControllers.append(vc5! as! T)
        let vc6 = self.storyboard?.instantiateViewController(withIdentifier: "OutdoorVC") as? OutdoorVC
        viewControllers.append(vc6! as! T)
        let vc7 = self.storyboard?.instantiateViewController(withIdentifier: "AccessoriesVC") as? AccessoriesVC
        viewControllers.append(vc7! as! T)
        let vc8 = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as? HomeVC
        viewControllers.append(vc8! as! T)
        let vc9 = self.storyboard?.instantiateViewController(withIdentifier: "BusinessVC") as? BusinessVC
        viewControllers.append(vc9! as! T)

//        for _ in 1...count {
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: String(describing:type)) as? T
//            viewControllers.append(vc!)
//        }
        return viewControllers
    }
    
    
    
    
    //MARK: - Web Services
    
        func getProductApi(){
            
            GlobalMethods.presentLoaderViewCtr(withViewController: self)
            
            WebServiceCall.callMethodWithURL(route: APIRouter.GetProduct) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
                
                 if (responseValue != nil) {
                               
                   let responseModel = ProductModel(dictionary: responseValue! as NSDictionary)
                   
                   if responseModel.status == 200 {
                    
                    self.objProduct = responseModel
                    
                    NotificationCenter.default.post(name: Notification.Name(kUpdateProductTops), object: nil, userInfo: ["objTopsItem":self.objProduct.data[0]])
                    NotificationCenter.default.post(name: Notification.Name(kUpdateProductLaundry), object: nil, userInfo: ["objLaundryItem":self.objProduct.data[1]])
                    NotificationCenter.default.post(name: Notification.Name(kUpdateProductBedding), object: nil, userInfo: ["objBeddingItem":self.objProduct.data[2]])
                    NotificationCenter.default.post(name: Notification.Name(kUpdateProductSuits), object: nil, userInfo: ["objSuitsItem":self.objProduct.data[3]])
                    NotificationCenter.default.post(name: Notification.Name(kUpdateProductTrousers), object: nil, userInfo: ["objTrousersItem":self.objProduct.data[4]])
                    NotificationCenter.default.post(name: Notification.Name(kUpdateProductDresses), object: nil, userInfo: ["objDressesItem":self.objProduct.data[5]])
                    NotificationCenter.default.post(name: Notification.Name(kUpdateProductOutdoor), object: nil, userInfo: ["objOutdoorItem":self.objProduct.data[6]])
                    NotificationCenter.default.post(name: Notification.Name(kUpdateProductAccessories), object: nil, userInfo: ["objAccessoriesItem":self.objProduct.data[7]])
                    NotificationCenter.default.post(name: Notification.Name(kUpdateProductHome), object: nil, userInfo: ["objHomeItem":self.objProduct.data[8]])
                    NotificationCenter.default.post(name: Notification.Name(kUpdateProductBusiness), object: nil, userInfo: ["objBusinessItem":self.objProduct.data[9]])

                   }
                   else {
                       GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
                   }
               }
               else {
                    GlobalMethods.dismissLoaderView()
                    GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
               }
            }
        }
    
    
    func setProductRespone(){
        
    }
    //MARK:- IBActions
    @IBAction func btnBasketPressed(_ sender: Any) {
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "BasketVC") as! BasketVC
        self.presentInFullScreen(objVC, animated: true)
    }
    
    @IBAction func btnSearchPressed(_ sender: UIButton) {
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchItemViewController") as! SearchItemViewController
        self.presentInFullScreen(objVC, animated: true)
    }
    
    @IBAction func btnAccountPressed(_ sender: UIButton) {
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "AccountVC") as! AccountVC
        self.presentInFullScreen(objVC, animated: true)
    }
    
    @IBAction func btnOrderPressed(_ sender: UIButton) {
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderViewController") as! OrderViewController
        self.presentInFullScreen(objVC, animated: true)
    }
}
