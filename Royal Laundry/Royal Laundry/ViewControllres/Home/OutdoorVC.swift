//
//  OutdoorVC.swift
//  Royal Laundry
//
//  Created by Shine on 10/10/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit

class OutdoorVC: SlideViewControllerTitleWithImage, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {

    //MARK:- Outlets
    @IBOutlet weak var collOutdoorItem: UICollectionView!

    // MARK: - constants & variables
    var objOutdoorItem = ProductDataModel()

    //MARK:- UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        collOutdoorItem.delegate = self
        collOutdoorItem.dataSource = self

        collOutdoorItem.register(UINib(nibName: laundryItemCell, bundle: nil), forCellWithReuseIdentifier: laundryItemCell)

            /*~~~~~ notification  ~~~~~*/
            NotificationCenter.default.addObserver(self, selector: #selector(self.updateItem), name: NSNotification.Name(rawValue: kUpdateProductOutdoor), object: nil)

        }
        
        //MARK:- Class Methods and Functions
        @objc func updateItem(_ notification: Notification){
            
            objOutdoorItem = notification.userInfo?["objOutdoorItem"] as! ProductDataModel
            
            collOutdoorItem.reloadData()
        }

    // MARK: - UICollectionView Delegate, DataSource Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objOutdoorItem.categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: laundryItemCell, for: indexPath) as! LaundryItemCell
        
        cell.imgItem.sd_setImage(with: objOutdoorItem.categories[indexPath.row].imgUrl, placeholderImage: kBusinessImagePlaceHolder)
        cell.lblItemTitle.text = objOutdoorItem.categories[indexPath.row].name
        cell.viewPrice.isHidden = true
        cell.viewOffer.isHidden = true

        return cell
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let cellSize  = CGSize(width: SystemSize.width, height: 180)
        return cellSize

    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {

    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {


        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailVC") as! ProductDetailVC
        vc.objProductDetail = objOutdoorItem.categories[indexPath.row]
        presentInFullScreen(vc, animated: true)

    }

}
