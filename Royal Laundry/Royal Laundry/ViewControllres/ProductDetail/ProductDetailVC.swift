//
//  ProductDetailVC.swift
//  Royal Laundry
//
//  Created by Shine on 13/10/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit

class ProductDetailVC: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var viewTopStatusBar: UIView!
    
    @IBOutlet weak var viewMainBg: UIView!
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var imgHeader: UIImageView!
    
    @IBOutlet weak var viewTitle: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var objScroll: UIScrollView!
    
    @IBOutlet weak var viewScrollContent: UIView!
    
    @IBOutlet weak var lblProductDetailDescr: UILabel!
    
    @IBOutlet weak var tblProductItem: UITableView!
    
    @IBOutlet weak var lblFaqTitle: UILabel!
    
    @IBOutlet weak var tblFaq: UITableView!
    
    
    @IBOutlet weak var viewBottom: UIView!
    
    @IBOutlet weak var btnBottom: UIButton!
    
    @IBOutlet weak var constTblFaqHeight: NSLayoutConstraint!
    @IBOutlet weak var constTblProductHeight: NSLayoutConstraint!
    
    @IBOutlet weak var constHeaderHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblItemYourBasket: UILabel!
    @IBOutlet weak var lblPriceBasket: UILabel!
    
    
    // MARK: - constants & variables
    var objProductDetail = ProductCatModel()
    var datasource = [ExpandingTableViewCellContent]()
    
    var arrProductItem = [ProductDetailDataModel]()
    
    var arrCatProductItem = [ProductSubCatModel]()
    
    
    var oldContentOffset = CGPoint.zero
    let topConstraintRange = (CGFloat(55 / SystemSize.height)..<CGFloat(230/SystemSize.height))
    
    
    let objDbManager = SQLiteHelper()
    
    var isExpanded:Bool?
    
    var searchcatid:String?
    var searchproductid:String?
    var isSearch:Bool?
    var searchname:String?
    var searchprice:String?
    var searchquantity:String?
    var searchDesc:String?
    var isBool:Bool?
    //MARK:- UIView Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        /*~~~~~ notification  ~~~~~*/
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBasketPrice), name: NSNotification.Name(rawValue: kUpdateProductDetailViewBasketPrice), object: nil)
        
        setupInitialView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.updateBasketPrice()
    
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        viewTopStatusBar.backgroundColor = kAppThemePrimaryDarkColor
        tblFaq.dataSource = self
        tblFaq.delegate = self
        tblFaq.tableFooterView = UIView() // Removes empty cell separators
        tblFaq.estimatedRowHeight = 60
        tblFaq.rowHeight = UITableView.automaticDimension
        tblFaq.registerCell(cellIDs: [faqCell], isDynamicHeight: true, estimatedHeight: 40)
        tblProductItem.dataSource = self
        tblProductItem.delegate = self
        //tblProductItem.tableFooterView = UIView() // Removes empty cell separators
        tblProductItem.estimatedRowHeight = 85
        tblProductItem.rowHeight = UITableView.automaticDimension
        tblProductItem.registerCell(cellIDs: [productItemtableviewCell], isDynamicHeight: true, estimatedHeight: 40)
        objScroll.delegate = self
        if isSearch == true{
            self.getProductDetail(withId: searchcatid ?? "")
            self.getCategoryFAQs(withId: searchcatid ?? "")
        }else{
            self.getProductDetail(withId: objProductDetail.id)
            self.getCategoryFAQs(withId: objProductDetail.id)
        }
        lblProductDetailDescr.text = objProductDetail.description1
        imgHeader.sd_setImage(with: objProductDetail.imgUrl, placeholderImage: kBusinessImagePlaceHolder)
        lblTitle.text = ""
    }
    
    @objc func updateBasketPrice(){
      //  let str1 = "Your Basket                 "
        if kAppDelegate.basketPrice != 0{
            let pricedouble = Double(kAppDelegate.basketPrice)
            let str2 = "\(StringFile.strPriceSymbol[kAppDelegate.userCurrentLanguage])\(pricedouble) >"
            //lblItemYourBasket.text = str1
            lblPriceBasket.text =  str2
        }else{
            //let str1 = "Your Basket                    "
            let str2 = "\(StringFile.strPriceSymbol[kAppDelegate.userCurrentLanguage])0.00 >"
           // lblItemYourBasket.text = str1
            lblPriceBasket.text = str2
        }
    }

    // MARK: - Web Service Function Methods
    func getProductDetail(withId Id: String){
        
        WebServiceCall.callMethodWithURL(route: APIRouter.GetService(withID: Id)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                let responseModel = ProductDetailModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200 {
                    
                    print(responseModel)
                    self.arrProductItem = responseModel.data
                    self.setResponseProductItem()
                }
                else {
                    GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func getCategoryFAQs(withId Id: String){
        
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        let reqParam = [ParameterKeys.Category_id : Id] as [String : Any]
        
        WebServiceCall.callMethodWithURL(route: APIRouter.GetCategoryFAQs(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = FaqModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200{
                    
                    self.datasource.removeAll()
                    for faqData in responseModel.data {
                        let addData = ExpandingTableViewCellContent(title: faqData.Que, subtitle: faqData.Ans)
                        self.datasource.append(addData)
                    }
                    self.tblFaq.reloadData()
                }
                else {
                    
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func setResponseProductItem(){
        
        let categorieNames = Array(Set(arrProductItem.map({$0.service_name})))
        
        self.arrCatProductItem.removeAll()
        
        for serviceName in categorieNames{
            
            let check = arrProductItem.filter { $0.service_name == serviceName}
            
            let catObj = ProductSubCatModel()
            catObj.catData = check
            catObj.service_name = serviceName
            
            self.arrCatProductItem.append(catObj)
        }
        

        if isSearch == true{
            self.constTblProductHeight.constant = 150
        }else{
            self.constTblProductHeight.constant = CGFloat(self.arrProductItem.count * 90) + CGFloat(self.arrCatProductItem.count * 40)
        }
        
        self.reloadData()
    }
    
    func reloadData(){
        if arrCatProductItem.count != 0{
            self.tblProductItem.reloadData()
        }else{
            //            tblProductItem.emptyDataSetSource = self
            //            tblProductItem.emptyDataSetDelegate = self
            self.tblProductItem.reloadData()
        }
    }

    //MARK:- IBActions
    @IBAction func btnBackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnBottomPressed(_ sender: Any) {
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "BasketVC") as! BasketVC
        self.presentInFullScreen(objVC, animated: true)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        setPosition(scrollView)
    }
//    func addgradient(img:UIImageView){
//        let  gradientLayer   = CAGradientLayer()
//        gradientLayer.frame  = img.bounds
//        gradientLayer.colors = [UIColor.clear.cgColor,UIColor.lightGray.cgColor]
//        gradientLayer.opacity = 0.1
//        img.layer.addSublayer(gradientLayer)
//    }
    
    func setPosition(_ scrollView:UIScrollView) {
        
        if scrollView.contentOffset.y >= 75 {
            isBool = false
            lblTitle.text = objProductDetail.name
            imgHeader.alpha = 0.8
            let newMultiplier:CGFloat = 75/SystemSize.height
            self.constHeaderHeight = self.constHeaderHeight.setMultiplier(multiplier: newMultiplier)
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }else if scrollView.contentOffset.y <= 230 {
            isBool = true
            lblTitle.text = ""
            imgHeader.alpha = 1.0
            let newMultiplier:CGFloat = 230/896
            self.constHeaderHeight = self.constHeaderHeight.setMultiplier(multiplier: newMultiplier)
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
}

// MARK: - UITableView DataSource, Delegate Methods

extension ProductDetailVC : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblProductItem {
            return arrCatProductItem.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView == tblProductItem {
            
            let view = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:40))
            let label = UILabel(frame: CGRect(x:16, y:0, width:tableView.frame.size.width, height:40))
            let font = kCustom_Font(fontName: kNunito_Bold, fontSize: 17.0)
            label.font = font
       
                if isSearch == true{
                    let items = arrCatProductItem[section].catData
                    for product in items{
                        if product.id == searchproductid{
                            label.text = product.service_name
                        }
                    }
                }else{
                    label.text = arrCatProductItem[section].service_name
                }
            
            label.textColor = UIColor.gray
            view.addSubview(label);
            view.backgroundColor = UIColor.clear
            return view
        }
        else{
            let view = UIView(frame: CGRect(x:0, y:0, width:0, height:0))
            view.backgroundColor = UIColor.clear
            return view
        }
        
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    
        if tableView == tblProductItem {
            if arrCatProductItem[section].service_name != "" {
                return 40
            } else {
                return 0
            }
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblProductItem {
            if isSearch == true{
                return 1
            }else{
                let items = arrCatProductItem[section].catData
                return items.count
            }
        } else {
            return datasource.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblProductItem {
            let cell = tableView.dequeueReusableCell(withIdentifier: productItemtableviewCell, for: indexPath) as! ProductItemTableViewCell
            tblProductItem.rowHeight = 85
            let items = arrCatProductItem[indexPath.section].catData
            if isSearch == true{
                for product in items{
                    if product.id == searchproductid{
                        cell.lblTitle.text = product.name
                        cell.lblDesc.text = product.description1
                        let doubleprice = Double(product.price)
                        cell.lblPrice.text = "\(StringFile.strPriceSymbol[kAppDelegate.userCurrentLanguage])\(doubleprice ?? 0.0)"
                        cell.lblQuantity.text = "\(product.userSelectedQuantity)"
                        
                        let strCountQuery = "SELECT * FROM cart_item WHERE service_id=\(product.service) and category_id=\(product.category_id) and subcategory_id=\(product.subcat_id);"
                        print(strCountQuery)
                        
                        let database =  try! SQLitePool.manager().initialize(database: "royalLaundry", withExtension: "sqlite")
                        
                        let resultSelectAll = try! database.query(strCountQuery)
                        print(resultSelectAll)
                        
                        if resultSelectAll.affectedRowCount > 0 {
                            
                            let resultPrice = resultSelectAll.results![0]
                            
                            let quantity = Int(resultPrice["quantity"] as! String)
                            
                            if quantity! > 0 {
                                cell.imgIncrement.image = #imageLiteral(resourceName: "add_item")
                                cell.btnDecHeight.constant = 40
                                cell.lblQuaHeight.constant = 40
                                
                                cell.imgDecrement.isHidden = false
                                cell.lblQuantity.isHidden = false
                                cell.btnDecrement.isHidden = false
                               // cell.imgIncrItem.isHidden = false
                                
                            } else {
                                cell.imgIncrement.image = #imageLiteral(resourceName: "Plus")
                                cell.btnDecHeight.constant = 0
                                cell.lblQuaHeight.constant = 0
                                
                                cell.imgDecrement.isHidden = true
                                cell.lblQuantity.isHidden = true
                                cell.btnDecrement.isHidden = true
                                //cell.imgIncrItem.isHidden = false
                                
                            }
                            
                            cell.lblQuantity.text = "\(quantity!)"
                            
                            items[indexPath.row].userSelectedQuantity = quantity!
                            
                        } else {
                            cell.imgIncrement.image = #imageLiteral(resourceName: "Plus")
                            cell.btnDecHeight.constant = 0
                            cell.lblQuaHeight.constant = 0
                            
                            cell.imgDecrement.isHidden = true
                            cell.lblQuantity.isHidden = true
                            cell.btnDecrement.isHidden = true
                           // cell.imgIncrItem.isHidden = false
                            
                            
                        }
                                
                        cell.actionDescrementItemClosure = { [weak self] in
                            print("item removed")
                            
                            let database =  try! SQLitePool.manager().initialize(database: "royalLaundry", withExtension: "sqlite")
                            
                            if resultSelectAll.affectedRowCount > 0 {
                                
                                let resultPrice = resultSelectAll.results![0]
                                
                                var quantity = Int(resultPrice["quantity"] as! String)
                                
                                quantity = quantity! - 1
                                
                                //                if itemsAdd[indexPath.row].userSelectedQuantity > 0{
                                
                                if quantity! > 0 {
                                    cell.imgIncrement.image = #imageLiteral(resourceName: "add_item")
                                    let strUpdateQuery = "UPDATE cart_item SET quantity = \(quantity!) WHERE service_id=\(product.service) and category_id=\(product.category_id) and subcategory_id=\(product.subcat_id);"
                                    //                    print(strUpdateQuery)
                                    
                                    database.query(strUpdateQuery, successClosure: { (result) in
                                        
                                        //                        print(result)
                                        GlobalMethods.getBasketUpdate()
                                        let updateIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
                                        self!.tblProductItem.reloadRows(at: [updateIndexPath], with: .none)
                                        
                                        
                                    }) { (e) in
                                        print(e)
                                    }
                                    
                                } else {
                                    cell.imgIncrement.image = #imageLiteral(resourceName: "Plus")
                                    let strDeleteQuery = "DELETE from cart_item WHERE service_id=\(product.service) and category_id=\(product.category_id) and subcategory_id=\(product.subcat_id);"
                                    //                    print(strDeleteQuery)
                                    
                                    database.query(strDeleteQuery, successClosure: { (result) in
                                        
                                        //                        print(result)
                                        GlobalMethods.getBasketUpdate()
                                        let updateIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
                                        self!.tblProductItem.reloadRows(at: [updateIndexPath], with: .none)
                                        
                                        
                                    }) { (e) in
                                        print(e)
                                    }
                                }
                            }
                            else{
                                
                                let strDeleteQuery = "DELETE from cart_item WHERE service_id=\(product.service) and category_id=\(product.category_id) and subcategory_id=\(product.subcat_id);"
                                //                    print(strDeleteQuery)
                                
                                database.query(strDeleteQuery, successClosure: { (result) in
                                    
                                    //                        print(result)
                                    GlobalMethods.getBasketUpdate()
                                    let updateIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
                                    self!.tblProductItem.reloadRows(at: [updateIndexPath], with: .none)
                                    
                                    
                                }) { (e) in
                                    print(e)
                                }
                                
                                
                            }
                            
                        }
                        
                        
                        cell.actionIncrementItemClosure = { [weak self] in
                            cell.imgIncrement.image = #imageLiteral(resourceName: "add_item")
                            print("item added")
                                            
                            let database =  try! SQLitePool.manager().initialize(database: "royalLaundry", withExtension: "sqlite")
                            
                                            
                            if resultSelectAll.affectedRowCount > 0 {

                                let resultPrice = resultSelectAll.results![0]
                                
                                var quantity = Int(resultPrice["quantity"] as! String)
                                
                                quantity = quantity! + 1
                                
                                let strUpdateQuery = "UPDATE cart_item SET quantity = \(quantity!) WHERE service_id=\(product.service) and category_id=\(product.category_id) and subcategory_id=\(product.subcat_id);"
                                //                    print(strUpdateQuery)
                                
                                database.query(strUpdateQuery, successClosure: { (result) in
                                    
                                    //                        print(result)
                                    GlobalMethods.getBasketUpdate()
                                    let updateIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
                                    self!.tblProductItem.reloadRows(at: [updateIndexPath], with: .none)
                                    
                                    
                                }) { (e) in
                                    print(e)
                                }
                            } else {
                                
                                let quantity = 1
                                
                                let strInsertQuery = "INSERT INTO cart_item (service_id, category_id, subcategory_id, customer_id, quantity, product_id, product_name, product_description ,price) VALUES('\(product.service)','\(product.category_id)','\(product.subcat_id)','newCust','\(quantity)','\(self!.objProductDetail.id)','\(self!.objProductDetail.name)','\(product.description1)','\(product.price)');"
                                //                    print(strInsertQuery)
                                
                                
                                database.query(strInsertQuery, successClosure: { (result) in
                                    
                                    //                        print(result)
                                    GlobalMethods.getBasketUpdate()
                                    let updateIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
                                    self!.tblProductItem.reloadRows(at: [updateIndexPath], with: .none)
                                    
                                    
                                }) { (e) in
                                    print(e)
                                }
                            }
                    }
                }
                
                }
            } else{
                cell.lblTitle.text = items[indexPath.row].name
                cell.lblDesc.text = items[indexPath.row].description1

                let doubleprice = Double(items[indexPath.row].price)
                            
                cell.lblPrice.text = "\(StringFile.strPriceSymbol[kAppDelegate.userCurrentLanguage])\(doubleprice ?? 0.0)"
                cell.lblQuantity.text = "\(items[indexPath.row].userSelectedQuantity)"
            }
            
            
            let strCountQuery = "SELECT * FROM cart_item WHERE service_id=\(items[indexPath.row].service) and category_id=\(items[indexPath.row].category_id) and subcategory_id=\(items[indexPath.row].subcat_id);"
            print(strCountQuery)
            
            let database =  try! SQLitePool.manager().initialize(database: "royalLaundry", withExtension: "sqlite")
            
            let resultSelectAll = try! database.query(strCountQuery)
            print(resultSelectAll)
            
            if resultSelectAll.affectedRowCount > 0 {
                
                let resultPrice = resultSelectAll.results![0]
                
                let quantity = Int(resultPrice["quantity"] as! String)
                
                if quantity! > 0 {
                    cell.imgIncrement.image = #imageLiteral(resourceName: "add_item")
                    cell.btnDecHeight.constant = 40
                    cell.lblQuaHeight.constant = 40
                    
                    cell.imgDecrement.isHidden = false
                    cell.lblQuantity.isHidden = false
                    cell.btnDecrement.isHidden = false
                   // cell.imgIncrItem.isHidden = false
                    
                } else {
                    cell.imgIncrement.image = #imageLiteral(resourceName: "Plus")
                    cell.btnDecHeight.constant = 0
                    cell.lblQuaHeight.constant = 0
                    
                    cell.imgDecrement.isHidden = true
                    cell.lblQuantity.isHidden = true
                    cell.btnDecrement.isHidden = true
                    //cell.imgIncrItem.isHidden = false
                    
                }
                
                cell.lblQuantity.text = "\(quantity!)"
                
                items[indexPath.row].userSelectedQuantity = quantity!
                
            } else {
                cell.imgIncrement.image = #imageLiteral(resourceName: "Plus")
                cell.btnDecHeight.constant = 0
                cell.lblQuaHeight.constant = 0
                
                cell.imgDecrement.isHidden = true
                cell.lblQuantity.isHidden = true
                cell.btnDecrement.isHidden = true
               // cell.imgIncrItem.isHidden = false
                
                
            }
                    
            cell.actionDescrementItemClosure = { [weak self] in
                print("item removed")
                
                let database =  try! SQLitePool.manager().initialize(database: "royalLaundry", withExtension: "sqlite")
                
                if resultSelectAll.affectedRowCount > 0 {
                    
                    let resultPrice = resultSelectAll.results![0]
                    
                    var quantity = Int(resultPrice["quantity"] as! String)
                    
                    quantity = quantity! - 1
                    
                    //                if itemsAdd[indexPath.row].userSelectedQuantity > 0{
                    
                    if quantity! > 0 {
                        cell.imgIncrement.image = #imageLiteral(resourceName: "add_item")
                        let strUpdateQuery = "UPDATE cart_item SET quantity = \(quantity!) WHERE service_id=\(items[indexPath.row].service) and category_id=\(items[indexPath.row].category_id) and subcategory_id=\(items[indexPath.row].subcat_id);"
                        //                    print(strUpdateQuery)
                        
                        database.query(strUpdateQuery, successClosure: { (result) in
                            
                            //                        print(result)
                            GlobalMethods.getBasketUpdate()
                            let updateIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
                            self!.tblProductItem.reloadRows(at: [updateIndexPath], with: .none)
                            
                            
                        }) { (e) in
                            print(e)
                        }
                        
                    } else {
                        cell.imgIncrement.image = #imageLiteral(resourceName: "Plus")
                        let strDeleteQuery = "DELETE from cart_item WHERE service_id=\(items[indexPath.row].service) and category_id=\(items[indexPath.row].category_id) and subcategory_id=\(items[indexPath.row].subcat_id);"
                        //                    print(strDeleteQuery)
                        
                        database.query(strDeleteQuery, successClosure: { (result) in
                            
                            //                        print(result)
                            GlobalMethods.getBasketUpdate()
                            let updateIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
                            self!.tblProductItem.reloadRows(at: [updateIndexPath], with: .none)
                            
                            
                        }) { (e) in
                            print(e)
                        }
                        
                    }
                    
                    
                }
                else{
                    
                    let strDeleteQuery = "DELETE from cart_item WHERE service_id=\(items[indexPath.row].service) and category_id=\(items[indexPath.row].category_id) and subcategory_id=\(items[indexPath.row].subcat_id);"
                    //                    print(strDeleteQuery)
                    
                    database.query(strDeleteQuery, successClosure: { (result) in
                        
                        //                        print(result)
                        GlobalMethods.getBasketUpdate()
                        let updateIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
                        self!.tblProductItem.reloadRows(at: [updateIndexPath], with: .none)
                        
                        
                    }) { (e) in
                        print(e)
                    }
                }
            }
            
            
            cell.actionIncrementItemClosure = { [weak self] in
                cell.imgIncrement.image = #imageLiteral(resourceName: "add_item")
                print("item added")
                                
                let database =  try! SQLitePool.manager().initialize(database: "royalLaundry", withExtension: "sqlite")
                
                                
                if resultSelectAll.affectedRowCount > 0 {

                    let resultPrice = resultSelectAll.results![0]
                    
                    var quantity = Int(resultPrice["quantity"] as! String)
                    
                    quantity = quantity! + 1
                    
                    let strUpdateQuery = "UPDATE cart_item SET quantity = \(quantity!) WHERE service_id=\(items[indexPath.row].service) and category_id=\(items[indexPath.row].category_id) and subcategory_id=\(items[indexPath.row].subcat_id);"
                    //                    print(strUpdateQuery)
                    
                    database.query(strUpdateQuery, successClosure: { (result) in
                        
                        //                        print(result)
                        GlobalMethods.getBasketUpdate()
                        let updateIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
                        self!.tblProductItem.reloadRows(at: [updateIndexPath], with: .none)
                    }) { (e) in
                        print(e)
                    }
                } else {
                    
                    let quantity = 1
                    
                    let strInsertQuery = "INSERT INTO cart_item (service_id, category_id, subcategory_id, customer_id, quantity, product_id, product_name, product_description ,price) VALUES('\(items[indexPath.row].service)','\(items[indexPath.row].category_id)','\(items[indexPath.row].subcat_id)','newCust','\(quantity)','\(self!.objProductDetail.id)','\(self!.objProductDetail.name)','\(items[indexPath.row].description1)','\(items[indexPath.row].price)');"
                    //                    print(strInsertQuery)
                    
                    
                    database.query(strInsertQuery, successClosure: { (result) in
                        
                        //                        print(result)
                        GlobalMethods.getBasketUpdate()
                        let updateIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
                        self!.tblProductItem.reloadRows(at: [updateIndexPath], with: .none)
                        
                        
                    }) { (e) in
                        print(e)
                    }
                    
                }
            }
            return cell
        } else {
            let cell = tableView .dequeueReusableCell(withIdentifier: String(describing: FaqCell.self), for: indexPath) as! FaqCell
            cell.set(content: datasource[indexPath.row])
            if isExpanded == true{
                cell.lblQueFaq.textColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
                cell.imgDropDown.image = #imageLiteral(resourceName: "downarrow")
                constTblFaqHeight.constant = constTblFaqHeight.constant + 100//constTblFaqHeight.constant
            }else{
                cell.lblQueFaq.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                cell.imgDropDown.image = #imageLiteral(resourceName: "rightarrow")
                constTblFaqHeight.constant = constTblFaqHeight.constant
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tblProductItem {
            
        } else {
            isExpanded = false
            let content = datasource[indexPath.row]
            content.expanded = !content.expanded
            if content.expanded{
                isExpanded = true
            }
            tableView.reloadRows(at: [indexPath], with: .automatic)
            
        }
    }

}
