//
//  OrderTableViewCell.swift
//  DemoAPI
//
//  Created by Latitude Technolabs on 23/11/20.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell {

    //MARK:Outlets
    
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblOrderID: UILabel!
    @IBOutlet var imgOrder: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
