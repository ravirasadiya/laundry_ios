//
//  OrderViewController.swift
//  DemoAPI
//
//  Created by Latitude Technolabs on 18/11/20.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

class OrderViewController: UIViewController {

    //MARK:Variables
    var objOrderList = [OrderListDataModel]()
    var orderIDarray = [Int]()
    var selectedIndex:Int?
    //MARK:Outlets
    
    @IBOutlet weak var imgClose: UIImageView!
    @IBOutlet weak var viewStatusBG: UIView!
    @IBOutlet var tblOrder: UITableView!
    @IBOutlet var imgOrderPage: UIImageView!
    @IBOutlet var lblTitleTop: UILabel!
    @IBOutlet var viewTop: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewStatusBG.backgroundColor = kAppThemePrimaryDarkColor
        viewTop.backgroundColor = kAppThemePrimaryLightColor
        let nib1 = UINib(nibName: "OrderTableViewCell", bundle: nil)
        tblOrder.register(nib1, forCellReuseIdentifier: "OrderTableViewCell")
        if GlobalMethods.getUserDefaultObjectForKey(key: MyUserDefaultsKeys.isloginsucess) != nil{
            imgOrderPage.isHidden = true
            tblOrder.isHidden = false
            let customerID:Int = GlobalMethods.getUserDefaultObjectForKey(key: MyUserDefaultsKeys.customerid) as! Int
            if GlobalMethods.getUserDefaultObjectForKey(key: MyUserDefaultsKeys.isloginsucess) != nil{
                getOrderList(withId: String(customerID))
            }
        }else{
            imgOrderPage.isHidden = false
            tblOrder.isHidden = true
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.closePress))
        self.imgClose.addGestureRecognizer(tap)
        self.imgClose.isUserInteractionEnabled = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func closePress(){
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:Actions
    
    @IBAction func btnCLosePressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    //MARK:API
    //orderlist api
    func getOrderList(withId custId: String){
        
        WebServiceCall.callMethodWithURL(route: APIRouter.OrderList(withCustId: custId)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            if (responseValue != nil) {
                let responseModel = OrderListModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200 {
                    self.objOrderList = responseModel.data
                   // print(responseModel)
                    if self.objOrderList.count == 0{
                        self.imgOrderPage.isHidden = false
                        self.tblOrder.isHidden = true
                    }
                    self.tblOrder.reloadData()
                }
                else {
                    //GlobalMethods.showAlert(alertTitle: stringfile.strError[kAppDelegate.userCurrentLanguage], alertMessage: stringfile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
}
extension OrderViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if objOrderList.count != 0 {
            return objOrderList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderTableViewCell") as! OrderTableViewCell
        cell.imgOrder.layer.cornerRadius = cell.imgOrder.frame.size.width / 2
        cell.imgOrder.clipsToBounds = true
        let obj = objOrderList[indexPath.row]
        cell.lblOrderID.text = "Order is - \(obj.id)"
        self.orderIDarray.append(obj.id)
        cell.lblDate.text = obj.date
        cell.lblPrice.text = "\(StringFile.strPriceSymbol[kAppDelegate.userCurrentLanguage])\(obj.total)"
        cell.lblStatus.text = "Order Process"
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsViewController") as! OrderDetailsViewController
        vc.OrderID = orderIDarray[indexPath.row]
        let id = orderIDarray[indexPath.row]
        vc.getOrderDetails(withId: id)
        vc.status = objOrderList[indexPath.row].status
        self.presentInFullScreen(vc, animated: true)
        //present(vc, animated: true, completion: nil)
    }
}
