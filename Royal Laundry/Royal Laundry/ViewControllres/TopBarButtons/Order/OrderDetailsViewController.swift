//
//  OrderDetailsViewController.swift
//  DemoAPI
//
//  Created by Latitude Technolabs on 23/11/20.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

class OrderDetailsViewController: UIViewController {

    //MARK:Variables
    
    var objOrderDetailsDataModel = [OrderDetailsDataModel]()
    var objcartmodel = [CartDataModel]()
    
    var OrderID:Int?
    var status:String?
    //MARK:Outletss
    
    @IBOutlet var lblTopTitle: UILabel!
    @IBOutlet var viewTop: UIView!
    @IBOutlet var OrderScrollView: UIScrollView!
    
    @IBOutlet var ViewOrderContain: UIView!
    @IBOutlet var lblOrderID: UILabel!
    @IBOutlet var lblOrderDate: UILabel!
    @IBOutlet var imgOrder: UIImageView!
    @IBOutlet var lblOrderPlaced: UILabel!
    
    
    @IBOutlet var imgDelivered: UIImageView!
    @IBOutlet var imgTransit: UIImageView!
    @IBOutlet var imgPending: UIImageView!
    @IBOutlet var imgOrdered: UIImageView!
    
    
    @IBOutlet var lblDelivery: UILabel!
    @IBOutlet var lblTransit: UILabel!
    @IBOutlet var lblPending: UILabel!
    @IBOutlet var lblOrdered: UILabel!
    
    @IBOutlet var lblTotalAmt: UILabel!
    @IBOutlet var tblOrderClothes: UITableView!
    @IBOutlet var lblDeliveryDate: UILabel!
    @IBOutlet var lblDeliveryAddress: UILabel!
    
    @IBOutlet weak var viewStatusBG: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewStatusBG.backgroundColor = kAppThemePrimaryDarkColor
        viewTop.backgroundColor = kAppThemePrimaryLightColor
       // imgOrder.setRounded()
       // self.imgOrder.layer.cornerRadius = self.imgOrder.frame.size.width / 2
        self.imgOrder.clipsToBounds = true
        let nib = UINib(nibName: "ClothesTableViewCell", bundle: nil)
        tblOrderClothes.register(nib, forCellReuseIdentifier: "ClothesTableViewCell")
        tblOrderClothes.delegate = self
        tblOrderClothes.dataSource = self
        self.getOrderDetails(withId: OrderID!)
        
        lblOrderID.text = "Order id-\(OrderID ?? 0)"
        //imgOrder.image = #imageLiteral(resourceName: "icon")
        imgOrdered.image = #imageLiteral(resourceName: "one_colored")
//        if status == "1"{
//            imgOrdered.image = #imageLiteral(resourceName: "one_colored")
//            imgPending.image = #imageLiteral(resourceName: "two_gray")
//            imgTransit.image = #imageLiteral(resourceName: "three_gray")
//            imgDelivered.image = #imageLiteral(resourceName: "four_gray")
//        }else if status == "2"{
//            imgOrdered.image = #imageLiteral(resourceName: "one_gray")
//            imgPending.image = #imageLiteral(resourceName: "two_colored")
//            imgTransit.image = #imageLiteral(resourceName: "three_gray")
//            imgDelivered.image = #imageLiteral(resourceName: "four_gray")
//        }else if status == "3"{
//            imgOrdered.image = #imageLiteral(resourceName: "one_gray")
//            imgPending.image = #imageLiteral(resourceName: "two_gray")
//            imgTransit.image = #imageLiteral(resourceName: "three_colored")
//            imgDelivered.image = #imageLiteral(resourceName: "four_gray")
//        }else if status == "4"{
//            imgOrdered.image = #imageLiteral(resourceName: "one_gray")
//            imgPending.image = #imageLiteral(resourceName: "two_gray")
//            imgTransit.image = #imageLiteral(resourceName: "three_gray")
//            imgDelivered.image = #imageLiteral(resourceName: "four_colored")
//        }else{
//            imgOrdered.image = #imageLiteral(resourceName: "one_gray")
//            imgPending.image = #imageLiteral(resourceName: "two_gray")
//            imgTransit.image = #imageLiteral(resourceName: "three_gray")
//            imgDelivered.image = #imageLiteral(resourceName: "four_gray")
//        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:Actions
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    //MARK:API
    //orderlist api
    func getOrderDetails(withId orderId: Int){
        
        WebServiceCall.callMethodWithURL(route: APIRouter.OrderDetails(withOrderId: orderId)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            if (responseValue != nil) {
                let responseModel = OrderDetailsModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200 {
                    self.objOrderDetailsDataModel = responseModel.data
                    self.lblOrderDate.text = self.objOrderDetailsDataModel[0].collection_date
                    self.lblDeliveryAddress.text = self.objOrderDetailsDataModel[0].deliveryaddress
                    self.lblDeliveryDate.text = self.objOrderDetailsDataModel[0].delivery_date
                    self.lblTotalAmt.text = "\(StringFile.strPriceSymbol[kAppDelegate.userCurrentLanguage])\(self.objOrderDetailsDataModel[0].total)"
                    self.tblOrderClothes.reloadData()
                }
                else {
                    //GlobalMethods.showAlert(alertTitle: stringfile.strError[kAppDelegate.userCurrentLanguage], alertMessage: stringfile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
        
    }

}
extension OrderDetailsViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if objOrderDetailsDataModel.count != 0{
            return objOrderDetailsDataModel[0].cart.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClothesTableViewCell") as! ClothesTableViewCell
        let obj = objOrderDetailsDataModel[0].cart[indexPath.row]
        cell.lblTitle.text = "\(obj.count)*\(obj.product_name)"
        cell.lblPrice.text = "\(StringFile.strPriceSymbol[kAppDelegate.userCurrentLanguage])\(obj.product_price)"
      //  cell.lbldesc.text = "\(obj.count)*"
        return cell
    }
    
    
}
