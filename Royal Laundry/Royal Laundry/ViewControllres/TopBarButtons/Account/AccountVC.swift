//
//  AccountVC.swift
//  DemoAPI
//
//  Created by Latitude Technolabs on 18/11/20.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

class AccountVC: UIViewController {

    
    //MARK:Outlets
    @IBOutlet weak var ViewDetailsHeight: NSLayoutConstraint!
    @IBOutlet weak var viewMyProfile: UIView!
    @IBOutlet weak var MyProfileHeight: NSLayoutConstraint!
    @IBOutlet var viewSep: UIView!
    @IBOutlet var imgPrivacy: UIImageView!
    @IBOutlet var btnPrivacy: UIButton!
    @IBOutlet var viewPrivacy: UIView!
    
    @IBOutlet var imgTerms: UIImageView!
    @IBOutlet var btnTerms: UIButton!
    @IBOutlet var viewTerms: UIView!
    
    @IBOutlet var imgHelp: UIImageView!
    @IBOutlet var btnHelp: UIButton!
    @IBOutlet var viewHelp: UIView!
    @IBOutlet var lblOurDetails: UILabel!
    
    @IBOutlet var imgSignIn: UIImageView!
    @IBOutlet var btnSignIn: UIButton!
    @IBOutlet var viewSignIn: UIView!
    
    @IBOutlet weak var viewSignInHeight: NSLayoutConstraint!
    @IBOutlet var imgSignUp: UIImageView!
    @IBOutlet var btnSignUp: UIButton!
    @IBOutlet var viewSignUp: UIView!
    @IBOutlet weak var viewSignUpHeight: NSLayoutConstraint!
    @IBOutlet var lblYourDetails: UILabel!
    @IBOutlet var lblTitleTop: UILabel!
    @IBOutlet var viewTop: UIView!
    @IBOutlet weak var viewStatusBG: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        // Do any additional setup after loading the view.
    }
    func setupView(){
        viewStatusBG.backgroundColor = kAppThemePrimaryDarkColor
        viewTop.backgroundColor = kAppThemePrimaryLightColor
        if GlobalMethods.getUserDefaultObjectForKey(key: MyUserDefaultsKeys.isloginsucess) != nil{
            let login:Bool = GlobalMethods.getUserDefaultObjectForKey(key: MyUserDefaultsKeys.isloginsucess) as! Bool
            if login == true{
                ViewDetailsHeight.constant = 0
                viewSignInHeight.constant = 0
                viewSignUpHeight.constant = 0
                lblYourDetails.isHidden = true
                viewSignUp.isHidden = true
                viewSignIn.isHidden = true
                
                viewMyProfile.isHidden = false
                MyProfileHeight.constant = 36
            }else{
                ViewDetailsHeight.constant = lblOurDetails.frame.height
                viewSignInHeight.constant = 53
                viewSignUpHeight.constant = 53
                lblYourDetails.isHidden = false
                viewSignUp.isHidden = false
                viewSignIn.isHidden = false
                
                viewMyProfile.isHidden = true
                MyProfileHeight.constant = 0
            }
        }else{
            ViewDetailsHeight.constant = lblOurDetails.frame.height
            viewSignInHeight.constant = 53
            viewSignUpHeight.constant = 53
            lblYourDetails.isHidden = false
            viewSignUp.isHidden = false
            viewSignIn.isHidden = false
            
            viewMyProfile.isHidden = true
            MyProfileHeight.constant = 0
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:Actions
    @IBAction func btnMyProfilePressed(_ sender: Any) {
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.presentInFullScreen(objVC, animated: true)
    }
    @IBAction func btnPrivacyPressed(_ sender: UIButton) {
//        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
//        objVC.docurl = "https://laundrapp.com/privacy-policy/"
//        self.presentInFullScreen(objVC, animated: true)
        
    }
    @IBAction func btnTermsPressed(_ sender: UIButton) {
//        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
//       objVC.docurl =  "https://laundrapp.com/terms-conditions/"
//        self.presentInFullScreen(objVC, animated: true)
    }
    @IBAction func btnHelpPressed(_ sender: UIButton) {
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "HelpAlertViewController") as! HelpAlertViewController
        self.addChild(objVC)
        
        objVC.view.frame = self.view.frame
        self.view.addSubview(objVC.view)
        objVC.didMove(toParent: self)
        
    }
    @IBAction func btnSignInPressed(_ sender: UIButton) {
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.presentInFullScreen(objVC, animated: true)
    }
    @IBAction func btnSignUpPressed(_ sender: UIButton) {
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.presentInFullScreen(objVC, animated: true)
    }
    @IBAction func btnClosePressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
