//
//  SearchItemViewController.swift
//  DemoAPI
//
//  Created by Latitude Technolabs on 21/11/20.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

class SearchItemViewController: UIViewController,UISearchControllerDelegate, UISearchBarDelegate {

    //MARK:Variables
    var arrProductItem = [ProductDetailDataModel]()
    var arrCatProduct = [ProductSubCatModel]()
    let objDbManager = SQLiteHelper()
    var objProductDetail = ProductCatModel()
    var objproduct = ProductModel()
    var objproductdatamodel = [ProductDataModel]()
    
    var searchbarcontroller = UISearchController(searchResultsController: nil)
    var searching = false
    //var arrsearch = [ProductSubCatModel]()
    var arrsearch = [ProductDetailDataModel]()
    var searchdata = ProductDetailDataModel()
    var arrData = ProductDetailDataModel()
    
    var name = [String]()
    var custid:Int32?
    
    var didSelectItem: ((_ item: Bool) -> Void)?
    var isbasket:Bool!
    
    //MARK:Outlets
    
    
    @IBOutlet var tblSeachItem: UITableView!
    @IBOutlet var searchbar: UISearchBar!

    @IBOutlet weak var viewStatusBG: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib1 = UINib(nibName: "ProductItemTableViewCell", bundle: nil)
        tblSeachItem.register(nib1, forCellReuseIdentifier: "ProductItemTableViewCell")
        tblSeachItem.delegate = self
        tblSeachItem.dataSource = self
        print(self.arrCatProduct.count)
        self.searchbarcontroller.delegate = self
        searchbar.delegate = self
        searchbar.placeholder = "Search here"
        searchbar.tintColor = .white
        searchbarcontroller.hidesNavigationBarDuringPresentation = false
        searchbarcontroller.searchBar.becomeFirstResponder()
        searchbarcontroller.obscuresBackgroundDuringPresentation = false
        searchbarcontroller.searchBar.autocapitalizationType = .words
        searchbarcontroller.searchBar.textColor = UIColor.white
//        let textField = searchbarcontroller.searchBar.value(forKey: "searchField") as? UITextField
//        textField?.textColor = UIColor.white
//        if #available(iOS 13.0, *) {
//            searchbarcontroller.searchBar.searchTextField.textColor = .white
//        } else {
//            // Fallback on earlier versions
//        }
       // custid = UserDefaults.standard.object(forKey: "customerid") as! integer_t
        SetUPInitialView()
    }
    func SetUPInitialView(){
        viewStatusBG.backgroundColor = kAppThemePrimaryDarkColor
        self.view.backgroundColor = kAppThemePrimaryLightColor
        searchbarcontroller.searchBar.backgroundColor = kAppThemePrimaryLightColor
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getProductDetail(withId: "1")
        self.getProductDetail(withId: "2")
        self.getProductDetail(withId: "3")
        self.getProductDetail(withId: "4")
        self.getProductDetail(withId: "5")
        self.getProductDetail(withId: "6")
        self.getProductDetail(withId: "7")
        self.getProductDetail(withId: "8")
        self.getProductDetail(withId: "9")
        self.getProductDetail(withId: "10")
        self.getProductDetail(withId: "11")
        self.getProductDetail(withId: "12")
      
        self.tblSeachItem.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Mark:Actions
    @IBAction func btnClosePressed(_ sender: UIButton) {
        if isbasket == true{
             didSelectItem?(true)
            self.view.removeFromSuperview()
        }else{
            dismiss(animated: true, completion: nil)
        }
 
    }
    // MARK: - Web Service Function Methods
    func getProductDetail(withId Id: String){
        
        WebServiceCall.callMethodWithURL(route: APIRouter.GetService(withID: Id)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            if (responseValue != nil) {
                let responseModel = ProductDetailModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200 {
                    
                    print(responseModel)
                    self.arrProductItem =  self.arrProductItem + responseModel.data
                    
                    print(self.arrProductItem)
                    
                    // ()
                    self.tblSeachItem.reloadData()
                }
                else {
                    //GlobalMethods.showAlert(alertTitle: stringfile.strError[kAppDelegate.userCurrentLanguage], alertMessage: stringfile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    //MARK:Functions
    func setResponseProductItem(){
        
        let categorieNames = Array(Set(arrProductItem.map({$0.service_name})))
        
        //self.arrCatProduct.removeAll()
        for serviceName in categorieNames{
            
            let check = arrProductItem.filter { $0.service_name == serviceName}
            
            let catObj = ProductSubCatModel()
            catObj.catData = check
            catObj.service_name = serviceName
            
            self.arrCatProduct.append(catObj)
        }
        self.tblSeachItem.reloadData()
        
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let searchString = searchbar.text
        if searchString != "" && arrProductItem.count != 0{
            let filtredArray = Array(Set(arrProductItem))
            arrsearch = filtredArray.filter{
                $0.name.contains(searchString!)
            }
            searching  = true
            tblSeachItem.reloadData()
        }
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        tblSeachItem.reloadData()
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        tblSeachItem.reloadData()
    }
}
//MARK:Tableview Methods
extension SearchItemViewController:UITableViewDataSource,UITableViewDelegate{
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return ProductTitle.count
        if searching{
            let unique = Array(Set(arrsearch))
            return unique.count
        }else{
            let unique = Array(Set(arrProductItem))
                return unique.count
        }
      
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductItemTableViewCell") as! ProductItemTableViewCell
       // searchdata = arrProductItem[indexPath.section]
                if searching{
                    let obj = arrsearch[indexPath.row]
                    cell.lblTitle.text = obj.name
                    cell.lblDesc.text = obj.description1
                    let doubleprice = Double(obj.price)
                    cell.lblPrice.text = "\(StringFile.strPriceSymbol[kAppDelegate.userCurrentLanguage])\(doubleprice ?? 0.0)"
                    cell.lblQuantity.text = "\(obj.userSelectedQuantity)"
                    cell.btnDecHeight.constant = 0
                    cell.lblQuaHeight.constant = 0
                    cell.imgDecrement.isHidden = true
                    cell.lblQuantity.isHidden = true
                    cell.btnDecrement.isHidden = true


            //        cell.lblQtyItem.text = "\(arrProductItem[indexPath.row].userSelectedQuantity)"

                    let strCountQuery = "SELECT * FROM cart_item WHERE service_id=\(obj.service) and category_id=\(obj.category_id) and subcategory_id=\(obj.subcat_id);"
                    print(strCountQuery)
                    
                    let database =  try! SQLitePool.manager().initialize(database: "royalLaundry", withExtension: "sqlite")
                    
                    let resultSelectAll = try! database.query(strCountQuery)
                    print(resultSelectAll)
                    if resultSelectAll.affectedRowCount > 0 {
                        
                        let resultPrice = resultSelectAll.results![0]
                        
                        let quantity = Int(resultPrice["quantity"] as! String)
                        if quantity! > 0 {
                            cell.imgIncrement.image = #imageLiteral(resourceName: "add_item")
                            cell.btnDecHeight.constant = 40
                            cell.lblQuaHeight.constant = 40
                            
                            cell.imgDecrement.isHidden = false
                            cell.lblQuantity.isHidden = false
                            cell.btnDecrement.isHidden = false
                            
                        } else {
                            cell.imgIncrement.image = #imageLiteral(resourceName: "Plus")
                            cell.btnDecHeight.constant = 0
                            cell.lblQuaHeight.constant = 0
                            
                            cell.imgDecrement.isHidden = true
                            cell.lblQuantity.isHidden = true
                            cell.btnDecrement.isHidden = true
                            
                        }
                        cell.lblQuantity.text = "\(quantity!)"
                        
                        obj.userSelectedQuantity = quantity!
                    } else {
                        cell.imgIncrement.image = #imageLiteral(resourceName: "Plus")
                        cell.btnDecHeight.constant = 0
                        cell.lblQuaHeight.constant = 0
                        
                        cell.lblQuantity.isHidden = true
                        cell.btnDecrement.isHidden = true
                    }
                    cell.actionDescrementItemClosure = { [weak self] in
                        print("item removed")
                        
                        let database =  try! SQLitePool.manager().initialize(database: "royalLaundry", withExtension: "sqlite")
                        
                        if resultSelectAll.affectedRowCount > 0 {
                            
                            let resultPrice = resultSelectAll.results![0]
                            
                            var quantity = Int(resultPrice["quantity"] as! String)
                            
                            quantity = quantity! - 1
                            
                            if quantity! > 0 {
                                
                                let strUpdateQuery = "UPDATE cart_item SET quantity = \(quantity!) WHERE service_id=\(obj.service ) and category_id=\(obj.category_id ) and subcategory_id=\(obj.subcat_id);"
                                //                    print(strUpdateQuery)
                                
                                database.query(strUpdateQuery, successClosure: { (result) in
                                    
                                    print(result)
                                    GlobalMethods.getBasketUpdate()
                                    let updateIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
                                    self!.tblSeachItem.reloadRows(at: [updateIndexPath], with: .none)
                                    
                                    
                                }) { (e) in
                                    print(e)
                                }
                                
                            } else {
                                
                                let strDeleteQuery = "DELETE from cart_item WHERE service_id=\(obj.service) and category_id=\(obj.category_id) and subcategory_id=\(obj.subcat_id);"
                                                    print(strDeleteQuery)
                                
                                database.query(strDeleteQuery, successClosure: { (result) in
                                    
                                    print(result)
                                    GlobalMethods.getBasketUpdate()
                                    let updateIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
                                    self!.tblSeachItem.reloadRows(at: [updateIndexPath], with: .none)
                                    
                                }) { (e) in
                                    print(e)
                                }
                            }
                        }
                        else{
                            
                            let strDeleteQuery = "DELETE from cart_item WHERE service_id=\(obj.service) and category_id=\(obj.category_id) and subcategory_id=\(obj.subcat_id);"
                                                print(strDeleteQuery)
                            
                            database.query(strDeleteQuery, successClosure: { (result) in
                                
                                print(result)
                                GlobalMethods.getBasketUpdate()
                                let updateIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
                                self!.tblSeachItem.reloadRows(at: [updateIndexPath], with: .none)
                                
                            }) { (e) in
                                print(e)
                            }
                        }
                    }
                    
                    cell.actionIncrementItemClosure = { [weak self] in
                        print("item added")
                        
                        let database =  try! SQLitePool.manager().initialize(database: "royalLaundry", withExtension: "sqlite")
                        
                        
                        if resultSelectAll.affectedRowCount > 0 {
                            
                            let resultPrice = resultSelectAll.results![0]
                            
                            var quantity = Int(resultPrice["quantity"] as! String)
                            
                            quantity = quantity! + 1
                            
                            let strUpdateQuery = "UPDATE cart_item SET quantity = \(quantity!) WHERE service_id=\(obj.service) and category_id=\(obj.category_id) and subcategory_id=\(obj.subcat_id) "
                                                print(strUpdateQuery)
                            
                            database.query(strUpdateQuery, successClosure: { (result) in
                                
                                print(result)
                                GlobalMethods.getBasketUpdate()
                                let updateIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
                                self!.tblSeachItem.reloadRows(at: [updateIndexPath], with: .none)
                                
                            }) { (e) in
                                print(e)
                            }
                            
                        } else {
                            
                            let quantity = 1
                            
                            let strInsertQuery = "INSERT INTO cart_item (service_id, category_id, subcategory_id, customer_id, quantity, product_id, product_name, product_description ,price) VALUES('\(obj.service)','\(obj.category_id)','\(obj.subcat_id)','newCust','\(quantity)','\(self!.objProductDetail.id )','\(obj.name)','\(obj.description1)','\(obj.price)');"
                            //                    print(strInsertQuery)

                            database.query(strInsertQuery, successClosure: { (result) in
                                
                                print(result)
                                GlobalMethods.getBasketUpdate()
                                let updateIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
                                self!.tblSeachItem.reloadRows(at: [updateIndexPath], with: .none)
                            }) { (e) in
                                print(e)
                            }
                        }
                    }
                }else{
                    cell.lblTitle.text = arrProductItem[indexPath.row].name
                    cell.lblDesc.text = arrProductItem[indexPath.row].description1
                    let doubleprice = Double(arrProductItem[indexPath.row].price)
                    cell.lblPrice.text = "\(StringFile.strPriceSymbol[kAppDelegate.userCurrentLanguage])\(doubleprice ?? 0.0)"
                    cell.btnDecHeight.constant = 0
                    cell.lblQuaHeight.constant = 0
                    cell.imgDecrement.isHidden = true
                    cell.lblQuantity.isHidden = true
                    cell.btnDecrement.isHidden = true


            //        cell.lblQtyItem.text = "\(arrProductItem[indexPath.row].userSelectedQuantity)"

                    let strCountQuery = "SELECT * FROM cart_item WHERE service_id=\(arrProductItem[indexPath.row].service) and category_id=\(arrProductItem[indexPath.row].category_id) and subcategory_id=\(arrProductItem[indexPath.row].subcat_id);"
                    print(strCountQuery)
                    
                    let database =  try! SQLitePool.manager().initialize(database: "royalLaundry", withExtension: "sqlite")
                    
                    let resultSelectAll = try! database.query(strCountQuery)
                    print(resultSelectAll)
                    if resultSelectAll.affectedRowCount > 0 {
                        
                        let resultPrice = resultSelectAll.results![0]
                        
                        let quantity = Int(resultPrice["quantity"] as! String)
                        if quantity! > 0 {
                            cell.imgIncrement.image = #imageLiteral(resourceName: "add_item")
                            cell.btnDecHeight.constant = 40
                            cell.lblQuaHeight.constant = 40
                            
                            cell.imgDecrement.isHidden = false
                            cell.lblQuantity.isHidden = false
                            cell.btnDecrement.isHidden = false
                            
                        } else {
                            cell.imgIncrement.image = #imageLiteral(resourceName: "Plus")
                            cell.btnDecHeight.constant = 0
                            cell.lblQuaHeight.constant = 0
                            
                            cell.imgDecrement.isHidden = true
                            cell.lblQuantity.isHidden = true
                            cell.btnDecrement.isHidden = true
                            
                        }
                        cell.lblQuantity.text = "\(quantity!)"
                        
                        arrProductItem[indexPath.row].userSelectedQuantity = quantity!
                    } else {
                        cell.imgIncrement.image = #imageLiteral(resourceName: "Plus")
                        cell.btnDecHeight.constant = 0
                        cell.lblQuaHeight.constant = 0
                        
                        cell.lblQuantity.isHidden = true
                        cell.btnDecrement.isHidden = true
                    }
                    cell.actionDescrementItemClosure = { [weak self] in
                        print("item removed")
                        
                        let database =  try! SQLitePool.manager().initialize(database: "royalLaundry", withExtension: "sqlite")
                        
                        if resultSelectAll.affectedRowCount > 0 {
                            
                            let resultPrice = resultSelectAll.results![0]
                            
                            var quantity = Int(resultPrice["quantity"] as! String)
                            
                            quantity = quantity! - 1
                            
                            if quantity! > 0 {
                                
                                let strUpdateQuery = "UPDATE cart_item SET quantity = \(quantity!) WHERE service_id=\(self?.arrProductItem[indexPath.row].service ?? "") and category_id=\(self?.arrProductItem[indexPath.row].category_id ?? "") and subcategory_id=\(self?.arrProductItem[indexPath.row].subcat_id ?? "");"
                                //                    print(strUpdateQuery)
                                
                                database.query(strUpdateQuery, successClosure: { (result) in
                                    
                                    print(result)
                                    GlobalMethods.getBasketUpdate()
                                    let updateIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
                                    self!.tblSeachItem.reloadRows(at: [updateIndexPath], with: .none)
                                    
                                    
                                }) { (e) in
                                    print(e)
                                }
                                
                            } else {
                                
                                let strDeleteQuery = "DELETE from cart_item WHERE service_id=\(self?.arrProductItem[indexPath.row].service ?? "") and category_id=\(self?.arrProductItem[indexPath.row].category_id ?? "") and subcategory_id=\(self?.arrProductItem[indexPath.row].subcat_id ?? "");"
                                                    print(strDeleteQuery)
                                
                                database.query(strDeleteQuery, successClosure: { (result) in
                                    
                                    print(result)
                                    GlobalMethods.getBasketUpdate()
                                    let updateIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
                                    self!.tblSeachItem.reloadRows(at: [updateIndexPath], with: .none)
                                    
                                }) { (e) in
                                    print(e)
                                }
                            }
                        }
                        else{
                            
                            let strDeleteQuery = "DELETE from cart_item WHERE service_id=\(self?.arrProductItem[indexPath.row].service ?? "") and category_id=\(self?.arrProductItem[indexPath.row].category_id ?? "") and subcategory_id=\(self?.arrProductItem[indexPath.row].subcat_id ?? "");"
                                                print(strDeleteQuery)
                            
                            database.query(strDeleteQuery, successClosure: { (result) in
                                
                                print(result)
                                GlobalMethods.getBasketUpdate()
                                let updateIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
                                self!.tblSeachItem.reloadRows(at: [updateIndexPath], with: .none)
                                
                            }) { (e) in
                                print(e)
                            }
                        }
                    }
                    
                    cell.actionIncrementItemClosure = { [weak self] in
                        print("item added")
                        
                        let database =  try! SQLitePool.manager().initialize(database: "royalLaundry", withExtension: "sqlite")
                        
                        
                        if resultSelectAll.affectedRowCount > 0 {
                            
                            let resultPrice = resultSelectAll.results![0]
                            
                            var quantity = Int(resultPrice["quantity"] as! String)
                            
                            quantity = quantity! + 1
                            
                            let strUpdateQuery = "UPDATE cart_item SET quantity = \(quantity!) WHERE service_id=\(self?.arrProductItem[indexPath.row].service ?? "") and category_id=\(self?.arrProductItem[indexPath.row].category_id ?? "") and subcategory_id=\(self?.arrProductItem[indexPath.row].subcat_id ?? "") "
                                                print(strUpdateQuery)
                            
                            database.query(strUpdateQuery, successClosure: { (result) in
                                
                                print(result)
                                GlobalMethods.getBasketUpdate()
                                let updateIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
                                self!.tblSeachItem.reloadRows(at: [updateIndexPath], with: .none)
                                
                            }) { (e) in
                                print(e)
                            }
                            
                        } else {
                            
                            let quantity = 1
                            
                            let strInsertQuery = "INSERT INTO cart_item (service_id, category_id, subcategory_id, customer_id, quantity, product_id, product_name, product_description ,price) VALUES('\(self?.arrProductItem[indexPath.row].service ?? "")','\(self?.arrProductItem[indexPath.row].category_id ?? "")','\(self?.arrProductItem[indexPath.row].subcat_id ?? "")','newCust','\(quantity)','\(self!.objProductDetail.id )','\(self?.arrProductItem[indexPath.row].name ?? "")','\(self?.arrProductItem[indexPath.row].description1 ?? "")','\(self?.arrProductItem[indexPath.row].price ?? "")');"
                            //                    print(strInsertQuery)

                            database.query(strInsertQuery, successClosure: { (result) in
                                
                                print(result)
                                GlobalMethods.getBasketUpdate()
                                let updateIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
                                self!.tblSeachItem.reloadRows(at: [updateIndexPath], with: .none)
                            }) { (e) in
                                print(e)
                            }
                        }
                    }
        }


        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailVC") as! ProductDetailVC
        objVC.isSearch = true
        if searching == true{
            let unique = Array(Set(arrsearch))
            objVC.searchproductid = unique[indexPath.row].id
            objVC.searchname = unique[indexPath.row].name
            objVC.searchDesc = unique[indexPath.row].description1
            objVC.searchDesc = unique[indexPath.row].price
            objVC.searchcatid = unique[indexPath.row].category_id
            
        }else{
            //let unique = Array(Set(arrProductItem))
            objVC.searchproductid = arrProductItem[indexPath.row].id
            objVC.searchname = arrProductItem[indexPath.row].name
            objVC.searchDesc = arrProductItem[indexPath.row].description1
            objVC.searchDesc = arrProductItem[indexPath.row].price
            objVC.searchcatid = arrProductItem[indexPath.row].category_id
        }

        self.presentInFullScreen(objVC, animated: true)
    }

}
extension UISearchBar {

 var textColor:UIColor? {
     get {
        if let textField = self.value(forKey: "searchField") as? UITextField  {
             return textField.textColor
         } else {
             return nil
         }
     }

     set (newValue) {
        if let textField = self.value(forKey: "searchField") as? UITextField  {
             textField.textColor = newValue
         }
     }
 }
}
