//
//  CustomAlertVC.swift
//  Royal Laundry
//
//  Created by Shine on 02/10/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit

class CustomAlertVC: UIViewController {

    //MARK:- Outlets

    @IBOutlet weak var viewMainBG: UIView!
    @IBOutlet weak var btnDismissVC: UIButton!
    @IBOutlet weak var viewPopArea: UIView!
    @IBOutlet weak var txtPopUpTitle: UITextView!
    @IBOutlet weak var txtPopUpDetail: UITextView!
    @IBOutlet weak var btnRetry: UIButton!
    @IBOutlet weak var ImgAlertIcon: UIImageView!
    
    @IBOutlet weak var imgClose: UIImageView!
    @IBOutlet weak var btnClose: UIButton!
    
    
    // MARK: - constants & variables
    
    
    
    //MARK:- UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupInitialView()
    }

    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        viewMainBG.backgroundColor = UIColor(white: 1, alpha: 0.5)
        
        btnRetry.backgroundColor = kAppButtonBGActiveColor
        btnRetry.setTitle(StringFile.strRetry[kAppDelegate.userCurrentLanguage], for: .normal)

        ImgAlertIcon.setRounded()
        
        viewPopArea.dropShadow(color: UIColor.darkGray, opacity: 0.5, shadowRadius: 10.0, cornerRadius: 10.0)
        
//        txtPopUpTitle.fitText()
//
//        txtPopUpDetail.fitText()

    }
    
    func dismissPopView(){
    
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()

    }

        

    //MARK:- IBActions
    @IBAction func btnDismissVCPressed(_ sender: Any) {
        self.dismissPopView()
    }
    @IBAction func btnRetryPressed(_ sender: Any) {
       self.dismissPopView()
    }
    
    @IBAction func btnClosePressed(_ sender: Any) {
        self.dismissPopView()
    }
    
}
