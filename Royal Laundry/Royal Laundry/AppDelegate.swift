//
//  AppDelegate.swift
//  Royal Laundry
//
//  Created by Shine on 30/09/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import Stripe
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    /*~~~~~ push notification ~~~~~*/
    var dicRemoteNotification : [AnyHashable : Any]?
    var iOSDeviceToken : String = ""
    var isPaymentSucess:Bool?
    
    
    var basketPrice = 0.0
    var basketItemQuantity = 0
    enum UserDefaultsKeys : String {
        case PostalCodeData
    }

    /*----- 0-English, 1-Hindi 2-Gujarati -----*/
    var userCurrentLanguage = 0


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        Stripe.setDefaultPublishableKey("pk_test_gz44KxgHfxwAqXMKP0u13hX8")
        if GlobalMethods.getUserDefaultObjectForKey(key: MyUserDefaultsKeys.isloginsucess) == nil{
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "AvailabilityCheckVC")
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        }
        
        if  GlobalMethods.getUserDefaultObjectForKey(key: MyUserDefaultsKeys.isloginsucess) != nil || GlobalMethods.getUserDefaultObjectForKey(key: MyUserDefaultsKeys.PostalCodeData) != nil{
            if  GlobalMethods.getUserDefaultObjectForKey(key: MyUserDefaultsKeys.isloginsucess) != nil{
            let login:Bool = GlobalMethods.getUserDefaultObjectForKey(key: MyUserDefaultsKeys.isloginsucess) as! Bool
            if login == true{
                self.window = UIWindow(frame: UIScreen.main.bounds)
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "MainTabVC")
                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
            }else{
                self.window = UIWindow(frame: UIScreen.main.bounds)
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "AvailabilityCheckVC")
                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
            }
        }
            if GlobalMethods.getUserDefaultObjectForKey(key: MyUserDefaultsKeys.PostalCodeData) != nil{
                self.window = UIWindow(frame: UIScreen.main.bounds)
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "MainTabVC")
                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
            }

        } else {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "AvailabilityCheckVC")
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        }
        return true
    }


}

