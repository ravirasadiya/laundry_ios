//
//  ProductItemTableViewCell.swift
//  DemoAPI
//
//  Created by Latitude Technolabs on 18/11/20.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

class ProductItemTableViewCell: UITableViewCell {

    //MARK:Variables
    var actionIncrementItemClosure : (()->Void)? = nil
    var actionDescrementItemClosure : (()->Void)? = nil
    
    //MARK: Outlets
    
    
    @IBOutlet weak var imgDecrement: UIImageView!
    @IBOutlet weak var imgIncrement: UIImageView!
    @IBOutlet var viewProductItem: UIView!
    @IBOutlet var lblQuantity: UILabel!
    @IBOutlet var btnIncrement: UIButton!
    @IBOutlet var btnDecrement: UIButton!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblQuaHeight: NSLayoutConstraint!
    @IBOutlet var btnDecHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //MARK:Actions
    
    @IBAction func btnDecrementPressed(_ sender: UIButton) {
        self.actionDescrementItemClosure!()
    }
    @IBAction func btnIncrementPressed(_ sender: UIButton) {
        self.actionIncrementItemClosure!()
    }
}
