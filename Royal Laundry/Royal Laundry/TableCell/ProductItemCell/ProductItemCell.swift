//
//  ProductItemCell.swift
//  Royal Laundry
//
//  Created by Shine on 13/10/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit

class ProductItemCell: UITableViewCell {
    
    @IBOutlet weak var viewMainBG: UIView!
    @IBOutlet weak var viewSelectedItem: UIView!
    @IBOutlet weak var lblItemTitle: UILabel!
    @IBOutlet weak var lblItemPrice: UILabel!
    @IBOutlet weak var lblItemDetail: UILabel!
        
    @IBOutlet weak var btnIncrementItem: UIButton!
    @IBOutlet weak var btnDescrementItem: UIButton!
    @IBOutlet weak var lblQtyItem: UILabel!

    @IBOutlet weak var constBtnDecHeight: NSLayoutConstraint!
    @IBOutlet weak var constLblQtyHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var imgDecrItem: UIImageView!
    @IBOutlet weak var imgIncrItem: UIImageView!
    
    var actionIncrementItemClosure : (()->Void)? = nil
    var actionDescrementItemClosure : (()->Void)? = nil
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnIncrementItemPressed(_ sender: UIButton) {
        //here I want to execute the UIActionSheet
        self.actionIncrementItemClosure?()
    }
    
    @IBAction func btnDescrementItemPressed(_ sender: UIButton) {
        //here I want to execute the UIActionSheet
        self.actionDescrementItemClosure?()
    }


}
