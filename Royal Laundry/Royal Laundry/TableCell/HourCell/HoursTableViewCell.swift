//
//  HoursTableViewCell.swift
//  DemoAPI
//
//  Created by Latitude Technolabs on 02/12/20.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

class HoursTableViewCell: UITableViewCell {

    //MARK:Outlets
    @IBOutlet var lblCharge: UILabel!
    @IBOutlet var lblHoursSlot: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
